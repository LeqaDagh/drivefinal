<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{

    	protected $fillable = [
				'subject_id','subject_type','name','user_id','ip','browser','browser_version','platform','platform_version','device'
							  ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }   

  public function scopeLikeUrl($query,$url)
    {       
            if($url){

                    $query->where('activities.url','like','%'.$url.'%');
                }       
    }
      public function scopeLikeText($query,$text)
    {       
            if($text){

                    $query->where('activities.text','like','%'.$text.'%');
                }       
    }
   public function scopeEqualUserId($query,$user_id)
    {       
            if($user_id){

                    $query->where('activities.user_id','=',$user_id);
                }       
    }   
    public function scopeEqualController($query,$controller)
    {       
            if($controller){

                    $query->where('activities.controller','=',$controller);
                }       
    }  
       public function scopeEqualFunction($query,$function)
    {       
            if($function){

                    $query->where('activities.function','=',$function);
                }       
    }  

    public function scopeFrom($query,$from)
    {       
            if($from){

                    $query->where('activities.created_at','>=', date('Y-m-d H:i:s',strtotime($from)));
                }       
    }
    public function scopeTo($query,$to)
    {       
            if($to){

                     $query->where('activities.created_at','<=', date('Y-m-d H:i:s',strtotime($to)));
                }       
    }

}