<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use \App\Vehicle;

class DeleteVehiclesPeriodCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:delete_vehicle_period';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $vehicles = Vehicle::whereRaw(' delete_at < NOW() ')->get();

        foreach ($vehicles as $key => $vehicle) 
        {
       
                $vehicle->update(['is_deleted'=>'yes']);
        }
    }
}
