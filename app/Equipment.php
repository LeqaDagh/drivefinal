<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{

	protected $table = 'equipments';
    protected $fillable = ['equipment_value','equipment_type'];

      public function vehicle()
    {
       return $this->belongsTo(Vehicle::class)->withPivot(['equipment_value', 'equipment_type']);
    }

}