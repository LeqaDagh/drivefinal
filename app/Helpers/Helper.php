<?php
namespace App\Helpers;

use App\Setting;

use App\Sms;


class Helper
{
    public static function sendSms(string $mobile,string $msg)
    {

        $sms_status =  (Setting::where('settings_key','sms_status')->limit(1)->first()->settings_value);
     
        $st = "not_active";

        if($sms_status=='active')
        {
          
            $st = "success";

            $jawalsms = new \App\Helpers\JawalSms();
            $jawalsms->sendSms($mobile,$msg);  


        }  

        Sms::create(['sms_type'=>'send','sms_number'=>$mobile,'sms_text'=>$msg,'sms_status'=>$st]);   
    }


	public static function distance($lat1, $lon1, $lat2, $lon2, $unit='K') 
    { 
        $theta = $lon1 - $lon2; 
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)); 
        $dist = acos($dist); 
        $dist = rad2deg($dist); 
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") 
        {
            return ($miles * 1.609344)*1000; 
        } 
        else if ($unit == "N") 
        {
        return ($miles * 0.8684);
        } 
        else 
        {
        return $miles;
      }
    }  
}