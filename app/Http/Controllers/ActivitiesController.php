<?php

 namespace App\Http\Controllers;


use App\Activity;

use App\User;

use App\Http\Requests;

use App\Http\Requests\NullRequest;

use App\Http\Requests\AdminRequest;

use Illuminate\HttpResponse;

use App\Http\Controllers\Controller;

use Request;

use Auth;



class ActivitiesController extends Controller
{
    public function __construct()
	{
	}


	public function index(NullRequest $request)
	{
		

		 $activities = Activity::select('activities.*')
							   ->LikeUrl($request->url)
							   ->LikeText($request->text)
							   ->EqualUserId($request->user_id)
							   ->EqualController($request->controller)
							   ->EqualFunction($request->function)
							   ->From($request->from)
							   ->To($request->to)
							   ->latest('activities.id')
		 					   ->paginate(30);


		 $users 	 = User::pluck('name','id'); 

		 $users->prepend(trans('main.all'),0);	



		 return view('activities.index',compact('activities','users'));
	}

		public function show($id)
	{

	if(!Auth::user()->haspermmission('show_activity')) {die(trans('main.do_not_have_permissions'));}

		 $activity = Activity::select('activities.*','users.name')
		 					  ->leftJoin('users','activities.user_id','=','users.id')
		 					  ->findOrFail($id);

	Activity::create([
				'user_id'=>Auth::user()->id,
				'controller'=>'activities',
				'function'=>'show',
				'url'=>$id

				 ]);

		 return view('activities.show',compact('activity'));
	}
	
}
