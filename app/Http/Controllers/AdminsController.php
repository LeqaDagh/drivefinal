<?php

namespace App\Http\Controllers;

use App\User;

use App\Activity;

use App\Region;

use App\Role; 

use App\Http\Requests;

use App\Http\Requests\AdminRequest;

use App\Http\Requests\NullRequest;

use Illuminate\HttpResponse;

use Request;

use Auth;

use DB;

use PDF;

use App\Enums\UserGroups;


class AdminsController extends Controller
{
    
	private $METHOD ;

	private $role ;

	private $group = UserGroups::ADMIN;

  	public function __construct()
	{
	

			 $this->role = trans('main.user_groups')[$this->group]['default_role'];

	}




	private function getAdmins(NullRequest $request)
	{
		$admins = User::admin()
					  ->hisOwen()
 		   			  ->equalRegion($request->get('region_id'))
			          ->likeUsername($request->get('username'))
			          ->likeName($request->get('name'))
			          ->likeEmail($request->get('email'))
			          ->likeMobile($request->get('mobile'))
			          ->equalUserStatus($request->get('user_status'));

		return $admins;		          

	}
    public function index(NullRequest $request)
	{

		$paginate  = 10;
		if($request->pagination)
		{
			$paginate  = $request->pagination;	
		}

		if($paginate>2000)
		{
			$paginate = 2000;
		}


 		 $admins = $this->getadmins($request)->paginate($paginate);


		 $regions 	= Region::pluck('name','id');
		 $regions->prepend(trans('main.all'),0);

		 $user_status = collect(trans('main.user_status'));
		 $user_status->prepend(trans('main.all'),0);

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('admins.index',compact('admins',"regions","user_status"));
	}

    public function pdf(NullRequest $request)
	{
		$paginate  = 10;

		if($request->pagination)
		{
			$paginate  = $request->pagination;	
		}

		if($paginate>2000)
		{
			$paginate = 2000;
		}


 		$admins = $this->getadmins($request)->paginate($paginate);


		 $view = \View::make('admins.pdf')->with(['admins'=>$admins]);
         $html = $view->render();
		 PDF::SetFooterMargin(40);
         PDF::SetTitle('طباعة تقرير');
         PDF::AddPage();
         PDF::SetAutoPageBreak(TRUE, 40);
         PDF::setRTL(true);
         PDF::SetFont('dejavusans', '', 10); 
         PDF::writeHTML($html, true, false, true, false, '');
       	 ob_end_clean();
         PDF::Output(time(). '_' . rand(1,999) . '_' .'admins.pdf');

	}

	public function create()
	{


		 $regions 	= Region::whereNull('parent')->pluck('name','id');

		 $roles 	= Role::pluck('name','id');

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('admins.create',compact("regions","roles"));	
	}

	public function store(AdminRequest $request)
	{


		$region = Region::find($request->region_id);


		$role = Role::find($this->role);

		$request->merge(['group' => $this->group]);

		$admin = new \App\User($request->all());

		$admin->region()->associate($region);

		$admin->save();

		$admin->roles()->sync([@$role->id]);

		return redirect('admins');	
	}

	public function edit($id)
	{
		 $admin 		= User::admin()->hisOwen()->findOrFail($id);

		 $regions 	= Region::whereNull('parent')->pluck('name','id');	

		 $roles 	= Role::pluck('name','id');


		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('admins.edit',compact('admin','regions','roles'));	
	}
	public function update($id,AdminRequest $request)
	{
	

		$region = Region::find($request->region_id);

		$admin = User::admin()->hisOwen()->findOrFail($id);

		$admin->fill($request->all());

		$admin->region()->associate($region);

		$admin->save();
		
		return redirect('admins/'.$id.'/edit');
		
	}

	public function show($id)
	{
		$admin = User::admin()->hisOwen()->findOrFail($id);

		Auth::user()->recordUserActivity($this->METHOD,$admin);

		 return view('admins.show',compact('admin'));

	}


	public function destroy(NullRequest	 $request)
	{
		
	}

	

	


}
