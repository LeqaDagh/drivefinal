<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Requests;
use Illuminate\HttpResponse;
use App\Http\Controllers\ApiController;
use Request;
use App\Http\Requests\NullRequest;
use App\Http\Requests\VehicleRequest;
use App\Http\Requests\VehicleEditRequest;
use App\Make;
use App\Mould;
use App\Vehicle;
use App\Region; 
use App\Favorite;
use App\Search;
use App\Media;
use App\Equipment;
use App\User;
use Auth;
use App\Transformers\VehicleTransformer;
use App\Transformers\SearchTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Support\Str;
use Image;

class VehiclesApiController extends ApiController
{

	private $filters = [];

	private $types = [];

	private $vehicle = NULL;

	protected $vehicleTransformer;

	protected $searchTransformer;

	protected $userTransformer;

    public function __construct(VehicleTransformer $vehicleTransformer,SearchTransformer $searchTransformer,UserTransformer $userTransformer)
	{

		 $this->vehicleTransformer = $vehicleTransformer;
		 $this->searchTransformer = $searchTransformer;
		 $this->userTransformer = $userTransformer;
	}

 public function addVehicleFiltersSearch()
 {
 	
 		
 		$this->filters['makes']   = Make::with('moulds.childs')->get();
 		$this->setTypes();
		return   $this->filters;
 }

 public function searchVehicleFilters()
 {
 		$this->filters['makes']   		= Make::with('moulds.childs')->get();
 		//$this->filters['sellers'] 		= User::seller()->get();
 		$this->filters['colors_lists'] 	= trans('types.color_types'); 
 		$this->filters['interior_colors_lists'] 	= trans('types.interior_color_types'); 
 		$this->setTypes();
		return   $this->filters;
 }
 public function addVehicleFilters()
 {
 		$this->filters['log_user'] =  $this->userTransformer->transform(Auth::user()->toArray());
 		$this->filters['makes']   		= Make::with('moulds.childs')->get();
 		$this->filters['colors_lists'] 	= trans('types.color_types'); 
 		$this->filters['interior_colors_lists'] 	= trans('types.interior_color_types'); 
 		$this->setTypes();
		return   $this->filters;
 }
  public function editVehicleFilters()
 {

 		$vehicle = Auth::user()->vehicles()->notDeleted()->findOrFail(request()->vehicle_id);
 		$equipments = $vehicle->equipments;
 		$this->filters['log_user'] =  $this->userTransformer->transform(Auth::user()->toArray());
 		$this->filters['makes']   		  = Make::with('moulds.childs')->get();
 		$this->filters['colors_lists'] 	  = trans('types.color_types'); 
 		$this->filters['interior_colors_lists'] 	  = trans('types.interior_color_types'); 
 		
 		$this->filters['selected_moulds'] = $vehicle->mould()->with('make')->first();
 		$this->setTypes();
 		$this->filters['edit_vehicle_data']=[
                          'make_id'=>$vehicle->make_id,
                          'mould_id'=>$vehicle->mould_id,
                          'body'=>$vehicle->body,
                          'extra_title'=>$vehicle->extra_title,
                          'year_of_product'=>$vehicle->year_of_product,
                          'year_of_work'=>$vehicle->year_of_work,
                          'price'=>$vehicle->price,
                          'first_payment'=>$vehicle->first_payment,
                          'price_type'=>$vehicle->price_type,
                          'power'=>$vehicle->power,
                          'hp'=>$vehicle->hp,
                          'mileage'=>$vehicle->mileage,
                          'gear'=>$vehicle->gear,
                          'fuel'=>$vehicle->fuel,
                          'drivetrain_system'=>$vehicle->drivetrain_system,
                          'drivetrain_type'=>$vehicle->drivetrain_type,
                          'num_of_seats'=>$vehicle->num_of_seats,
                          'vehicle_status'=>$vehicle->vehicle_status,
                          'body_color'=>$vehicle->body_color,
                          'interior_color'=>$vehicle->interior_color,
                          'delete_at'=>$vehicle->delete_at,

                          'customs_exemption'=>$vehicle->customs_exemption,
                          'num_of_doors'=>$vehicle->num_of_doors,
                          'license_date'=>$vehicle->license_date,
                          'previous_owners'=>$vehicle->previous_owners,
                          'origin'=>$vehicle->origin,
                          'num_of_keys'=>$vehicle->num_of_keys,
                          'desc'=>$vehicle->desc,

                          'payment_method'=>$equipments->where('equipment_type','payment_method')->pluck('equipment_value')->toArray(),
                          'ext_int_furniture'=>@$equipments->where('equipment_type','ext_int_furniture')->first()->equipment_value,
                          'ext_int_seats'=>$equipments->where('equipment_type','ext_int_seats')->pluck('equipment_value')->toArray(),
                          'ext_int_steering'=>$equipments->where('equipment_type','ext_int_steering')->pluck('equipment_value')->toArray(),
                          'ext_int_screens'=>$equipments->where('equipment_type','ext_int_screens')->pluck('equipment_value')->toArray(),
                          'ext_int_sunroof'=>@$equipments->where('equipment_type','ext_int_sunroof')->first()->equipment_value,
                          'ext_int_glass'=>$equipments->where('equipment_type','ext_int_glass')->pluck('equipment_value')->toArray(),
                          'ext_int_other'=>$equipments->where('equipment_type','ext_int_other')->pluck('equipment_value')->toArray(),
                          'ext_ext_light'=>$equipments->where('equipment_type','ext_ext_light')->pluck('equipment_value')->toArray(),
                          'ext_ext_mirrors'=>$equipments->where('equipment_type','ext_ext_mirrors')->pluck('equipment_value')->toArray(),
                          'ext_ext_rims'=>$equipments->where('equipment_type','ext_ext_rims')->first()->equipment_value,
                          'ext_ext_cameras'=>$equipments->where('equipment_type','ext_ext_cameras')->pluck('equipment_value')->toArray(),
                          'ext_ext_sensors'=>$equipments->where('equipment_type','ext_ext_sensors')->pluck('equipment_value')->toArray(),
                          'ext_ext_other'=>$equipments->where('equipment_type','ext_ext_other')->pluck('equipment_value')->toArray(),
                          'ext_gen_other'=>$equipments->where('equipment_type','ext_gen_other')->pluck('equipment_value')->toArray(),
                      ];
		return   $this->filters;

 }
 


 	private function getTypes()
 	{
 		$types = [
		 			'sell_type_types',
					'vehicle_type_types',
					'body_types',
					'gear_types',
					'fuel_types',
					'drivetrain_system_types',
					'drivetrain_type_types',
					'price_type_types',
					'payment_method_types',
					'num_of_seats_types',
					'num_of_doors_types',
					'num_of_keys_types',
					'vehicle_status_types',
					'previous_owners_types',
					'origin_types',
					'order_type_types',
					'customs_exemption_types',
					'color_types',
					'interior_color_types',
					'ext_int_furniture',
					'ext_int_seats',
					'ext_int_steering',
					'ext_int_screens',
					'ext_int_sunroof',
					'ext_int_glass',
					'ext_int_other',
					'ext_ext_light',
					'ext_ext_mirrors',
					'ext_ext_rims',
					'ext_ext_cameras',
					'ext_ext_sensors',
					'ext_ext_other',
					'ext_gen_other'
 				 ];
 		return  $types;
 	}

	private function setTypes()
	{

		$types = $this->getTypes();

		foreach ($types as $key => $type) 
		{
			$this->filters[$type] = array_keys(trans("types.".$type));
		}
		
	}

	private function appendChecked()
	{
		$types = $this->getTypes();

		foreach ($types as $key => $type) 
		{
			foreach ($this->filters[$type] as $key2 => $t) 
			{
				$this->filters[$type][$key2]['checked'] = false;


				$te = [ 'gear_types',
						'fuel_types',
						'drivetrain_system_types',
						'drivetrain_type_types',
						'payment_method_types',
						'price_type_types',
						'num_of_seats_types',
						'num_of_doors_types',
						'num_of_keys_types',
						'vehicle_status_types',
						'previous_owners_types',
						'origin_types',
						'customs_exemption_types'
						];
				if(in_array($type, $te) AND !is_null($this->vehicle) )
				{
					$ort = str_replace("_types","",$type);

					

					if($this->vehicle->$ort==$key2)
					{
						$this->filters[$type][$key2]['checked'] = true;
					}
				}		
				
			}
		}

	}




	public function addVehicle(VehicleRequest $request)
	{
	

		$user 		= Auth::user();

		$make 		= Make::findOrFail($request->make_id);

		$mould 		= Mould::findOrFail($request->mould_id);

		$vehicle 	= new \App\Vehicle();

		$request->merge(['delete_at'=> date("Y-m-d",strtotime('+2 months'))]);

		$vehicle->fill($request->all());

		$vehicle->user()->associate($user);
	
		$vehicle->make()->associate($make);

		$vehicle->mould()->associate($mould);

		$eq=[];

		$types = [
					'ext_int_sunroof',
					'ext_int_furniture',
					'payment_method',			
				 	'ext_int_steering',
				    'ext_int_screens',
				    'ext_int_glass',
				    'ext_int_other',
				    'ext_ext_light',
				    'ext_ext_mirrors',
				    'ext_ext_rims',
				    'ext_ext_cameras',
				    'ext_ext_sensors',
				    'ext_ext_other',
				    'ext_gen_other'
				];
                      

    	foreach ($types as $key => $type) 
    	{
    		# code...
    	if(is_array($request->$type))
    	{
			foreach ($request->$type as $key => $req) {
				$eq[] = new Equipment(['equipment_value'=>$req,'equipment_type'=>$type]);
			}
		}
		else
		{
				$eq[] = new Equipment(['equipment_value'=>$request->$type,'equipment_type'=>$type]);	
		}
		}

		$vehicle->save();

		$vehicle->equipments()->saveMany($eq,['equipment_value','equipment_type']);

		return $vehicle;

	}
	public function editVehicleStore($id,VehicleEditRequest $request)
	{
	
		$make 		= Make::findOrFail($request->make_id);

		$mould 		= Mould::findOrFail($request->mould_id);

		$vehicle 	= Auth::user()->vehicles()->notDeleted()->findOrFail($id);

		$vehicle->fill($request->all());
	
		$vehicle->make()->associate($make);

		$vehicle->mould()->associate($mould);

		$eq=[];

		$types = [
					'ext_int_sunroof',
		 			'ext_int_furniture',
					'payment_method',			
				 	'ext_int_steering',
				    'ext_int_screens',
				    'ext_int_glass',
				    'ext_int_other',
				    'ext_ext_light',
				    'ext_ext_mirrors',
				    'ext_ext_rims',
				    'ext_ext_cameras',
				    'ext_ext_sensors',
				    'ext_ext_other',
				    'ext_gen_other'
		 		];
                      

    	foreach ($types as $key => $type) 
    	{
    		# code...
	    	if(is_array($request->$type))
	    	{
				foreach ($request->$type as $key => $req) {
					$eq[] = new Equipment(['equipment_value'=>$req,'equipment_type'=>$type]);
				}
			}
			else
			{
					$eq[] = new Equipment(['equipment_value'=>$request->$type,'equipment_type'=>$type]);	
			}
		}

		$vehicle->save();

		$vehicle->equipments()->delete();

		$vehicle->equipments()->saveMany($eq,['equipment_value','equipment_type']);

		return $vehicle;

	}
	

	private function extraAttr()
	{

		$arr = ['is_compare','is_fav','year_of_product_min','year_of_product_max','price_min','price_max','power_min','power_max','hp_min','hp_max'];

		return $arr;
	}



	public function editVehicles($id)
	{

		$this->vehicle = Vehicle::findOrFail($id);

 		$this->filters['makes']  = Make::with('moulds.childs')->get();
 		$this->filters['sellers'] = Auth::user()->sellers;
 		$this->filters['selected_model'] = Mould::with('make')->find($this->vehicle->mould_id);
 		$this->setTypes();
 		$this->appendChecked();
 		$this->filters['submit_data'] = $this->vehicle;

 		$body_types = collect($this->filters['body_types'])->toArray();

 		$body_types[$this->vehicle['body']]['checked']=true;

 		$this->filters['body_types'] = $body_types ;



 		$this->appendCheckedEqu();
		return   $this->filters;
	}

	private function appendCheckedEqu()
	{
		$items = [	'ext_int_furniture',
					'ext_int_seats',
					'ext_int_steering',
					'ext_int_screens',
					'ext_int_sunroof',
					'ext_int_glass',
					'ext_int_other',
					'ext_ext_light',
					'ext_ext_mirrors',
					'ext_ext_rims',
					'ext_ext_cameras',
					'ext_ext_sensors',
					'ext_ext_other',
					'ext_gen_other'];


		foreach ($items as $key => $item) 
		{

			foreach (trans('types.'.$item)  as $k => $i) 
			{
				
			
				if(isset($this->filters['submit_data'][$k]))
				{
					if($this->filters['submit_data'][$k])
					{
						$this->filters[$item][$k]['checked'] = true;
					}
					

				}
			}
							
		}				


	}


	//-----------------------------------------

	public function myVehicles(NullRequest $request)
	{
		
		

 		$vehicles = Auth::user()
 							->vehicles()
 							->with(['user','make','mould','cover','photos','equipments'])	
					 		->notDeleted()
 							->latest('id')
							->paginate(5);

		return $this->respondWithPagination($vehicles, ['data' => $this->vehicleTransformer->transformCollection($vehicles->all())]);

	

	}

	public function show($id)
	{
	
		$vehicle = Vehicle::with(['cover','photos','user','mould','seller'])
							->notDeleted()
							->published()
							->findOrFail($id);
	
		$vehicle =   $this->vehicleTransformer->transform($vehicle);	



	if(is_null($vehicle['ext_int_sunroof'][0]))
	{
		unset($vehicle['ext_int_sunroof'][0]);
	}

	if(is_null($vehicle['ext_int_furniture'][0]))
	{
		unset($vehicle['ext_int_furniture'][0]);
	}
	if(is_null($vehicle['ext_ext_rims'][0]))
	{
		unset($vehicle['ext_ext_rims'][0]);
	}


		return $vehicle;

	}	


	public function getResultVehicles(NullRequest $request)
	{





		$vehicles = Vehicle::with(['user','make','mould','cover','photos','equipments'])
					 		->notDeleted()
					 		->published()
					 		->doFilters()
					 		->inSellers($request->seller_id)
							->doSort()
							->paginate(12);

		return $this->respondWithPagination($vehicles, ['data' => $this->vehicleTransformer->transformCollection($vehicles->all())]);


	}
		public function getSpecialVehicles(NullRequest $request)
	{



	$vehicles = Vehicle::with(['user','make','mould','cover','photos'])
							->notDeleted()
							->published()
							->where('stared','yes')
							->orderBy('order','ASC')
							->paginate(10);

		return $this->respondWithPagination($vehicles, ['data' => $this->vehicleTransformer->transformCollection($vehicles->all())]);
	}
	public function myFavorites(NullRequest $request)
	{



	$vehicles = Vehicle::with(['user','make','mould','cover','photos'])
							->whereIn('id',request()->ids)
							->notDeleted()
							->published()
							->orderBy('order_type','asc')
							->orderBy('order','asc')
							->paginate(10);

		return $this->respondWithPagination($vehicles, ['data' => $this->vehicleTransformer->transformCollection($vehicles->all())]);
	}
	public function mySearches(NullRequest $request)
	{
		$searches  =  Search::where('uuid',$request->uuid)->paginate(10);	
	    return $this->respondWithPagination($searches, ['data' => $this->searchTransformer->transformCollection($searches->all())]);

	}
	





public function compare(NullRequest $request)
	{


		$vehicle_one = Vehicle::with(['user','make','mould','cover','photos'])->notDeleted()->published()->findOrFail($request->vehicle_one);

		$vehicle_two = Vehicle::with(['user','make','mould','cover','photos'])->notDeleted()->published()->findOrFail($request->vehicle_two);


		$result = 
				[
					'vehicle_one'=>$this->vehicleTransformer->transform($vehicle_one),
					'vehicle_two'=>$this->vehicleTransformer->transform($vehicle_two),
				]	;		


	if(is_null($result['vehicle_two']['ext_int_sunroof'][0]))
	{
		unset($result['vehicle_two']['ext_int_sunroof'][0]);
	}
	if(is_null($result['vehicle_one']['ext_int_sunroof'][0]))
	{
		unset($result['vehicle_one']['ext_int_sunroof'][0]);
	}

	if(is_null($result['vehicle_two']['ext_int_furniture'][0]))
	{
		unset($result['vehicle_two']['ext_int_furniture'][0]);
	}
	if(is_null($result['vehicle_one']['ext_int_furniture'][0]))
	{
		unset($result['vehicle_one']['ext_int_furniture'][0]);
	}

	if(is_null($result['vehicle_two']['ext_ext_rims'][0]))
	{
		unset($result['vehicle_two']['ext_ext_rims'][0]);
	}
	if(is_null($result['vehicle_one']['ext_ext_rims'][0]))
	{
		unset($result['vehicle_one']['ext_ext_rims'][0]);
	}


		return $result;		


	}


	public function homeData(){


 		$vehicles = Vehicle::with(['user','make','mould','cover','photos'])
							->inStared(['yes'])
							->notDeleted()
							->orderBy('order','ASC')
							->published()
							->paginate(3);

		$collection = Media::with('fileable')->whereIn('id',Vehicle::whereNotNull('slider_id')->pluck('slider_id')->toArray())->get();	



		$sliders = $collection->map(function ($item) {
				$photo =[];
				$photo['id']   = $item['fileable_id']; 
				$photo['path'] = url('public/storage/'.@$item['disk_name'],@$item['file_name']);

			    return ($photo);
			});

		
					
		return $this->respondWithPagination($vehicles, ['sliders'=>$sliders,'data' => $this->vehicleTransformer->transformCollection($vehicles->all())]);

	}

	public function delete()
	{

 		$vehicle = Auth::user()->vehicles()->notDeleted()->findOrFail(request()->vehicle_id);
 		$vehicle->update(['is_deleted'=>'yes']);
 		return ['ok'];
	}
	public function deletePhoto(NullRequest $request)
	{

		$vehicle = Auth::user()->vehicles()->notDeleted()->findOrFail($request->vehicle_id);

		$photo = $vehicle->photos()->findOrFail($request->id);

		$photo->delete();

		return [true];
	}
	public function getPhotos(NullRequest $request)
	{

		$vehicle = Auth::user()->vehicles()->notDeleted()->findOrFail($request->id);

		$photos = $vehicle->photos;

		$collection = [];

		if(count($photos))
		{

			$collection = collect($photos)->map(function ($photo) 
			{
				$photo['thumb_path']   = url('public/storage/'.@$photo['disk_name'],@$photo['file_name']);

			    return ($photo);
			});
		}



		return $collection;
	}
	public function uploadPhotos(NullRequest $request)
	{
		  $vehicle = Auth::user()->vehicles()->notDeleted()->findOrFail($request->id);



			$photos  = [];

 			 if( $request->has('images'))
 			 {



		foreach ($request->images as $key => $image) {

         if (preg_match('/^data:image\/(\w+);base64,/', $image, $type)) {  

 
     	    $extra_name    = uniqid().'_'.time().'_'.uniqid().'.'.$type[1];
           $encoded_base64_image = substr($image, strpos($image, ',') + 1);

           $resized_image = Image::make($encoded_base64_image);
           $resized_image->save(storage_path('app/public/vehicles/'.$extra_name));

           		$path = storage_path('app/public/vehicles/'.$extra_name);
			    $logo = public_path('opa_logo.png'); 
			    $v_logo = public_path('v_logo.png');    
			 	$img = Image::make($path);
			    $img->insert($logo, 'bottom-left', 10, 10);
			    $pad = intval(($img->height()-500)/2);
			    $img->insert($v_logo, 'top-right',10,$pad);

			     $img->resize(800,null, function ($constraint) {
				    $constraint->aspectRatio();
				});

			    $img->save($path);


			        	 $photos[] = [
			        	 	'disk_name' =>'vehicles',
			        	 	'file_name' =>$extra_name,
			        	 	'file_size' =>11,
			        	 	'content_type'=>11,
			        	 	'title'=>$vehicle->extra_title,
			        	 	'description'=>'',
			        	 	'field'=>'photos',
			        	 	'sort_order'=>'1'
			        	 ];
			        
 }
			 
}
			        	 
	 			 
        	 }        	


		$vehicle->photos()->createMany($photos);


		if($vehicle->publish_status=="ini")
		{
			$vehicle->update(['publish_status'=>'iwp']);	
		}

		


		$photos = $vehicle->photos;

		$collection = collect($photos)->map(function ($photo) 
		{
			$photo['thumb_path']   = url('public/storage/'.@$photo['disk_name'],@$photo['file_name']);

		    return ($photo);
		});

		return $collection;

		 
	}


	public function deleteCompanyPhoto(NullRequest $request)
	{

		$user = Auth::user();

		$photo = $user->photos()->findOrFail($request->id);

		$photo->delete();

		return [true];
	}
	public function getCompanyPhotos(NullRequest $request)
	{

		$user = Auth::user();

		$photos = $user->photos;

		$collection = [];

		if(count($photos))
		{

			$collection = collect($photos)->map(function ($photo) 
			{
				$photo['thumb_path']   = url('public/storage/'.@$photo['disk_name'],@$photo['file_name']);

			    return ($photo);
			});
		}



		return $collection;
	}
	public function uploadCompanyPhotos(NullRequest $request)
	{
		  $user = Auth::user();



			$photos  = [];

 			 if( $request->has('images'))
 			 {



		foreach ($request->images as $key => $image) {

         if (preg_match('/^data:image\/(\w+);base64,/', $image, $type)) {  

 
     	   $extra_name    = uniqid().'_'.time().'_'.uniqid().'.'.$type[1];
           $encoded_base64_image = substr($image, strpos($image, ',') + 1);

           $resized_image = Image::make($encoded_base64_image);
           $resized_image->save(storage_path('app/public/sellers/'.$extra_name));

           		$path = storage_path('app/public/sellers/'.$extra_name);
			    $logo = public_path('opa_logo.png'); 
			    $v_logo = public_path('v_logo.png');    
			 	$img = Image::make($path);
			    $img->insert($logo, 'bottom-left', 10, 10);
			    $pad = intval(($img->height()-500)/2);
			    $img->insert($v_logo, 'top-right',10,$pad);

			     $img->resize(800, null, function ($constraint) {
				    $constraint->aspectRatio();
				});

			    $img->save($path);


			        	 $photos[] = [
			        	 	'disk_name' =>'sellers',
			        	 	'file_name' =>$extra_name,
			        	 	'file_size' =>11,
			        	 	'content_type'=>11,
			        	 	'title'=>@$user->name,
			        	 	'description'=>'',
			        	 	'field'=>'photos',
			        	 	'sort_order'=>'1'
			        	 ];
			        
 }
			 
}
			        	 
	 			 
        	 }        	


		$user->photos()->createMany($photos);

		

		$photos = $user->photos;

		$collection = collect($photos)->map(function ($photo) 
		{
			$photo['thumb_path']   = url('public/storage/'.@$photo['disk_name'],@$photo['file_name']);

		    return ($photo);
		});

		return $collection;

		 
	}

	public function saveSearch()
	{
		return Search::create(request()->all());
	}

	public function deleteSearch()
	{

		$search = Search::where('uuid',request()->uuid)->findOrFail(request()->id);

		$search->delete();

		return [true];	
	}


}