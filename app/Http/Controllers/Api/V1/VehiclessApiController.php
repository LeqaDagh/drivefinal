<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Requests;
use Illuminate\HttpResponse;
use App\Http\Controllers\ApiController;
use Request;
use App\Http\Requests\NullRequest;
use App\Http\Requests\VehicleRequest;
use App\Vehicle;
use App\Seller; 
use App\Region; 
use App\Favorite;
use App\Search;
use App\Type;
use App\Media;
use Auth;
use App\Attribute; 
use App\Transformers\VehicleTransformer;
use App\Transformers\SearchTransformer;
use Illuminate\Support\Str;
use App\Enums\EquipmentsTypes;

class VehiclesApiController extends ApiController
{
	protected $vehicleTransformer;

    public function __construct(VehicleTransformer $vehicleTransformer,SearchTransformer $searchTransformer)
	{

		 $this->vehicleTransformer = $vehicleTransformer;
		 $this->searchTransformer  = $searchTransformer;
		 
	}
	public function deletePhoto(NullRequest $request)
	{
		$media = Media::findOrFail($request->id);

		$media->delete();

		return [true];
	}
	public function getPhotos(NullRequest $request)
	{
		$vehicle = Vehicle::findOrFail($request->id);

		$photos = $vehicle->photos;

		$collection = collect($photos)->map(function ($photo) 
		{
			$photo['thumb_path']   = url('public/storage/'.@$photo['disk_name'],@$photo['file_name']);

		    return ($photo);
		});

		return $collection;
	}

	public function uploadPhotos(NullRequest $request)
	{
		$vehicle = Vehicle::findOrFail($request->id);

			 	 $photos  = [];

 			 if( $request->hasFile('photos'))
 			 {
	 			
	 					 $file = $request->file('photos');

						 $original_name = $file->getClientOriginalName();
						 $extra_name    = uniqid().'_'.time().'_'.uniqid().'.'.$file->extension();
			        	 $file->storeAs('vehicles', $extra_name);
			        	 $photos[] = [
			        	 	'disk_name' =>'vehicles',
			        	 	'file_name' =>$extra_name,
			        	 	'file_size' =>$file->getSize(),
			        	 	'content_type'=>$file->getClientMimeType(),
			        	 	'title'=>$vehicle->extra_title,
			        	 	'description'=>'',
			        	 	'field'=>'photos',
			        	 	'sort_order'=>'1'
			        	 ];

			 

			        	 
	 			 
        	 }        	


		$vehicle->photos()->createMany($photos);

		return $vehicle;
	}

	public function edit($id)
	{

		$vehicle = Vehicle::findOrFail($id);

		$attributes 			= [];
		
		$types = Type::with('attributes.childs.childs')->get();
		foreach ($types as $key => $type) 
		{
			
			$filters[$type->search_field] = $type->toArray();
		}
		$filters['sell_type'] 			= trans('types.sell_type_types');
		$filters['vehicle_type'] 		= trans('types.vehicle_type_types');
		$filters['body'] 				= trans('types.body_types');
		$filters['gear'] 				= trans('types.gear_types');
		$filters['fuel'] 				= trans('types.fuel_types');
		$filters['drivetrain_system'] 	= trans('types.drivetrain_system_types');
		$filters['drivetrain_type'] 	= trans('types.drivetrain_type_types');
		$filters['payment_method'] 		= trans('types.payment_method_types');
		$filters['num_of_seats'] 		= trans('types.num_of_seats_types');
		$filters['vehicle_status'] 		= trans('types.vehicle_status_types');
		$filters['customs_exemption'] 	= trans('types.customs_exemption_types');
		$filters['previous_owners'] 	= trans('types.previous_owners_types');
		$filters['origin'] 				= trans('types.origin_types');
		$filters['colors'] 				= trans('types.color_types');

		
		
		$arr = [];

		foreach ($filters['body'] as $key => $body) 
		{
		
			$att = $body;
		
			$att['logo'] = url('public/cars',$att['logo']);

			$att['alt_logo'] = url('public/cars',$att['alt_logo']);

			$att['title']  = strtoupper($att['title']);

			$arr [] = $att; 
		}

		$filters['body'] = $arr;



		$filters['vehicle'] = $vehicle;
		$filters['vehicle']['extra_equipments']    = $vehicle->extra_equipments()->pluck('attribute_id')->toArray();
		$filters['vehicle']['interior_equipments'] = $vehicle->interior_equipments()->pluck('attribute_id')->toArray();
		$filters['vehicle']['exterior_equipments'] = $vehicle->exterior_equipments()->pluck('attribute_id')->toArray();


		return $filters;                    
                       
	}
	public function store(){

	}
	public function editVehicleStore($id,VehicleRequest $request)
	{

		$vehicle = Vehicle::findOrFail($id);

		$seller = Seller::findOrFail($request->seller_id);

		$make = Attribute::findOrFail($request->make_id);

		$model = Attribute::findOrFail($request->model_id);

		$vehicle ->fill($request->all());

		$vehicle->seller()->associate($seller);
	
		$vehicle->make()->associate($make);

		$vehicle->model()->associate($model);

		$vehicle->save();

		$equipment = [];

		if(!is_null($request->ext_int_furniture)){
			 array_merge($equipment,[$request->ext_int_furniture]);
		} 
		if(!is_null($request->ext_int_sunroof)){
			 array_merge($equipment,[$request->ext_int_sunroof]);
		}



		$equipments = array_merge(
									$request->ext_int_steering,
									$request->ext_int_seats,
									$request->ext_int_screens,
									$request->ext_int_glass,
									$request->ext_int_other,
									$request->ext_ext_light,
									$request->ext_ext_mirrors,
									$request->ext_ext_rims,
									$request->ext_ext_cameras,
									$request->ext_ext_sensors,
									$request->ext_ext_other,
									$request->ext_gen_other
		);

		if(count($equipments))
		{  
			foreach($equipments as $eq)
			{
			 $equipment = new \App\Equipment();
			 $equipment->item = $eq;
			 $equipment->vehicle()->associate($vehicle);	
			 $equipment->save();
			}
		}  

		return $vehicle;                 
	}
	public function delete(NullRequest $request)
	{
		$vehicle = Vehicle::findOrFail($request->id);
		$vehicle->update(['is_deleted'=>'y']);
		return [1];
	}
	public function index(NullRequest $request)
	{
	
		$vehicles = Vehicle::with(['seller','make','model','extra_equipments','interior_equipments','exterior_equipments','cover','photos'])
							->doFilters($request->all())
							->orderBy('order_type','asc')
							->orderBy('order','asc')
							->LikeName($request->name)
							->PriceBetween($request->price_min,$request->price_max)
							->YearBetween($request->year_min,$request->year_max)
							->PowerBetween($request->power_min,$request->power_max)
							->EqualNumOfSeat($request->num_of_seat)
							->EqualBodyColor($request->body_color)
							->EqualPreviousWwners($request->previous_owners)
							->paginate(10);

		return $this->respondWithPagination($vehicles, ['data' => $this->vehicleTransformer->transformCollection($vehicles->all())]);

	

	}

		public function myVehicles(NullRequest $request)
	{
		

		$vehicles = Vehicle::with(['seller','make','model','equipments','cover','photos'])
							->doFilters($request->all())
							->orderBy('order_type','asc')
							->orderBy('order','asc')
							->LikeName($request->name)
							// ->PriceBetween($request->price_min,$request->price_max)
							// ->YearBetween($request->year_min,$request->year_max)
							// ->PowerBetween($request->power_min,$request->power_max)
							// ->EqualNumOfSeat($request->num_of_seat)
							// ->EqualBodyColor($request->body_color)
							// ->EqualPreviousWwners($request->previous_owners)
							->paginate(10);

		return $this->respondWithPagination($vehicles, ['data' => $this->vehicleTransformer->transformCollection($vehicles->all())]);

	

	}


		public function myFavorites(NullRequest $request)
	{
		

		$vehicles = Vehicle::with(['seller','make','model','equipments','cover','photos'])
							->doFilters($request->all())
							->orderBy('order_type','asc')
							->orderBy('order','asc')
							->LikeName($request->name)
							->PriceBetween($request->price_min,$request->price_max)
							->YearBetween($request->year_min,$request->year_max)
							->PowerBetween($request->power_min,$request->power_max)
							->EqualNumOfSeat($request->num_of_seat)
							->EqualBodyColor($request->body_color)
							->EqualPreviousWwners($request->previous_owners)
							->paginate(10);

		return $this->respondWithPagination($vehicles, ['data' => $this->vehicleTransformer->transformCollection($vehicles->all())]);

	

	}


	public function addVehicle(VehicleRequest $request)
	{
	

		$seller = Seller::findOrFail($request->seller_id);

		$make = Attribute::findOrFail($request->make_id);

		$model = Attribute::findOrFail($request->model_id);

		$vehicle = new \App\Vehicle($request->all());

		$vehicle->seller()->associate($seller);
	
		$vehicle->make()->associate($make);

		$vehicle->model()->associate($model);

		$vehicle->save();

		$equipment = [];

		if(!is_null($request->ext_int_furniture)){
			 array_merge($equipment,[$request->ext_int_furniture]);
		} 
		if(!is_null($request->ext_int_sunroof)){
			 array_merge($equipment,[$request->ext_int_sunroof]);
		}



		$equipments = array_merge(
									$request->ext_int_steering,
									$request->ext_int_seats,
									$request->ext_int_screens,
									$request->ext_int_glass,
									$request->ext_int_other,
									$request->ext_ext_light,
									$request->ext_ext_mirrors,
									$request->ext_ext_rims,
									$request->ext_ext_cameras,
									$request->ext_ext_sensors,
									$request->ext_ext_other,
									$request->ext_gen_other
		);

		if(count($equipments))
		{  
			foreach($equipments as $eq)
			{
			 $equipment = new \App\Equipment();
			 $equipment->item=$eq;
			 $equipment->vehicle()->associate($vehicle);	
			 $equipment->save();
			}
		}


		 return $vehicle;
	

	}
	public function updateVehicle(VehicleRequest $request)
	{
	

		$seller = Seller::findOrFail($request->seller_id);

		$make = Attribute::findOrFail($request->make_id);

		$model = Attribute::findOrFail($request->model_id);

		$vehicle = Vehicle::findOrFail($request->id);

		$vehicle->fill($request->all());

		$vehicle->seller()->associate($seller);
	
		$vehicle->make()->associate($make);

		$vehicle->model()->associate($model);

		$vehicle->save();

		$equipments = array_merge(
									$request->ext_int_furniture,
									$request->ext_int_seats,
									$request->ext_int_steering,
									$request->ext_int_screens,
									$request->ext_int_sunroof,
									$request->ext_int_glass,
									$request->ext_int_other,
									$request->ext_ext_light,
									$request->ext_ext_mirrors,
									$request->ext_ext_rims,
									$request->ext_ext_cameras,
									$request->ext_ext_sensors,
									$request->ext_ext_other,
									$request->ext_gen_other
		);

		
		   
                          
		foreach($equipments as $eq)
		{
		 $equipment = new \App\Equipment();
		 $equipment->item=$eq;
		 $equipment->vehicle()->associate($vehicle);	
		 $equipment->save();
		}


		return $vehicle;
	

	}

	public function show($id)
	{
	
		$vehicle = Vehicle::with(['cover','photos','seller','model'])
							->findOrFail($id);
	
		return  $this->vehicleTransformer->transform($vehicle);	

	

	}	

	public function myFavoritesold(NullRequest $request)
	{
	

		$vehicles = Vehicle::with(['seller','make','model','equipments','equipments','cover','photos'])
							->doFilters($request->all())
							->orderBy('order_type','asc')
							->orderBy('order','asc')
							->LikeName($request->name)
							->PriceBetween($request->price_min,$request->price_max)
							->YearBetween($request->year_min,$request->year_max)
							->PowerBetween($request->power_min,$request->power_max)
							->EqualNumOfSeat($request->num_of_seat)
							->EqualBodyColor($request->body_color)
							->EqualPreviousWwners($request->previous_owners)
							->paginate(10);

		return $this->respondWithPagination($vehicles, ['data' => $this->vehicleTransformer->transformCollection($vehicles->all())]);

	}
	public function addToFavorites(NullRequest $request)
	{


 		 $vehicle = Vehicle::findOrFail($request->vehicle_id);

		 $user = Auth::user();

		 $favorite = Favorite::create([]);

		 $favorite->vehicle()->associate($vehicle);

		 $favorite->user()->associate($user);

		 $favorite->save();

		 return $favorite;

	}
	public function removeFromFavorites(NullRequest $request)
	{
		$favorite = Auth::user()->favorites()->findOrFail($request->id);

		$favorite->delete();

	}

	public function mySearches(NullRequest $request)
	{
		$searchs = Auth::user()->searches()->paginate(10);

		return $this->respondWithPagination($searchs, ['data' => $this->searchTransformer->transformCollection($searchs->all())]);

	}
	public function addToSearches(NullRequest $request)
	{

		 $search = Auth::user()->searches()->create(['text'=>$request->all()]);

		 return $search;

	}
	public function removeFromSearches(NullRequest $request)
	{
		$search = Auth::user()->searches()->findOrFail($request->id);
		
		$search->delete();

	}
	public function compare(NullRequest $request)
	{


		$vehicle_one = Vehicle::findOrFail(1);

		$vehicle_two = Vehicle::findOrFail(2);


		$result = 
				[
					'vehicle_one'=>$this->vehicleTransformer->transform($vehicle_one),
					'vehicle_two'=>$this->vehicleTransformer->transform($vehicle_two),
				]	;		

		return $result;		

	}

	private function serializeData($data)
	{
		$ides = []; 

			foreach ($data['attributes'] as $key => $attribute) 
			{
				if($attribute['checked'])
				{
				  $ides[] = $attribute['id'];	
				}
				
				if(count($attribute['attributes']))
				{
					foreach ($attribute['attributes'] as $sub_key => $sub_attribut) 
					{
							if($sub_attribut['checked'])
							{
							  $ides[] = $sub_attribut['id'];	
							}
					}
				}

			}

		return $ides;
	}
	
	
}

