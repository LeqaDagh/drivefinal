<?php

namespace App\Http\Controllers;

use App\User;

use App\Activity;

use App\Region;

use App\Station; 

use App\Role; 

use App\Account; 

use App\Seller; 

use App\Attribute; 

use App\Http\Requests;

use App\Http\Vehicle;

use App\Http\Requests\ClientRequest;

use App\Http\Requests\NullRequest;

use App\Http\Requests\PhotosRequest;

use App\Http\Requests\SellerRequest;

use App\Http\Requests\VehicleRequest;

use Illuminate\HttpResponse;

use Request;

use Auth;

use DB;

use PDF;

use App\Enums\UserGroups;

use Illuminate\Support\Str;

use App\Enums\EquipmentsTypes;


class ClientsController extends Controller
{
    
	private $METHOD ;

	private $role ;

	private $group = UserGroups::CLIENT; 

  	public function __construct()
	{
			 $this->role = trans('main.user_groups')[$this->group]['default_role'];

	}




	private function getClients(NullRequest $request)
	{
		$clients = User::client()
					  ->hisOwen()
 		   			  ->equalRegion($request->get('region_id'))
			          ->likeUsername($request->get('username'))
			          ->likeName($request->get('name'))
			          ->likeEmail($request->get('email'))
			          ->likeMobile($request->get('mobile'))
			          ->equalUserStatus($request->get('user_status'));

		return $clients;		          

	}
    public function index(NullRequest $request)
	{

		$paginate  = 10;
		if($request->pagination)
		{
			$paginate  = $request->pagination;	
		}

		if($paginate>2000)
		{
			$paginate = 2000;
		}


 		 $clients = $this->getClients($request)->paginate($paginate);


		 $regions 	= Region::pluck('name','id');
		 $regions->prepend(trans('main.all'),0);

		 $user_status = collect(trans('main.user_status'));
		 $user_status->prepend(trans('main.all'),0);

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('clients.index',compact('clients',"regions","user_status"));
	}

    public function pdf(NullRequest $request)
	{
		$paginate  = 10;

		if($request->pagination)
		{
			$paginate  = $request->pagination;	
		}

		if($paginate>2000)
		{
			$paginate = 2000;
		}


 		$clients = $this->getClients($request)->paginate($paginate);


		 $view = \View::make('clients.pdf')->with(['clients'=>$clients]);
         $html = $view->render();
		 PDF::SetFooterMargin(40);
         PDF::SetTitle('طباعة تقرير');
         PDF::AddPage();
         PDF::SetAutoPageBreak(TRUE, 40);
         PDF::setRTL(true);
         PDF::SetFont('dejavusans', '', 10); 
         PDF::writeHTML($html, true, false, true, false, '');
       	 ob_end_clean();
         PDF::Output(time(). '_' . rand(1,999) . '_' .'clients.pdf');

	}

	public function create()
	{


		 $regions 	= Region::whereNull('parent')->pluck('name','id');

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('clients.create',compact("regions"));	
	}

	public function store(ClientRequest $request)
	{


		$region = Region::find($request->region_id);

		$role = Role::find($this->role);

		$request->merge(['group' => $this->group]);

		$client = new \App\User($request->all());

		$client->region()->associate($region);

		$client->save();

		$client->roles()->sync([@$role->id]);

		return redirect('clients');	
	}

	public function edit($id)
	{
		 $client 		= User::client()->hisOwen()->findOrFail($id);

		 $regions 	= Region::whereNull('parent')->pluck('name','id');	

		 return view('clients.edit',compact('client','regions'));	
	}
	public function update($id,ClientRequest $request)
	{
	

		$region = Region::find($request->region_id);

		$client = User::client()->hisOwen()->findOrFail($id);

		$client->fill($request->all());

		$client->region()->associate($region);


		$client->save();
		
		return redirect('clients/'.$id.'/edit');
		
	}

	public function show($id)
	{
		$client = User::client()->hisOwen()->findOrFail($id);

		Auth::user()->recordUserActivity($this->METHOD,$client);

		 return view('clients.show',compact('client'));

	}


	public function destroy($id,NullRequest	 $request)
	{
		$client = User::client()->hisOwen()->findOrFail($id);
		$client->delete();
	}

	public function addSellerForClient(NullRequest $request)
	{
		$client = User::client()->hisOwen()->findOrFail($request->client_id);

		$regions = Region::all()->pluck('name','id');
		$regions->prepend(trans('main.choose'),0);

		$publish_status = collect(trans('main.one_two_status'));
		$can_ignor_price = collect(trans('main.one_two_status'));

		

		return view('clients.js.show.add_seller_for_client_modal',compact('client','regions','publish_status','can_ignor_price'));
	}
	public function addSellerForClientPost(SellerRequest $request,PhotosRequest $photos_request)
	{

		
		$client = User::client()->hisOwen()->findOrFail($request->client_id);

		$region = Region::find($request->region_id);

		$slug = Str::slug($request->name, '_');

		$request->merge(['slug'=>$slug]);

		$seller = new \App\Seller($request->all());

		$seller->user()->associate($client);

		$seller->region()->associate($region);

		$seller->save();

		 $photos  = [];

 			 if( $request->hasFile('photos'))
 			 {
	 			 foreach ($request->file('photos') as $key => $file) 
	 			 {
						 $original_name = $file->getClientOriginalName();
						 $extra_name    = uniqid().'_'.time().'_'.uniqid().'.'.$file->extension();
			        	 $file->storeAs('sellers', $extra_name);
			        	 $photos[] = [
			        	 	'disk_name' =>'sellers',
			        	 	'file_name' =>$extra_name,
			        	 	'file_size' =>$file->getSize(),
			        	 	'content_type'=>$file->getClientMimeType(),
			        	 	'title'=>$request->name,
			        	 	'description'=>'',
			        	 	'field'=>'photos',
			        	 	'sort_order'=>'1'
			        	 ];

			 

			        	 
	 			 }	
        	 }
        	






		$seller->photos()->createMany($photos);

		return $seller;
	}
	
	public function addVehicleForSeller(NullRequest $request)
	{
		$seller = Seller::findOrFail($request->seller_id);

		$regions = Region::all()->pluck('name','id');
		$regions->prepend(trans('main.choose'),0);

		$sell_types = Attribute::where('type_id',$this->getIdForType('sell_type_id'))->get()->pluck('name','id');
		$sell_types->prepend(trans('main.choose'),0);

		$vehicle_types = Attribute::where('type_id',$this->getIdForType('vehicle_type_id'))->get()->pluck('name','id');
		$vehicle_types->prepend(trans('main.choose'),0);

		$makes = Attribute::where('type_id',$this->getIdForType('make_id'))->get()->pluck('name','id');
		$makes->prepend(trans('main.choose'),0);

		$models = Attribute::where('type_id',$this->getIdForType('model_id'))->get()->pluck('name','id');
		$models->prepend(trans('main.choose'),0);

		$gears = Attribute::where('type_id',$this->getIdForType('gear_id'))->get()->pluck('name','id');
		$gears->prepend(trans('main.choose'),0);

	
		$body_types = Attribute::where('type_id',$this->getIdForType('body_type_id'))->get()->pluck('name','id');
		$body_types->prepend(trans('main.choose'),0);

		$fuel_types = Attribute::where('type_id',$this->getIdForType('fuel_type_id'))->get()->pluck('name','id');
		$fuel_types->prepend(trans('main.choose'),0);

		$vehicle_status = Attribute::where('type_id',$this->getIdForType('vehicle_status_id'))->get()->pluck('name','id');
		$vehicle_status->prepend(trans('main.choose'),0);

		

		$payment_method = Attribute::where('type_id',$this->getIdForType('payment_method_id'))->get()->pluck('name','id');
		$payment_method->prepend(trans('main.choose'),0);

		$origin = Attribute::where('type_id',$this->getIdForType('origin_id'))->get()->pluck('name','id');
		$origin->prepend(trans('main.choose'),0);

		$order_types =  Attribute::where('type_id',$this->getIdForType('order_type_id'))->get()->pluck('name','id');
		$order_types->prepend(trans('main.choose'),0);

		$extra_equipments =  Attribute::where('type_id',$this->getIdForType('extra_equipments_id'))->get()->pluck('name','id');

		$interior_equipments =  Attribute::where('type_id',$this->getIdForType('interior_equipments_id'))->get()->pluck('name','id');

		$exterior_equipments =  Attribute::where('type_id',$this->getIdForType('exterior_equipments_id'))->get()->pluck('name','id');
	
		$published = collect(trans('main.one_two_status'));
		$published->prepend(trans('main.choose'),0);

		$customs_exemption = collect(trans('main.one_two_status'));
		$customs_exemption->prepend(trans('main.choose'),0);

		
		

		return view('clients.js.show.add_vehicle_for_seller_modal',
			compact('seller','regions','sell_types','vehicle_types','makes','models','gears','body_types','fuel_types','vehicle_status','payment_method','origin','order_types','published','customs_exemption','extra_equipments','interior_equipments','exterior_equipments'));
	}

	

	public function addVehicleForSellerPost(VehicleRequest $request,PhotosRequest $photos_request)
	{
	



		$seller = Seller::findOrFail($request->seller_id);

		$region = Region::findOrFail($request->region_id);

		$sell_type = Attribute::findOrFail($request->sell_type_id);

		$vehicle_type = Attribute::findOrFail($request->vehicle_type_id);

		$make = Attribute::findOrFail($request->make_id);

		$model = Attribute::findOrFail($request->model_id);

		$gear = Attribute::findOrFail($request->gear_id);

		$body_type = Attribute::findOrFail($request->body_type_id);

		$fuel_type = Attribute::findOrFail($request->fuel_type_id);

		$payment_method= Attribute::findOrFail($request->payment_method_id);

		$origin = Attribute::findOrFail($request->origin_id);

		$vehicle_status= Attribute::findOrFail($request->vehicle_status_id);

		$order_type = Attribute::findOrFail($request->order_type_id);

		$slug = Str::slug($request->name, '_');

		$request->merge(['slug'=>$slug]);

		$vehicle = new \App\Vehicle($request->all());

		$vehicle->seller()->associate($seller);

		$vehicle->region()->associate($region);

		$vehicle->sell_type()->associate($sell_type);

		$vehicle->vehicle_type()->associate($vehicle_type);

		$vehicle->make()->associate($make);

		$vehicle->model()->associate($model);

		$vehicle->gear()->associate($gear);

		$vehicle->body_type()->associate($body_type);

		$vehicle->fuel_type()->associate($fuel_type);

		$vehicle->payment_method()->associate($payment_method);

		$vehicle->origin()->associate($origin);

		$vehicle->vehicle_status()->associate($vehicle_status);

		$vehicle->order_type()->associate($order_type);

		$vehicle->save();

		$vehicle->exterior_equipments()->attach($request->exterior_equipments_id,['type'=>EquipmentsTypes::EXTRA]);

		$vehicle->interior_equipments()->attach($request->interior_equipments_id,['type'=>EquipmentsTypes::INTERIOR]);
		
		$vehicle->extra_equipments()->attach($request->extra_equipments_id,['type'=>EquipmentsTypes::EXTERIOR]);

		 	 $photos  = [];

 			 if( $request->hasFile('photos'))
 			 {
	 			 foreach ($request->file('photos') as $key => $file) 
	 			 {
						 $original_name = $file->getClientOriginalName();
						 $extra_name    = uniqid().'_'.time().'_'.uniqid().'.'.$file->extension();
			        	 $file->storeAs('vehicles', $extra_name);
			        	 $photos[] = [
			        	 	'disk_name' =>'vehicles',
			        	 	'file_name' =>$extra_name,
			        	 	'file_size' =>$file->getSize(),
			        	 	'content_type'=>$file->getClientMimeType(),
			        	 	'title'=>$request->name,
			        	 	'description'=>'',
			        	 	'field'=>'photos',
			        	 	'sort_order'=>'1'
			        	 ];

			 

			        	 
	 			 }	
        	 }
        	




		$vehicle->photos()->createMany($photos);

		return $vehicle;
	}


	public function showVehicleForSeller(NullRequest $request)
	{
		$seller = Seller::findOrFail($request->seller_id);

		return view('clients.js.show.show_vehicle_for_seller_modal',compact('seller'));
	}
	public function getIdForType($field)
	{
  		return DB::table('types')->select('id')->where('search_field',$field)->first()->id;
	}

}
