<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Model\Equipment;
use App\Model\Make;
use App\Model\Media;
use App\Model\Models;
use App\User;
use App\Model\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CompareController extends Controller
{
 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the search page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $makes = Make::all();
        $vehicles =  Vehicle::where('is_deleted', 'no')
            ->join('makes', 'vehicles.make_id', '=', 'makes.id')
            ->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
            ->select('vehicles.*', 'makes.name', 'moulds.name as model_name')
            ->get();

        return view('pages.compare.index', compact('makes', 'vehicles'));
    }

    /**
     * Search for vehicles.
     *
     * @param  Illuminate\Http\Request  $request
     * @return \App\Vehicle
     */
    protected function store(Request $request)
    {
        $query = Vehicle::query()->where('is_deleted', 'no');

        $makeId1 = $request->make_id1;
        
        if ($makeId1) {
            $query->join('makes', 'makes.id' , '=', 'vehicles.make_id')
                ->where('makes.id', '=', $makeId1);
        }

        if($request->has('body1')) {
            $query->where(function($query) use ($request) {
                foreach ($request->body1 as $body1) {
                    $query->orWhere('body', $body1);
                }
            });
        }

        $modelId1 = $request['model_id1'];
        if ($modelId1) {
            $query->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
                ->where('moulds.id', '=', $modelId1);
        }

        $yearFrom1 = $request['range_year_from1'];
        $yearTo1 = $request['range_year_to1'];
        if ($yearFrom1 && $yearTo1 ) {
            $query->whereBetween('year_of_product', [$yearFrom1, $yearTo1]);
        }
        
        $priceFrom1 = $request['range_price_from1'];
        $priceTo1 = $request['range_price_to1'];
        if ($priceFrom1 && $priceTo1 ) {
            $query->whereBetween('price', [$priceFrom1, $priceTo1]);
        }
  
        if($request->has('gears')) {
            $query->where(function($query) use ($request) {
                foreach ($request->gears as $gear) {
                    $query->orWhere('gear', $gear);
                }
            });
        }
       
        if($request->has('fuel1')) {
            $query->where(function($query) use ($request) {
                foreach ($request->fuel1 as $fuel) {
                    $query->orWhere('fuel', $fuel);
                }
            });
        }
        
        if( $request->has('make_id1') && !empty($request->make_id1) && $request->has('model_id1') && 
                !empty($request->model_id1)) {

            $vehicles = $query->select('vehicles.*', 'makes.name', 'moulds.name as model_name', 'file_name')
                ->leftJoin('media', function($query) {
                    $query->on('vehicles.id','=','media.fileable_id')
                    ->whereRaw('media.id IN (select MAX(a2.id) from media as a2 join vehicles as u2 on u2.id = a2.fileable_id group by u2.id)');
                })
                ->get();
        } else if ($request->has('make_id1') && !empty($request->make_id1)) {

            $vehicles = $query->join('moulds', 'moulds.id', '=', 'vehicles.mould_id')
                ->leftJoin('media', function($query) {
                    $query->on('vehicles.id','=','media.fileable_id')
                    ->whereRaw('media.id IN (select MAX(a2.id) from media as a2 join vehicles as u2 on u2.id = a2.fileable_id group by u2.id)');
                })
                ->select('vehicles.*', 'makes.name', 'moulds.name as model_name', 'file_name')
                ->get();

        } else  {
            $vehicles = $query->join('makes', 'vehicles.make_id', '=', 'makes.id')
                ->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
                ->select('vehicles.*', 'makes.name', 'moulds.name as model_name', 'file_name')
                ->leftJoin('media', function($query) {
                    $query->on('vehicles.id','=','media.fileable_id')
                    ->whereRaw('media.id IN (select MAX(a2.id) from media as a2 join vehicles as u2 on u2.id = a2.fileable_id group by u2.id)');
                })
                ->get();
        }

        return $vehicles;

    }

    /**
     * Search for vehicles.
     *
     * @param  Illuminate\Http\Request  $request
     * @return \App\Vehicle
     */
    protected function secondCompare(Request $request)
    {
        $query2 = Vehicle::query()->where('is_deleted', 'no');

        $makeId2 = $request['make_id2'];
        if ($makeId2) {
            $query2->join('makes', 'makes.id' , '=', 'vehicles.make_id')
                ->where('makes.id', '=', $makeId2);
        }

        if($request->has('body2')) {
            $query2->where(function($query2) use ($request) {
                foreach ($request->body2 as $body2) {
                    $query2->orWhere('body', $body2);
                }
            });
        }

        $modelId2 = $request['model_id2'];
        if ($modelId2) {
            $query2->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
                ->where('moulds.id', '=', $modelId2);
        }
        $yearFrom2 = $request['range_year_from2'];
        $yearTo2 = $request['range_year_to2'];
        if ($yearFrom2) {
            $query2->whereBetween('year_of_product', [$yearFrom2, $yearTo2]);
        }
        $priceFrom2 = $request['range_price_from2'];
        $priceTo2 = $request['range_price_to2'];
        if ($priceFrom2 && $priceTo2 ) {
            $query2->whereBetween('price', [$priceFrom2, $priceTo2]);
        }
        if($request->has('gear2')) {
            $query2->where(function($query2) use ($request) {
                foreach ($request->gear2 as $gear) {
                    $query2->orWhere('gear', $gear);
                }
            });
        }
        
        if($request->has('fuel2')) {
            $query2->where(function($query2) use ($request) {
                foreach ($request->fuel2 as $fuel) {
                    $query2->orWhere('fuel', $fuel);
                }
            });
        }
        
        if( $request->has('make_id2') && !empty($request->make_id2) && $request->has('model_id2') && 
                !empty($request->model_id2)) {

            $vehicles2 = $query2->select('vehicles.*', 'makes.name', 'moulds.name as model_name', 'file_name')
                ->leftJoin('media', function($query2) {
                    $query2->on('vehicles.id','=','media.fileable_id')
                    ->whereRaw('media.id IN (select MAX(a2.id) from media as a2 join vehicles as u2 on u2.id = a2.fileable_id group by u2.id)');
                })
                ->get();
        } else if ($request->has('make_id2') && !empty($request->make_id2)) {

            $vehicles2 = $query2->join('moulds', 'moulds.id', '=', 'vehicles.mould_id')
                ->leftJoin('media', function($query2) {
                    $query2->on('vehicles.id','=','media.fileable_id')
                    ->whereRaw('media.id IN (select MAX(a2.id) from media as a2 join vehicles as u2 on u2.id = a2.fileable_id group by u2.id)');
                })
                ->select('vehicles.*', 'makes.name', 'moulds.name as model_name', 'file_name')
                ->get();

        } else  {
            $vehicles2 = $query2->join('makes', 'vehicles.make_id', '=', 'makes.id')
                ->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
                ->select('vehicles.*', 'makes.name', 'moulds.name as model_name', 'file_name')
                ->leftJoin('media', function($query2) {
                    $query2->on('vehicles.id','=','media.fileable_id')
                    ->whereRaw('media.id IN (select MAX(a2.id) from media as a2 join vehicles as u2 on u2.id = a2.fileable_id group by u2.id)');
                })
                ->get();
        }

        return $vehicles2;
    }

     /**
     * Compare two vehicles.
     *
     * @param  int $id1, $id2
     * @return \App\Vehicle
     */
    protected function compare(Request $request)
    {
        $id1 = $request->id1;
        $id2 = $request->id2;

        if($id1 == 0  || $id2 == 0) {

            $canNotCompare = "canNotCompare";
            //return response()->json($canNotCompare);
            return view('pages.compare.result', compact('canNotCompare'));
        } else {
        
            $vehicle = Vehicle::findOrFail($id1);
            $user = User::where("id", "=", $vehicle->user_id)->get();
            $photos = Media::where('fileable_id', '=', $id1)->get();
            $furniture = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_int_furniture')
                ->get();
            $sunroof = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_int_sunroof')
                ->get();
            $rims = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_ext_rims')
                ->get();
    
            $sensors = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_ext_sensors')
                ->get();
            $sensors = $sensors->toArray(); 
            $sensors = array_column($sensors, 'equipment_value');
            $sensors =  implode(',', $sensors);
    
            $screens = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_int_screens')
                ->get();
            $screens = $screens->toArray(); 
            $screens = array_column($screens, 'equipment_value');
            $screens =  implode(',', $screens);
    
            $seats = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_int_seats')
                ->get();
            $seats = $seats->toArray(); 
            $seats = array_column($seats, 'equipment_value');
            $seats =  implode(',', $seats);
            
            
            $steering = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_int_steering')
                ->get();
            $steering = $steering->toArray(); 
            $steering = array_column($steering, 'equipment_value');
            $steering =  implode(',', $steering);
    
            $glass = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_int_glass')
                ->get();
            $glass = $glass->toArray(); 
            $glass = array_column($glass, 'equipment_value');
            $glass =  implode(',', $glass);
    
            $intOthers = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_int_other')
                ->get();
            $intOthers = $intOthers->toArray(); 
            $intOthers = array_column($intOthers, 'equipment_value');
            $intOthers =  implode(',', $intOthers);
    
            $lights = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_ext_light')
                ->get();
            $lights = $lights->toArray(); 
            $lights = array_column($lights, 'equipment_value');
            $lights =  implode(',', $lights);
    
            $mirrors = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_ext_mirrors')
                ->get();
            $mirrors = $mirrors->toArray(); 
            $mirrors = array_column($mirrors, 'equipment_value');
            $mirrors =  implode(',', $mirrors);
    
            $cameras = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_ext_cameras')
                ->get();
            $cameras = $cameras->toArray(); 
            $cameras = array_column($cameras, 'equipment_value');
            $cameras =  implode(',', $cameras);
    
            $genOther = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_gen_other')
                ->get();
            $genOther = $genOther->toArray(); 
            $genOther = array_column($genOther, 'equipment_value');
            $genOther =  implode(',', $genOther);
    
            $extOther = Equipment::where('vehicle_id', '=', $id1)
                ->where('equipment_type', '=', 'ext_ext_other')
                ->get();
            $extOther = $extOther->toArray(); 
            $extOther = array_column($extOther, 'equipment_value');
            $extOther =  implode(',', $extOther);
            $collection = [];
          
            if(count($photos)) {
                $collection = collect($photos)->map(function ($photo) {
                    $photo['thumb_path']   = url('public/storage/'.@$photo['disk_name'],@$photo['file_name']);
    
                    return ($photo);
                });
            }
            
            $make =  DB::table('vehicles')
                ->join('makes', 'vehicles.make_id', '=', 'makes.id')
                ->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
                ->select('makes.name', 'makes.logo', 'moulds.name as model_name')
                ->where('vehicles.id', '=', $id1)
                ->get(); 
    
    
            $vehicle2 = Vehicle::findOrFail($id2);
            $user2 = User::where("id", "=", $vehicle->user_id)->get();
            $photos2 = Media::where('fileable_id', '=', $id2)->get();
            $furniture2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_int_furniture')
                ->get();
            $sunroof2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_int_sunroof')
                ->get();
            $rims2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_ext_rims')
                ->get();
    
            $sensors2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_ext_sensors')
                ->get();
            $sensors2 = $sensors2->toArray(); 
            $sensors2 = array_column($sensors2, 'equipment_value');
            $sensors2 =  implode(',', $sensors2);
    
            $screens2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_int_screens')
                ->get();
            $screens2 = $screens2->toArray(); 
            $screens2 = array_column($screens2, 'equipment_value');
            $screens2 =  implode(',', $screens2);
    
            $seats2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_int_seats')
                ->get();
            $seats2 = $seats2->toArray(); 
            $seats2 = array_column($seats2, 'equipment_value');
            $seats2 =  implode(',', $seats2);
            
            
            $steering2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_int_steering')
                ->get();
            $steering2 = $steering2->toArray(); 
            $steering2 = array_column($steering2, 'equipment_value');
            $steering2 =  implode(',', $steering2);
    
            $glass2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_int_glass')
                ->get();
            $glass2 = $glass2->toArray(); 
            $glass2 = array_column($glass2, 'equipment_value');
            $glass2 =  implode(',', $glass2);
    
            $intOthers2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_int_other')
                ->get();
            $intOthers2 = $intOthers2->toArray(); 
            $intOthers2 = array_column($intOthers2, 'equipment_value');
            $intOthers2 =  implode(',', $intOthers2);
    
            $lights2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_ext_light')
                ->get();
            $lights2 = $lights2->toArray(); 
            $lights2 = array_column($lights2, 'equipment_value');
            $lights2 =  implode(',', $lights2);
    
            $mirrors2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_ext_mirrors')
                ->get();
            $mirrors2 = $mirrors2->toArray(); 
            $mirrors2 = array_column($mirrors2, 'equipment_value');
            $mirrors2 =  implode(',', $mirrors2);
    
            $cameras2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_ext_cameras')
                ->get();
            $cameras2 = $cameras2->toArray(); 
            $cameras2 = array_column($cameras2, 'equipment_value');
            $cameras2 =  implode(',', $cameras2);
    
            $genOther2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_gen_other')
                ->get();
            $genOther2 = $genOther2->toArray(); 
            $genOther2 = array_column($genOther2, 'equipment_value');
            $genOther2 =  implode(',', $genOther2);
    
            $extOther2 = Equipment::where('vehicle_id', '=', $id2)
                ->where('equipment_type', '=', 'ext_ext_other')
                ->get();
            $extOther2 = $extOther2->toArray(); 
            $extOther2 = array_column($extOther2, 'equipment_value');
            $extOther2 =  implode(',', $extOther2);
            $collection2 = [];
    
            if(count($photos2)) {
                $collection2 = collect($photos2)->map(function ($photo2) {
                    $photo2['thumb_path']   = url('public/storage/'.@$photo2['disk_name'],@$photo2['file_name']);
    
                    return ($photo2);
                });
            }
            
            $make2 =  DB::table('vehicles')
                ->join('makes', 'vehicles.make_id', '=', 'makes.id')
                ->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
                ->select('makes.name', 'makes.logo', 'moulds.name as model_name')
                ->where('vehicles.id', '=', $id2)
                ->get(); 
    
            return view('pages.compare.result', compact('collection', 'vehicle', 'furniture', 'sensors', 'seats',
            'steering', 'glass', 'intOthers', 'screens', 'sunroof', 'mirrors', 'lights', 'cameras', 'genOther', 
            'extOther', 'rims', 'user', 'make','collection2', 'vehicle2', 'furniture2', 'sensors2', 'seats2',
            'steering2', 'glass2', 'intOthers2', 'screens2', 'sunroof2', 'mirrors2', 'lights2', 'cameras2', 'genOther2', 
            'extOther2', 'rims2', 'user2', 'make2'));
        }
    }


    /**
     * get all models of specific make.
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function show($id) { 
  
        $models = Models::where('make_id', $id)->get();
        
        return  $models;
    }
}
