<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Model\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the contact page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $contact = Setting::where('settings_key', 'contact_us')->get();
        $str = $contact[0]->settings_value;
        $new_arr = explode("</p>", $str);

        $facebook =  $new_arr[2] . '</p>';
        preg_match('/href=(["\'])([^\1]*)\1/i', $facebook, $faceLink);
        $faceLink= $faceLink[2];

        $mobile = explode("<p> Mobile : ", ($new_arr[1]) );
        $mobile = $mobile[1];

        $instgram = $new_arr[3] . '</p>';
        preg_match('/href=(["\'])([^\1]*)\1/i', $instgram, $instgramLink);
        $instgramLink= $instgramLink[2];

        $whatsApp  = explode("<p> WhatsApp : ", ($new_arr[4]) );
        $whatsApp = $whatsApp[1];

        $email  = explode("<p> E-mail : ", ($new_arr[5]) );
        $email = $email[1];

 
        return view('pages.more.contact', compact('contact', 'faceLink', 'mobile', 'instgramLink', 'whatsApp',
            'email'));
    }

}
