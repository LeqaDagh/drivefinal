<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Vehicle;

use App\Region;

use App\Station; 

use App\Role; 

use App\Account; 

use App\Seller; 

use App\Attribute; 

use App\Http\Requests;

use DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
        return view('home');
    }
      /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        return view('home');
    }
    public function privacyAndPolicy()
    {

        $privacy_and_policy = \App\Setting::where('settings_key','privacy_and_policy')->first();

        return view('privacy_and_policy',compact('privacy_and_policy'));
    }
    public function termsAndCondtions()
    {

        $terms_and_condtions = \App\Setting::where('settings_key','terms_and_condtions')->first();

        return view('terms_and_condtions',compact('terms_and_condtions'));
    }
        /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function support()
    {
        return view('support');
    }
    
    
}
