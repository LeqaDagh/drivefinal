<?php

namespace App\Http\Controllers;

use App\Model\Make;
use App\Model\Models;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class MakeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }


    /**
     * Display all the static pages when authenticated
     *
     * @param int $id
     * @return array $makes
     */
    public function show($id) { 
  
        $makes = Make::where('id', $id)->get();

        return  $makes;

    }

}