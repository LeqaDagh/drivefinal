<?php

namespace App\Http\Controllers;

use App\Media;

use App\Activity;

use Illuminate\Http\Request;

use App\Http\Requests\MediaRequest;

use App\Http\Requests\NullRequest;

use App\Http\Requests;

use Auth;

class MediasController extends Controller
{
	private $METHOD ;

    public function __construct()
	{


	}


	public function getMedia(){

		 $medias = Media::latest('id');

		return $medias; 				
	}

	public function index()
	{
		
	 	$medias  =$this->getMedia()->paginate(10);

	 	Auth::user()->recordUserActivity($this->METHOD);

		 return view('medias.index',compact('medias'));
	}

	public function show($id)
	{

		 $media = Media::findOrFail($id);

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('medias.show',compact('media'));
	}

	public function create()
	{

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('medias.create');
	}

	public function store(MediaRequest $request)
	{

		$medias = Media::create($request->all());

		return redirect('medias');
	}


	public function edit($id)
	{

		$media= Media::findOrFail($id);

		 Auth::user()->recordUserActivity($this->METHOD,Auth::user());

		 return view('medias.edit',compact('media'));
	}

	public function update($id,MediaRequest $request)
	{

		 $media= Media::findOrFail($id);

		 $media->update($request->all());

		return redirect('medias');
	}

	public function destroy(NullRequest $request)
    {

      

    }
}
