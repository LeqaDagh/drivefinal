<?php

namespace App\Http\Controllers;

use App\Model\Make;
use App\Model\Models;
use App\Model\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Display all the static pages when authenticated
     *
     * @param array $makes, $firstMakes
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $sellers = DB::table('users')
            ->join('regions', 'users.seller_region_id', '=', 'regions.id')
            ->select('users.*', 'regions.name as city')
            ->where("group", "=", "se")
            ->get();

        return view('pages.more.index', compact('sellers'));
    }

    /**
     * Display all the static pages when authenticated
     *
     * @param int $id
     * @return array $makes
     */
    public function show($id) { 
  
      
    }

}