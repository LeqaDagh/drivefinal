<?php

namespace App\Http\Controllers;

use App\Model\Media;
use App\Model\Region;
use App\User;
use App\Model\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use Illuminate\Support\Facades\Hash;
use Image;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display the auth profile.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $regions = Region::all();
        $user = Auth::user();
        $userId = $user->id;
        $name = Region::where('id', $user->region_id)->get();
        $maxVehicles = $user->max_vehicles;
        $vehicleCount = Vehicle::where("user_id", $userId)->count();
        $allowedVehicles = $maxVehicles - $vehicleCount;

        return view('pages.more.profile.index', compact('regions', 'allowedVehicles', 'user', 
        'name', 'vehicleCount'));
    }

    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        return view('profile.edit');
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(User $user, Request $request)
    { 
        if($request->name) $user->name = $request->name;
        if($request->email) $user->email = $request->email;
        if($request->region_id) $user->region_id = $request->region_id;
        if($request->address) $user->address = $request->address;
        if($request->mobile) $user->mobile = $request->mobile;

        $user->save();

        return back();
    }

    /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(User $user, Request $request)
    {
        $request->validate([
            'password' => 'required',
            'password_confirmation' => 'required|string|confirmed|min:8|different:password'          
        ]);

        /*if (Hash::check($request->password, Auth::user()->password) == false)
        {
            return response(['message' => 'Unauthorized'], 401);  

        } else */

        if ($request->password_confirmation) {
            $user->password = Hash::make($request->password_confirmation);
            $user->save();
        }
    
        return back();    
    }

    /**
     * Create a new Media for company.
     *
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {

        $user = Auth::user();

        foreach ($request->photos as $photo) {

            $original_name = $photo->getClientOriginalName();
            $extra_name  = uniqid().'_'.time().'_'.uniqid().'.'.$photo->extension();
            $encoded_base64_image = substr($photo, strpos($photo, ',') + 1);
            $resized_image = Image::make($photo->getRealPath());
            $resized_image->save(public_path('storage/sellers/'.$extra_name));
            $path = public_path('storage/sellers/'.$extra_name);
            $logo = public_path('opa_logo.png'); 
            $v_logo = public_path('v_logo.png');    
            $img = Image::make($path);
            $img->insert($logo, 'bottom-left', 10, 10);
            $pad = intval(($img->height()-500)/2);
            $img->insert($v_logo, 'top-right',10,$pad);
            
            $img->resize(800,null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save($path);

            Media::create([
                'disk_name' => 'sellers',
                'file_name' => $extra_name,
                'file_size' => '11',
                'sort_order' => '1',
                'field' => 'photos',
                'description' => '',
                'title' => $user->name,
                'content_type' =>'11',
                'fileable_id' =>  $user->id,
                'fileable_type' => 'App\Vehicle'
            ]);

        }
  
        return redirect()->back();
    }
    /**
     * Display the Image edit page for company.
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function companyImages($id) { 

        $photos = Media::where('fileable_id', '=', $id)
            ->where('disk_name', 'sellers')
            ->get();
         
		$collection = [];

		if(count($photos)) {
			$collection = collect($photos)->map(function ($photo) {
                $photo['thumb_path']   = url('public/storage/'.@$photo['disk_name'],@$photo['file_name']);

			    return ($photo);
			});
		}
       
        return view('pages.more.profile.images', compact('collection', 'id'));
    }
}
