<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Request;

use App\Region;

use App\Activity;

use App\Http\Requests\RegionRequest;

use App\Http\Requests\NullRequest;

use Auth;

class RegionsController extends Controller
{
		private $METHOD ;

    	public function __construct()
	{


	}

    public function getRegionsByParentId(Request $request){

    	$regions = Region::where('parent',$request::input('id'))->get();

    	return $regions;
    }

    public function getRegions($request)
	{

		return $regions = Region::LikeName($request->get('name'))
		                   		->equalFrom($request->get('from'))
		                   		->equalTo($request->get('to'))
				 				->latest();
			}

	public function getXhrRegions(NullRequest $request)

	{

			$regions = Region::select("regions.*")
						  ->equalParent($request->get('parent'))
						  ->get();
		 
	

			return $regions;
	}

	public function index(NullRequest $request)
	{
		
		 $regions =$this->getRegions($request)->paginate(10);

		 


		 return view('regions.index',compact('regions'));
	}

	public function show($id,NullRequest $request)
	{

		 $region 				= Region::findOrFail($id);

		 $regions = $this->getRegions($request)->where('regions.parent','=',$id)->get();



		 return view('regions.show',compact('region','regions'));
	}

	public function create(NullRequest $request)
	{

	
		$parents = Region::whereNull('parent')->pluck('name','id');

		 $parents->prepend(trans('main.no_one'));


		 return view('regions.create',compact('parents'));
	}

	public function store(RegionRequest $request)
	{
		
		$parent = Region::find($request->parent);

		$region = new \App\Region($request->all());

		$region->region_parent()->associate($parent);

		$region->save();


		return redirect('regions');
	}


	public function edit($id,NullRequest $request)
	{

	
		$parents = Region::whereNull('parent')->pluck('name','id');
		
		$parents->prepend(trans('main.no_one'));

		$region= Region::findOrFail($id);


		 return view('regions.edit',compact('region','parents'));
	}

	public function update($id,RegionRequest $request)
	{

		
		$parent = Region::find($request->parent);

		$region = Region::findOrFail($id);
		$region->fill($request->all());

		$region->region_parent()->associate($parent);

		$region->save();

		

		return redirect('regions');
	}

	 public function destroy(NullRequest $request)
    {

  

    }
    public function removeRegion(NullRequest $request)
    {

  		$region = Region::findOrFail(request()->region_id);

  		$region->delete();

    }
    
}
