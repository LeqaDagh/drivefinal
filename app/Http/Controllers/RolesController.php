<?php

namespace App\Http\Controllers;

use App\Role;

use App\Permission;

use App\Activity;

use Illuminate\Http\Request;

use App\Http\Requests\RoleRequest;

use App\Http\Requests\NullRequest;

use App\Http\Requests;

use Auth;

class RolesController extends Controller
{

    public function __construct()
	{
	}


	public function getRole(){

		 $roles = Role::latest('id');

		 return $roles; 				
	}

	public function index()
	{

		 $roles = $this->getRole()->paginate(10);


		 return view('roles.index',compact('roles'));
	}


	public function show($id)
	{

		 $role 	= Role::findOrFail($id);


		 return view('roles.show',compact('role'));
	}

	public function create()
	{
		$permissions = Permission::pluck('label','id');


		 return view('roles.create',compact('permissions'));
	}

	public function store(RoleRequest $request)
	{

			 $role = Role::create($request->all());

			 $role->givePermissionTo($request->permissions);

			 return redirect('roles');
	}


	public function edit($id)
	{


		$role= Role::findOrFail($id);

		$permissions = Permission::pluck('label','id');


		 return view('roles.edit',compact('role','permissions'));
	}

	public function update($id,RoleRequest $request)
	{

		 $role = Role::findOrFail($id);

		 $role->update($request->all());

		 $role->givePermissionTo($request->permissions);
		
		return redirect('roles');
	}


	public function destroy(NullRequest $request)
    {

    }
}
