<?php

namespace App\Http\Controllers;

use App\Search;

use App\Activity;

use Illuminate\Http\Request;

use App\Http\Requests\SearchRequest;

use App\Http\Requests\NullRequest;

use App\Http\Requests;

use Auth;

class SearchesController extends Controller
{
	private $METHOD ;

    public function __construct()
	{


	}


	public function getSearch(){

		 $searches = Search::latest('id');

		return $searches; 				
	}

	public function index()
	{
		
	 	$searches  = $this->getSearch()->paginate(100);



		 return view('searches.index',compact('searches'));
	}

	public function show($id)
	{

		 $searche = Search::findOrFail($id);

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('searches.show',compact('searche'));
	}

	public function create()
	{

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('searches.create');
	}

	public function store(SearchRequest $request)
	{

		$searches = Search::create($request->all());

		return redirect('searches');
	}


	public function edit($id)
	{

		$searche= Search::findOrFail($id);

		 Auth::user()->recordUserActivity($this->METHOD,Auth::user());

		 return view('searches.edit',compact('searche'));
	}

	public function update($id,SearchRequest $request)
	{

		 $searche= Search::findOrFail($id);

		 $searche->update($request->all());

		return redirect('searches');
	}

	public function destroy(NullRequest $request)
    {

      

    }
}
