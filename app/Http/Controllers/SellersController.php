<?php

namespace App\Http\Controllers;

use App\Seller;

use App\Activity;

use App\User;

use App\Region;

use App\Media;

use Illuminate\Http\Request;

use App\Http\Requests\SellerAdminRequest;

use App\Http\Requests\ChangeSellerPasswordRequest;

use App\Http\Requests\NullRequest;

use App\Http\Requests\PhotosRequest;

use App\Http\Requests;

use Auth;


class SellersController extends Controller
{
	

    public function __construct()
	{


	}


	public function getSeller(){

		 $sellers = User::seller()
		 				 ->likeName(request()->name)
		 				 ->likeEmail(request()->email)
		 				 ->likeMobile(request()->mobile)
		 				 ->likeUsername(request()->username)
		 				 ->equalRegion(request()->region_id)
		 				 ->likeSellerName(request()->seller_name)
		 				 ->likeSellerMobile(request()->seller_mobile)
		 				 ->likeSellerEmail(request()->seller_email)
		 				 ->latest('id');

		return $sellers; 				
	}

	public function index()
	{
		$regions  = Region::pluck('name','id');
		$regions->prepend(trans('main.all'),0);

		$clients  = User::pluck('name','id');
		$clients->prepend(trans('main.all'),0);

	 	$sellers  = $this->getSeller()->paginate(10);


		 return view('sellers.index',compact('sellers','regions','clients'));
	}

	public function show($id)
	{

		 $seller = User::seller()->findOrFail($id);


		 return view('sellers.show',compact('seller'));
	}

	public function create()
	{


		 return view('sellers.create');
	}

	public function store(SellerAdminRequest $request)
	{

		$sellers = Seller::create($request->all());

		return redirect('sellers');
	}


	public function edit($id)
	{


 		$seller 		= User::seller()->hisOwen()->findOrFail($id);

		 $regions 	= Region::whereNull('parent')->pluck('name','id');	


		 return view('sellers.edit',compact('seller','regions'));

	}

	public function update($id,SellerAdminRequest $request)
	{

	
		$region = Region::find($request->region_id);

		$seller_region = Region::find($request->seller_region_id);

		$seller = User::seller()->hisOwen()->findOrFail($id);

		$seller->fill($request->all());

		$seller->region()->associate($region);

		$seller->seller_region()->associate($seller_region);

		$seller->save();
		
		return redirect('sellers/'.$id.'/edit');
	}

	public function destroy(NullRequest $request)
    {

      

    }

        public function removePhoto(NullRequest $request)
    {
    	$media = Media::findOrFail($request->media_id);

    	$media->delete();
    }


    
    public function addPhotosForSeller(NullRequest $request)
	{
		$seller = Seller::findOrFail($request->seller_id);

		return view('sellers.js.show.add_photos_for_seller_modal',compact('seller'));
	}

	

	public function addPhotosForSellerPost(PhotosRequest $request)
	{
	



		$seller = Seller::findOrFail($request->seller_id);

		 	 $photos  = [];

 			 if( $request->hasFile('photos'))
 			 {
	 			 foreach ($request->file('photos') as $key => $file) 
	 			 {
						 $original_name = $file->getClientOriginalName();
						 $extra_name    = uniqid().'_'.time().'_'.uniqid().'.'.$file->extension();
			        	 $file->storeAs('sellers', $extra_name);
			        	 $photos[] = [
			        	 	'disk_name' =>'sellers',
			        	 	'file_name' =>$extra_name,
			        	 	'file_size' =>$file->getSize(),
			        	 	'content_type'=>$file->getClientMimeType(),
			        	 	'title'=>$seller->name,
			        	 	'description'=>'',
			        	 	'field'=>'photos',
			        	 	'sort_order'=>'1'
			        	 ];

			 

			        	 
	 			 }	
        	 }
        	




		$seller->photos()->createMany($photos);

		return $seller;
	}

	public function changePassword(ChangeSellerPasswordRequest $request)
	{


		$user = User::seller()->findOrFail(request()->user_id);

		 $user->password = $request->password;

		$user->save();

		return redirect('sellers/'.$user->id);
	}
}
