<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Setting;

use App\Activity;

use App\Http\Requests\NullRequest;

use Auth;

use Spatie\Backup\BackupDestination\BackupDestination;

class SettingsController extends Controller
{
    


	 private $allow_settings ;

   	 public function __construct()
	{
 
     $this->allow_settings = ['close_site','about_us','contact_us','privacy_and_policy'];
	
	}



	public  function settings()
	{

	

		$settings = Setting::all();

		
		return view('settings.settings',compact('settings'));

	}

	public  function settingsUpdate(NullRequest $request)
	{

		
		$settings = Setting::all();



		foreach ($settings as $key => $setting) 
		{

		$setting->update(
						[
							'settings_value'=>$request->get($setting->settings_key)
						]
				);

		}

		return redirect('settings/settings');

	}



}