<?php

namespace App\Http\Controllers;

use App\User;

use App\Activity;

use App\Region;

use App\Station; 

use App\Role; 

use App\Http\Requests;

use App\Http\Requests\SupportRequest;

use App\Http\Requests\NullRequest;

use Illuminate\HttpResponse;

use Request;

use Auth;

use DB;

use PDF;

use App\Enums\UserGroups;

use Illuminate\Support\Str;



class SupportsController extends Controller
{
    
	private $METHOD ;

	private $role ;

	private $group =  UserGroups::SUPPORT; 

  	public function __construct()
	{
			 $this->role = trans('main.user_groups')[$this->group]['default_role'];

	}




	private function getSupports(NullRequest $request)
	{
		$supports = User::support()
					  ->hisOwen()
 		   			  ->equalRegion($request->get('region_id'))
			          ->likeUsername($request->get('username'))
			          ->likeName($request->get('name'))
			          ->likeEmail($request->get('email'))
			          ->likeMobile($request->get('mobile'))
			          ->equalUserStatus($request->get('user_status'));

		return $supports;		          

	}
    public function index(NullRequest $request)
	{

		$paginate  = 10;
		if($request->pagination)
		{
			$paginate  = $request->pagination;	
		}

		if($paginate>2000)
		{
			$paginate = 2000;
		}


 		 $supports = $this->getSupports($request)->paginate($paginate);


		 $regions 	= Region::pluck('name','id');
		 $regions->prepend(trans('main.all'),0);


		 $user_status = collect(trans('main.user_status'));
		 $user_status->prepend(trans('main.all'),0);

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('supports.index',compact('supports',"regions","user_status"));
	}

    public function pdf(NullRequest $request)
	{
		$paginate  = 10;

		if($request->pagination)
		{
			$paginate  = $request->pagination;	
		}

		if($paginate>2000)
		{
			$paginate = 2000;
		}


 		$supports = $this->getSupports($request)->paginate($paginate);


		 $view = \View::make('supports.pdf')->with(['supports'=>$supports]);
         $html = $view->render();
		 PDF::SetFooterMargin(40);
         PDF::SetTitle('طباعة تقرير');
         PDF::AddPage();
         PDF::SetAutoPageBreak(TRUE, 40);
         PDF::setRTL(true);
         PDF::SetFont('dejavusans', '', 10); 
         PDF::writeHTML($html, true, false, true, false, '');
       	 ob_end_clean();
         PDF::Output(time(). '_' . rand(1,999) . '_' .'supports.pdf');

	}

	public function create()
	{


		 $regions 	= Region::whereNull('parent')->pluck('name','id');

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('supports.create',compact("regions"));	
	}

	public function store(SupportRequest $request)
	{


		$region = Region::find($request->region_id);


		$role = Role::find($this->role);

		$request->merge(['group' => $this->group]);

		$support = new \App\User($request->all());

		$support->region()->associate($region);

		$support->save();

		$support->roles()->sync([@$role->id]);

		return redirect('supports');	
	}

	public function edit($id)
	{
		 $support 		= User::support()->hisOwen()->findOrFail($id);

		 $regions 	= Region::whereNull('parent')->pluck('name','id');	



		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('supports.edit',compact('support','regions'));	
	}
	public function update($id,SupportRequest $request)
	{
	

		$region = Region::find($request->region_id);


		$support = User::support()->hisOwen()->findOrFail($id);

		$support->fill($request->all());

		$support->region()->associate($region);

		$support->save();
		
		return redirect('supports/'.$id.'/edit');
		
	}

	public function show($id)
	{
		$support = User::support()->hisOwen()->findOrFail($id);

		Auth::user()->recordUserActivity($this->METHOD,$support);

		 return view('supports.show',compact('support'));

	}


	public function destroy(NullRequest	 $request)
	{
		
	}

	

	


}
