<?php

namespace App\Http\Controllers;

use App\Model\Equipment;
use App\Model\Make;
use App\Model\Media;
use App\Model\Models;
use App\User;
use App\Model\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VehicleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Display all the static pages when authenticated
     *
     * @param array $makes, $firstMakes
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $makes = Make::all();
        $firstMakes = Make::all()->take(8);
        $user = Auth::user();

        return view('pages.vehicle.index', compact('makes', 'firstMakes', 'user'));
    }

    /**
     * Display all the static pages when authenticated
     *
     * @param array $models
     * @return \Illuminate\View\View
     */
    public function show($id) { 

        $ids =  Models::where('make_id', '=', $id)->pluck('id');
        $test =  Models::where('parent', '=', $ids)
            ->where('make_id', '=', NULL)
            ->get();

        $count= count($ids);
        $i = 0;
        $t =array();

        while ($i < $count) {
            $models = Models::where('parent', '=', $ids[$i])
                ->orWhere('id', '=', $ids[$i])
                ->get();
                array_push($t, $models);
                $models = json_encode($t);
            $i++;
        }

        return $models;
    }

    /**
     * Create a new vehicle.
     *
     * @param  Illuminate\Http\Request  $request
     * @return \App\Vehicle
     */
    protected function store(Request $request)
    {
        $user = Auth::user();
        $userId = $user->id;
        $maxVehicles = $user->max_vehicles;
        $isPuplish = $user->publish_status;
        $vehicleCount = Vehicle::where("user_id", $userId)->count();
        
        if ($isPuplish =="yes" && $vehicleCount < $maxVehicles) {

            $vehicle = new Vehicle;
            $vehicle->user_id = $userId;
            $vehicle->fill($request->all());
            $vehicle->save();

            $types = [
                'payment_method',
                'ext_int_sunroof',
                'ext_int_furniture',
                'ext_int_seats',			
                'ext_int_steering',
                'ext_int_screens',
                'ext_int_glass',
                'ext_int_other',
                'ext_ext_light',
                'ext_ext_mirrors',
                'ext_ext_rims',
                'ext_ext_cameras',
                'ext_ext_sensors',
                'ext_ext_other',
                'ext_gen_other'
            ];

            foreach ($types as $key => $type) {
                if(is_array($request->$type)) {
                    foreach ($request->$type as $key => $req) {
                        Equipment::create([
                            'equipment_value' => $req,
                            'equipment_type' => $type,
                            'vehicle_id' => $vehicle->id,
                        ]);
                    }
                }
                else {
                    Equipment::create([
                        'equipment_value' => $request->$type,
                        'equipment_type' => $type,
                        'vehicle_id' => $vehicle->id,
                    ]);	
                }
            }
            
            return $vehicle;

        } else {

            return response()->json('غير مسموح الاضافة');
            //return redirect()->back()->withErrors(['name' => 'غير مسموح الاضافة ']);
        }
        
    }

    /**
     * Create a new vehicle.
     *
     * @param  Illuminate\Http\Request  $request, $id
     * @return \App\Vehicle
     */
    protected function update(Request $request, $id)
    {

    }

    /**
     * Delete vehicle.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function destroy($id)
    {
        $vehicle = Vehicle::findOrFail($id);
        $vehicle->is_deleted = "yes";
        $vehicle->save();
        //$vehicle->delete();
        return back();
    }
}