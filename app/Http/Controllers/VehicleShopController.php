<?php

namespace App\Http\Controllers;

use App\Model\Equipment;
use App\User;
use App\Model\Models;
use App\Model\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VehicleShopController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Display all the static pages when authenticated
     *
     * @param array $makes, $firstMakes
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $sellers = DB::table('users')
            ->join('regions', 'users.seller_region_id', '=', 'regions.id')
            ->select('users.*', 'regions.name as city')
            ->where("group", "=", "se")
            ->get();
        
        return view('pages.more.vehicle.index', compact('sellers'));
    }

    /**
     * Display all the static pages when authenticated
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function show($id) { 

        $sellers = DB::table('users')
            ->join('regions', 'users.seller_region_id', '=', 'regions.id')
            ->select('users.*', 'regions.name as city')
            ->where("users.id", "=", $id)
            ->get();

        $vehicles =  Vehicle::where('user_id', '=', $id)
            ->where('is_deleted', 'no')
            ->join('makes', 'vehicles.make_id', '=', 'makes.id')
            ->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
            ->select('vehicles.*', 'makes.name', 'moulds.name as model_name')
            ->get();

        return view('pages.more.vehicle.vehicles', compact('sellers', 'vehicles'));
    }

    /**
     * Display all the static pages when authenticated
     *
     * @param Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    function store(Request $request) { 

        $name = $request->input('seller_name');

        $sellers =  DB::table('users')
            ->join('regions', 'users.seller_region_id', '=', 'regions.id')
            ->select('users.*', 'regions.name as city')
            ->where('seller_name', 'LIKE', '%' . $name . '%')
            ->get();
            
        return view('pages.more.vehicle.found', compact('sellers', 'name'));
        
    }

}