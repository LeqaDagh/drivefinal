<?php

namespace App\Http\Controllers;

use App\Vehicle;

use App\Activity;

use App\Region;

use App\Type;

use App\Attribute;

use App\Media;

use Illuminate\Http\Request;

use App\Http\Requests\VehicleRequest;

use App\Http\Requests\PhotosRequest;

use App\Http\Requests\NullRequest;

use App\Http\Requests;

use Auth;

use DB;

use Image;

class VehiclesController extends Controller
{
	private $METHOD ;
	private $types = [
					'ext_int_furniture',
					'ext_int_seats',
					'ext_int_steering',
					'ext_int_screens',
					'ext_int_sunroof',
					'ext_int_glass',
					'ext_int_other',
					'ext_ext_light',
					'ext_ext_mirrors',
					'ext_ext_rims',
					'ext_ext_cameras',
					'ext_ext_sensors',
					'ext_ext_other',
					'ext_gen_other'
 				 ];
    public function __construct()
	{


	}

	public function updateOrder(){

		$vehicle = Vehicle::findOrFail(request()->id);

		
		$vehicle->update(['order'=>request()->order]);
		

		return redirect('vehicles/'.$vehicle->id);

	}
	public function getVehicle(){

		 $vehicles = Vehicle::doFilters()
		 					->inSellers(request()->sellers)
		 					->inIsDeleted(request()->is_deleted)
		 			  		->inPublishStatus(request()->publish_status)
		 			 		->inStared(request()->stared)
		 			 		->equalSlider(request()->slider)
		 			 		->inEquipments(request()->equipments)
		 			 		->latest('id');

		return $vehicles; 				
	}

	public function index()
	{
		
	 	$vehicles  =$this->getVehicle()->paginate(50);

	 	$sellers = \App\User::seller()->pluck('seller_name','id');

	 	$makes = \App\Make::pluck('name','id');
	 	$moulds = \App\Mould::pluck('name','id');

	 	 $body = collect(trans('types.body_types'))->map(function ($name) { return $name['title'];});
		 $gear = collect(trans('types.gear_types'))->map(function ($name) { return $name['title'];});
		 $fuel = collect(trans('types.fuel_types'))->map(function ($name) { return $name['title'];});
		 $drivetrain_system =  collect(trans('types.drivetrain_system_types'))->map(function ($name) { return $name['title'];});
		 $drivetrain_type =  collect(trans('types.drivetrain_type_types'))->map(function ($name) { return $name['title'];});
		 $num_of_seats = collect(trans('types.num_of_seats_types'))->map(function ($name) { return $name['title'];});
		 $vehicle_status = collect(trans('types.vehicle_status_types'))->map(function ($name) { return $name['title'];});
		 $body_color = collect(trans('types.color_types'))->map(function ($name) { return $name['title'];});
		 $interior_color = collect(trans('types.color_types'))->map(function ($name) { return $name['title'];});
		 $customs_exemption = collect(trans('types.customs_exemption_types'))->map(function ($name) { return $name['title'];});
		 $num_of_doors = collect(trans('types.num_of_doors_types'))->map(function ($name) { return $name['title'];});
		 $previous_owners = collect(trans('types.previous_owners_types'))->map(function ($name) { return $name['title'];});
		 $origin = collect(trans('types.origin_types'))->map(function ($name) { return $name['title'];});
		 $num_of_keys = collect(trans('types.num_of_keys_types'))->map(function ($name) { return $name['title'];});
		 $yes_no = collect(trans('main.yes_no'))->map(function ($name) { return $name;});
		 $publish_status = collect(trans('types.publish_status_type'))->map(function ($name) { return $name['title'];});
		
		  $equipments = [];

			foreach ($this->types as $key => $type) 
			{
				foreach ( trans("types.".$type) as $key => $type) 
				{
					$equipments[$key] = $type['title'];
				}
			
			}


		 


		 

		 
	 	



		 return view('vehicles.index',compact('vehicles','sellers','makes','moulds','body','gear','fuel','drivetrain_system','drivetrain_type','num_of_seats','vehicle_status','body_color','interior_color','customs_exemption','num_of_doors','previous_owners','origin','num_of_keys','yes_no','publish_status','equipments'));
	}

	public function show($id)
	{

		 $vehicle = Vehicle::findOrFail($id);

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('vehicles.show',compact('vehicle'));
	}

	public function create()
	{

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('vehicles.create');
	}

	public function store(VehicleRequest $request)
	{

		$vehicles = Vehicle::create($request->all());

		return redirect('vehicles');
	}


	public function edit($id)
	{

		$vehicle = Vehicle::findOrFail($id);
		
		$regions = Region::all()->pluck('name','id');
		$regions->prepend(trans('main.choose'),0);

		$sell_types = Attribute::where('type_id',$this->getIdForType('sell_type_id'))->get()->pluck('name','id');
		$sell_types->prepend(trans('main.choose'),0);

		$vehicle_types = Attribute::where('type_id',$this->getIdForType('vehicle_type_id'))->get()->pluck('name','id');
		$vehicle_types->prepend(trans('main.choose'),0);

		$makes = Attribute::where('type_id',$this->getIdForType('make_id'))->get()->pluck('name','id');
		$makes->prepend(trans('main.choose'),0);

		$models = Attribute::where('type_id',$this->getIdForType('model_id'))->get()->pluck('name','id');
		$models->prepend(trans('main.choose'),0);

		$gears = Attribute::where('type_id',$this->getIdForType('gear_id'))->get()->pluck('name','id');
		$gears->prepend(trans('main.choose'),0);

		$condtions = Attribute::where('type_id',$this->getIdForType('condtion_id'))->get()->pluck('name','id');
		$condtions->prepend(trans('main.choose'),0);

		$body_types = Attribute::where('type_id',$this->getIdForType('body_type_id'))->get()->pluck('name','id');
		$body_types->prepend(trans('main.choose'),0);

		$fuel_types = Attribute::where('type_id',$this->getIdForType('fuel_type_id'))->get()->pluck('name','id');
		$fuel_types->prepend(trans('main.choose'),0);

		$vehicle_status = Attribute::where('type_id',$this->getIdForType('vehicle_status_id'))->get()->pluck('name','id');
		$vehicle_status->prepend(trans('main.choose'),0);

		$order_types =  Attribute::where('type_id',$this->getIdForType('order_type_id'))->get()->pluck('name','id');
		$order_types->prepend(trans('main.choose'),0);

		$equipments =  Attribute::where('type_id',$this->getIdForType('equipment_id'))->get()->pluck('name','id');
	
		$published = collect(trans('main.yes_no'));
		$published->prepend(trans('main.choose'),0);

		return view('vehicles.edit',
			compact('vehicle','regions','sell_types','vehicle_types','makes','models','gears','condtions','body_types','fuel_types','vehicle_status','order_types','published','equipments'));
	}

	public function update($id,VehicleRequest $request)
	{

		$vehicle = Vehicle::findOrFail($id);

		$region = Region::findOrFail($request->region_id);

		$sell_type = Attribute::findOrFail($request->sell_type_id);

		$vehicle_type = Attribute::findOrFail($request->vehicle_type_id);

		$make = Attribute::findOrFail($request->make_id);

		$model = Attribute::findOrFail($request->model_id);

		$gear = Attribute::findOrFail($request->gear_id);

		$condtion = Attribute::findOrFail($request->condtion_id);

		$body_type = Attribute::findOrFail($request->body_type_id);

		$fuel_type = Attribute::findOrFail($request->fuel_type_id);

		$vehicle_status= Attribute::findOrFail($request->vehicle_status_id);

		$order_type = Attribute::findOrFail($request->order_type_id);

		$vehicle->fill($request->all());

		$vehicle->region()->associate($region);

		$vehicle->sell_type()->associate($sell_type);

		$vehicle->vehicle_type()->associate($vehicle_type);

		$vehicle->make()->associate($make);

		$vehicle->model()->associate($model);

		$vehicle->gear()->associate($gear);

		$vehicle->condtion()->associate($condtion);

		$vehicle->body_type()->associate($body_type);

		$vehicle->fuel_type()->associate($fuel_type);

		$vehicle->vehicle_status()->associate($vehicle_status);

		$vehicle->order_type()->associate($order_type);

		$vehicle->save();

		$vehicle->equipments()->sync($request->equipment_id);

		return redirect("vehicles/{$id}/edit");
	}

	public function destroy(NullRequest $request)
    {

      

    }

    public function removePhoto(NullRequest $request)
    {
    	$media = Media::findOrFail($request->media_id);

    	$media->delete();
    }


    public function setAsSlider(NullRequest $request)
    {
    	$vehicle = Vehicle::findOrFail($request->vehicle_id);

    	$slider = $vehicle->photos()->findOrFail($request->media_id);

    	$vehicle->update(['slider_id'=>$slider->id]);
    }
      public function deleteAsSlider(NullRequest $request)
    {
    	$vehicle = Vehicle::findOrFail($request->vehicle_id);

    	$slider = $vehicle->photos()->findOrFail($request->media_id);

    	$vehicle->update(['slider_id'=>null]);
    }




    
    public function addPhotosForVehicle(NullRequest $request)
	{
		$vehicle = Vehicle::findOrFail($request->vehicle_id);

		return view('vehicles.js.show.add_photos_for_vehicle_modal',compact('vehicle'));
	}

	

	public function addPhotosForVehiclePost(PhotosRequest $request)
	{
	



		$vehicle = Vehicle::findOrFail($request->vehicle_id);

		 	 $photos  = [];

 			 if( $request->hasFile('photos'))
 			 {
	 			 foreach ($request->file('photos') as $key => $file) 
	 			 {
						 $original_name = $file->getClientOriginalName();
						 $extra_name    = uniqid().'_'.time().'_'.uniqid().'.'.$file->extension();
			        	 $file->storeAs('vehicles', $extra_name);
			        	 $photos[] = [
			        	 	'disk_name' =>'vehicles',
			        	 	'file_name' =>$extra_name,
			        	 	'file_size' =>$file->getSize(),
			        	 	'content_type'=>$file->getClientMimeType(),
			        	 	'title'=>$vehicle->name,
			        	 	'description'=>'',
			        	 	'field'=>'photos',
			        	 	'sort_order'=>'1'
			        	 ];

					$path = public_path('storage/vehicles/'.$extra_name);
				    $logo = public_path('opa_logo.png'); 
				    $v_logo = public_path('v_logo.png');    
				 	$img = Image::make($path);
				    $img->insert($logo, 'bottom-left', 10, 10);
				    $pad = intval(($img->height()-500)/2);
				    $img->insert($v_logo, 'top-right',10,$pad);

				     $img->resize(800, null, function ($constraint) {
					    $constraint->aspectRatio();
					});

				    $img->save($path); 



								        	 
	 			 }	
        	 }
        	

		$vehicle->photos()->createMany($photos);

		return $vehicle;
	}

		public function getIdForType($field)
	{
  		return DB::table('types')->select('id')->where('search_field',$field)->first()->id;
	}

	public function makeStared(NullRequest $request)
    {
    	$vehicle = Vehicle::findOrFail($request->vehicle_id);

    	$vehicle->update(['stared'=>'yes']);
    }
    public function makeUnStared(NullRequest $request)
    {
    	$vehicle = Vehicle::findOrFail($request->vehicle_id);

    	$vehicle->update(['stared'=>'no']);
    }
    public function deleteVehicle(NullRequest $request)
    {
    	$vehicle = Vehicle::findOrFail($request->vehicle_id);

    	$vehicle->update(['is_deleted'=>'yes']);
    }
    public function restoreVehicle(NullRequest $request)
    {
    	$vehicle = Vehicle::findOrFail($request->vehicle_id);

    	$vehicle->update(['is_deleted'=>'no']);
    }
    public function publishVehicle(NullRequest $request)
    {
    	$vehicle = Vehicle::findOrFail($request->vehicle_id);

    	$vehicle->update(['publish_status'=>'pub']);
    }
     public function unpublishVehicle(NullRequest $request)
    {
    	$vehicle = Vehicle::findOrFail($request->vehicle_id);

    	$vehicle->update(['publish_status'=>'upu']);
    }

}
