<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Gate;

class CanDo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $controller = request()->route()->getAction()['controller'];

        $name_space = request()->route()->getAction()['namespace'].'\\';

        $value =  str_replace($name_space, "", $controller);

        if (strpos($value, 'Auth') == false) {
            $name =  str_replace("App\Http\Controllers\\", "", $value);

            $label_ex = str_replace("Controller@", "#", $name);

            $label = explode("#", $label_ex);

            $final_text =preg_replace('!\s+!', ' ', ltrim(preg_replace('/[A-Z]/', ' $0', $label[1].' '.$label[0])));

            $perm = strtolower(str_replace(" ", "_", $final_text));
           
            // dd($perm);
            if (Gate::denies($perm)) {
               // abort(403, trans('main.no_permissions_allow'));
            }
        }


        return $next($request);
    }
}
