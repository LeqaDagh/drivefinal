<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Rules\UniqueMobile;
use App\Rules\UniqueUserName;
use App\Rules\CheckPhoneNumber;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(isset($this->client_id))
        {
           $this->client = $this->client_id;  
        }
           

          return [
             'name' => 'required|regex:/^[\p{Arabic}a-z 0-9]{1,}$/u',
             'region_id'=>'required|integer',
             'mobile'=> [new CheckPhoneNumber(),new UniqueMobile($this->client)],
             'email' => 'required|string|email|max:255|unique:users,email,'.$this->client,
             'password' => 'required_if:form,==,adding|string|min:6|confirmed',
        ];
    }
}
