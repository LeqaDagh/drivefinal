<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest;

use App\Rules\UniqueMobile;
use App\Rules\UniqueUserName;
use App\Rules\CheckPhoneNumber;


class PersonalRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

       

    	if(isset($this->user_id))
        {
           $this->user = $this->user_id;  
        }
           

  
        return [
        
                'name'=>'required',
                'mobile'=> ['required',new CheckPhoneNumber(),new UniqueMobile($this->user)],
                'region_id'=>'required',
                'address'=>'required',
            	'email' => 'required|string|email|max:255|unique:users,email,'.$this->user,
                'password' => 'required|string|min:6|confirmed',

        ];
    }
}

