<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest;



use App\Rules\CanPublish;  
use App\Rules\CheckPrice;  




class VehicleEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

  
        return [
                'make_id'=>'required|integer|min:1',
                'mould_id'=>'required|integer|min:1',
                'body'=>'required|in:'.implode(',',array_keys(trans('types.body_types'))),
                'year_of_product'=>'required',
                'year_of_work'=>'required',
                 'price' => ['integer','integer','min:1', new CheckPrice()],
                'power' => 'required|integer|min:1',
                'gear'=>'required|in:'.implode(',',array_keys(trans('types.gear_types'))),
                'hp'=>'required|integer',
                'mileage'=>'required|integer',
                'fuel'=>'required|in:'.implode(',',array_keys(trans('types.fuel_types'))),
                'drivetrain_system'=>'required|in:'.implode(',',array_keys(trans('types.drivetrain_system_types'))),
                'drivetrain_type'=>'required|in:'.implode(',',array_keys(trans('types.drivetrain_type_types'))),
                'num_of_seats'=>'required|in:'.implode(',',array_keys(trans('types.num_of_seats_types'))),
                'vehicle_status'=>'required|in:'.implode(',',array_keys(trans('types.vehicle_status_types'))),
                'body_color'=>'required|in:'.implode(',',array_keys(trans('types.color_types'))),
                'interior_color'=>'required|in:'.implode(',',array_keys(trans('types.interior_color_types'))),   
                'payment_method'=>'required|array|between:1,4'               
        ];
    }
}
