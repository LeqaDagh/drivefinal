<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Make extends Model
{
    
    
    protected $fillable = ['name','slug','style_width','style_height','logo','order','visible'];
    protected $appends  = ['checked'];
   
    public function getCheckedAttribute()
    {
        return false;
    }

    public function vehicles()
    {
       return $this->hasMany(Vehicle::class);
    }
     public function moulds()
    {
       return $this->hasMany(Mould::class);
    }	
}
