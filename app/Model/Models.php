<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Models extends Model
{
    protected $table = 'moulds'; 
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'make_id', 'slug', 'logo', 'order', 'review', 'parent', 'visible'
    ];
}
