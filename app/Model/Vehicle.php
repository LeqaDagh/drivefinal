<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Vehicle extends Model
{
    protected $table = 'vehicles'; 
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'make_id', 'mould_id', 'body', 'extra_title', 'year_of_product', 'year_of_work', 'price', 
        'first_payment', 'price_type', 'power', 'hp', 'mileage', 'gear', 'fuel', 'drivetrain_system', 'drivetrain_type',
        'num_of_seats', 'vehicle_status', 'body_color', 'interior_color', 'customs_exemption', 'num_of_doors', 'origin',
        'license_date', 'previous_owners' , 'num_of_keys', 'desc', 'order_type', 'order', 'is_deleted', 'publish_status',
        'stared', 'slider_id', 'delete_at'
    ];
}
