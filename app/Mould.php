<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mould extends Model
{
    
    
    protected $fillable = ['name','slug','logo','order','visible','review'];
   
    protected $appends  = ['checked'];
   
    public function getCheckedAttribute()
    {
        return false;
    }

    public function vehicles()
    {
       return $this->hasMany(Vehicle::class);
    }
  
     public function make()
    {
       return $this->belongsTo(Make::class);
    }	

    public function childs(){
       return $this->hasMany(Mould::class,'parent');
    }	
     public function father(){
       return $this->belongsTo(Mould::class,'parent');
    }	

    

}
