<?php 

namespace App;

use Auth;

use Jenssegers\Agent\Agent;

use Request;

trait RecordsActivity

{

 protected static function bootRecordsActivity()

 {

        foreach (static::getModelEvents() as $event) {

         static::$event(function($model) use ($event) {

           		$model->recordActivity($event);

            });       	

        }




    }

    public function recordActivity($event)

    {

       //  $agent = new Agent();

       // $data = [

       //          'subject_id' => @$this->id,

       //          'subject_type' => get_class($this),

       //          'name' => $this->getActivityName($this, $event),

       //          'ip' => Request::ip(),

       //          'browser' => $agent->browser(),

       //          'browser_version' => $agent->version($agent->browser()),

       //          'platform' => $agent->platform(),

       //          'platform_version' => $agent->version($agent->platform()),

       //           'device' => $agent->device()

       //  ];

       //  if(Auth::check())
       //  {

       //      $data['user_id'] = Auth::user()->id;

       //  }

    //	Activity::create($data);

    }

    protected function getActivityName($model, $action)

    {

    	$name = strtolower((new \ReflectionClass($model))->getShortName());

    	return "{$action}_{$name}";

    }

    protected static function getModelEvents()

    {

    	if(isset(static::$recordEvents)) {

    		return static::$recordEvents;

    	}

    	return [

    		'created','updated','deleted'
    	
    	];

    }


}