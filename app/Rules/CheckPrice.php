<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use Auth;

class CheckPrice implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

        /**
     * Create a new rule instance.
     *
     * @return void
     */
      
      private $msg = "";

    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
            $user  = Auth::user();

            $can_ignor_price = $user->can_ignor_price;

            $can_show_first_payment = $user->can_show_first_payment;

            $return = true;


            if($can_ignor_price=="no" AND $can_show_first_payment=="no")
            {
            	
               
            }
            if($can_ignor_price=="yes" AND $can_show_first_payment=="no")
            {
            		
               
            }
            if($can_ignor_price=="no" AND $can_show_first_payment=="yes")
            {
            	if(request()->price_type=="price_type_types_fp3" AND empty(request()->first_payment) )
            	{
            		$this->msg = "يجب ادخال الدفعة الاولى";	
            		$return = false;
            	}
               
            }
            if($can_ignor_price=="yes" AND $can_show_first_payment=="yes")
            {
				if(request()->price_type=="price_type_types_fp3" AND empty(request()->first_payment) )
            	{
            		$this->msg = "يجب ادخال الدفعة الاولى";	
            		$return = false;
            	}
            }

            return  $return;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->msg;
    }
}
