<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

	use RecordsActivity;
	
    protected $fillable = ['settings_key','settings_name','settings_value'];
}