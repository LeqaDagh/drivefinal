<?php

namespace App\Transformers;

/**
 * Class LessonTransformer
 *
 * This class is used to transform lessons, in JSON format,
 * by explicitly stating which lesson properties to serve up.
 *
 * @package Acme\Transformers
 */
use Carbon\Carbon;
use App\Transformers\PhotoTransformer;


class SellerTransformer extends Transformer{

   protected $photoTransformer;

    public function __construct(PhotoTransformer $photoTransformer)
    {
         $this->photoTransformer = $photoTransformer;
      
    }

    /**
     * This function transforms a single lesson -
     * from JSON format with specified fields.
     *
     * @param $device A Device
     * @return array Returns an individual lesson,
     * according to specified fields.
     */
    public function  transform($seller)
    {
                                      
        return [
                'id'          	=> @$seller['id'],
                'seller_name'   => @$seller['seller_name'],
                'seller_mobile' => @$seller['seller_mobile'],
                'seller_email'  => @$seller['seller_email'],
                'region'      	=> @$seller['seller_region']['name'],
                'seller_address'=> @$seller['seller_address'],
                'seller_lat'    => @$seller['seller_lat'],
                'seller_lon'    => @$seller['seller_lon'],
                'cover'         => url('public/storage/'.@$seller['cover']['disk_name'],@$seller['cover']['file_name']),
                'cover_thumb'   => url('public/storage/'.@$seller['cover']['disk_name'],@$seller['cover']['file_name']),
                'photos'        => $seller['photos'] ? $this->photoTransformer->transformCollection(@$seller['photos']->toArray()):[],
                
        ];
    }
}


