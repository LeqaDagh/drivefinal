<?php

namespace App\Transformers;

/**
 * Class LessonTransformer
 *
 * This class is used to transform lessons, in JSON format,
 * by explicitly stating which lesson properties to serve up.
 *
 * @package Acme\Transformers
 */
use Carbon\Carbon;

class UserTransformer extends Transformer{


    /**
     * This function transforms a single lesson -
     * from JSON format with specified fields.
     *
     * @param $device A Device
     * @return array Returns an individual lesson,
     * according to specified fields.
     */
    public function  transform($user)
    {

        $permissions = [
                    'can_delete'=>false,
                    'can_edit'=>false,
                    'can_mange_photos'=>false,
                    'can_print_details'=>true
                ];

        if($user['group']=='se')
        {

        $permissions = [
                    'can_delete'=>true,
                    'can_edit'=>true,
                    'can_mange_photos'=>true,
                    'can_print_details'=>true
                ]; 
        }        
                                      
        return [
                'id'            => $user['id'],
                'name'          => $user['name'],
                'email'         => $user['email'],
                'address'       => $user['address'],
                'created_at'    => $user['created_at'],
                'permissions'              => $permissions,
                'can_ignor_price'          => $user['can_ignor_price'],
                'can_show_first_payment'   => $user['can_show_first_payment']
        ];
    }
}

