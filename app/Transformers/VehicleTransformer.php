<?php

namespace App\Transformers;

/**
 * Class LessonTransformer
 *
 * This class is used to transform lessons, in JSON format,
 * by explicitly stating which lesson properties to serve up.
 *
 * @package Acme\Transformers
 */
use Carbon\Carbon;
use App\Transformers\PhotoTransformer;
use App\Transformers\SellerTransformer;
use Auth;

class VehicleTransformer extends Transformer{

    protected $photoTransformer;

    public function __construct(PhotoTransformer $photoTransformer,SellerTransformer $sellerTransformer)
    {
         $this->photoTransformer = $photoTransformer;
         $this->sellerTransformer = $sellerTransformer;
    }

    /**
     * This function transforms a single lesson -
     * from JSON format with specified fields.
     *
     * @param $device A Device
     * @return array Returns an individual lesson,
     * according to specified fields.
     */
    public function  transform($vehicle)
    {
        
    
$data =  [
'id'=>$vehicle['id'],
'mileage_c'=>number_format($vehicle['mileage']),
'price_c'=>number_format($vehicle['price']),
'first_payment_c'=>number_format($vehicle['first_payment']),
'seller_id'=>$vehicle['seller_id'],
'make_id'=>$vehicle['make_id'],
'make_name'=>$vehicle['make']['name'],
'mould_id'=>$vehicle['mould_id'],
'mould_name'=>$vehicle['mould']['name'],
'body'=>$vehicle['body'],
'extra_title'=>$vehicle['extra_title'],
'year_of_product'=>$vehicle['year_of_product'],
'year_of_work'=>$vehicle['year_of_work'],
'price'=>$vehicle['price'],
'first_payment'=>$vehicle['first_payment'],
'price_type'=> $vehicle['price_type'],

'power'=>$vehicle['power'],
'hp'=>$vehicle['hp'],
'mileage'=>$vehicle['mileage'],
'gear'=>$vehicle['gear'],
'fuel'=>$vehicle['fuel'],
'drivetrain_system'=>$vehicle['drivetrain_system'],
'drivetrain_type'=>$vehicle['drivetrain_type'],
'num_of_seats'=>$vehicle['num_of_seats'],
'vehicle_status'=>$vehicle['vehicle_status'],
'body_color'=>$vehicle['body_color'],
'interior_color'=>$vehicle['interior_color'],
'customs_exemption'=>$vehicle['customs_exemption'],
'num_of_doors'=>$vehicle['num_of_doors'],
'license_date'=>$vehicle['license_date'],
'previous_owners'=>$vehicle['previous_owners'],
'origin'=>$vehicle['origin'],
'num_of_keys'=>$vehicle['num_of_keys'],
'desc'=>$vehicle['desc'],
'order_type'=>$vehicle['order_type'],
'order'=>$vehicle['order'],
'is_deleted'=>$vehicle['is_deleted'],
'delete_at'=>$vehicle['delete_at'],

'seller'            => $this->sellerTransformer->transform(@$vehicle['seller']),
'cover'             => url('public/storage/'.@$vehicle['cover']['disk_name'],@$vehicle['cover']['file_name']),
'cover_thumb'       => url('public/storage/'.@$vehicle['cover']['disk_name'],@$vehicle['cover']['file_name']),
'photos'            => $this->photoTransformer->transformCollection(!is_null(@$vehicle['photos']) ? @$vehicle['photos']->toArray() : []),
'is_fav'                => @$vehicle['is_fav'],
'is_compare'            => @$vehicle['is_compare'],
'can_delete'=>@$vehicle['is_compare'],
'can_edit'=>@$vehicle['is_compare'],
'can_mange_photos'=>@$vehicle['is_compare'],
'can_print_details'=>@$vehicle['can_print_details'],

'is_star'               => @$vehicle['stared']=='yes' ? true : false,
'call_number'=>'+970569114961',
'body_color_txt'        => trans('types.color_types.'.$vehicle['body_color']),
'interior_color_txt'    => trans('types.interior_color_types.'.$vehicle['interior_color']),

'vehicle_review'=> !is_null($vehicle['mould']['review'])  ? 'https://www.youtube.com/embed/'.$vehicle['mould']['review'] :  null
];

      $types = [
    'ext_int_sunroof',
    'ext_int_furniture',
    'payment_method',           
    'ext_int_steering',
    'ext_int_screens',
    'ext_int_glass',
    'ext_int_other',
    'ext_ext_light',
    'ext_ext_mirrors',
    'ext_ext_rims',
    'ext_ext_cameras',
    'ext_ext_sensors',
    'ext_ext_other',
    'ext_gen_other'];

    foreach ($types as $key => $type) {
       
       $data[$type] = collect($vehicle['equipments'])->where('equipment_type',$type)->pluck('equipment_value')->toArray();

    }

// $favs = [];
// $compares =[];

// if(request()->has('fav'))
// {
//      $fav  = request()->fav;
//      $fav  = str_replace(["[","]"," "],"",$fav);
//      $fav  = explode(",", $fav);
//      $favs = $fav;
// }
// if(request()->has('compare'))
// {
//     $compare  = request()->compare;
//     $compare  =  str_replace(["[","]"," "],"",$compare);
//     $compare  = explode(",", $compare);
//     $compares = $compare;
// }

//  $data['is_fav']     = in_array($vehicle['id'],$favs) ?  true :  false;
//  $data['is_compare'] = in_array($vehicle['id'],$compares) ? true : false ;

if(request()->has('fav'))
{ 
 $data['is_fav'] = false;

 if(is_array(request()->fav))
 {
   $data['is_fav']     =  in_array($vehicle['id'],request()->fav) ?  true :  false; 
 }
  
}
if(request()->has('compare'))
{
   $data['is_compare'] =  false; 
 if(is_array(request()->compare))
 {
  $data['is_compare'] = in_array($vehicle['id'],request()->compare) ? true : false ;
 }
}

if(Auth::check())
{
  

    if(Auth::user()->id == $vehicle['user_id'])
    {
        $data['can_delete']=true;
        $data['can_edit']=true;
        $data['can_manage_photos']=true;
        $data['can_print_details']=true;
    }

}
   return $data;

    }



}








