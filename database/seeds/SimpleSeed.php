<?php

use Illuminate\Database\Seeder;

class SimpleSeed extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
  
   public function getRandEquType()
    {
    	$yes_no_array = ['EXTR','INTE','EXTE'];

    	$rand_index = array_rand($yes_no_array);
 
		return $yes_no_array[$rand_index];

    }
    public function getRandYesNo()
    {
    	$yes_no_array = ['yes','no'];

    	$rand_index = array_rand($yes_no_array);
 
		return $yes_no_array[$rand_index];

    }

    public function getRandDate()
    {
    	//Generate a timestamp using mt_rand.
		$timestamp = mt_rand(1, time());
		 
		//Format that timestamp into a readable date string.
		$randomDate = date("Y-m-d", $timestamp);
		 
		//Print it out.
		return $randomDate;
    }

    public function run()
    {

    	
        
				  					DB::table('regions')->insert([
																	['name' => 'بيت لحم'],
																	['name' => 'الخليل'],
																	['name' => 'يطا'],
																	['name' => 'الظاهرية'],
																	['name' => 'دورا'],
																	['name' => 'بيت ساحور'],
																	['name' => 'بيت جالا'],
																	['name' => 'بيت لحم'],
																	['name' => 'رام الله '],
																	['name' => 'البيرة'],
																	['name' => 'نابلس'],
																	['name' => 'طولكرم'],
																	['name' => 'سلفيت'],
																	['name' => 'قلقيلية'],
																	['name' => 'اريحا'],
																	['name' => 'طوباس'],
																	['name' => 'جنين'],
																	['name' => 'القدس']
																 ]);

				  				
									//==========================
						

						




						$make = DB::table('types')->insertGetId(
												array(
												'name' => 'الشركة',
												'slug' => 'make_id',
												'logo'=>rand(1,6).'.png',
												'order'=>rand(1,10),
												'search_type'=>'single',
												'search_field'=>'make_id',
												'visible'=>$this->getRandYesNo(),
												)
										);//2


						$model = DB::table('types')->insertGetId(
							array(
							'name' => 'الموديل',
							'slug' => 'model_id',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'single',
							'search_field'=>'model_id',
							'visible'=>$this->getRandYesNo(),
							)
					);//2




			

	
		
				  //===================================================
				 $id =  DB::table('types')->insertGetId(
							array(
							'name' => 'اضافات عامة',
							'slug' => 'extra_equipments',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'multi',
							'search_field'=>'extra_equipments'

							)
					);//7



				 //===================================================

				  				  //===================================================
				 $id =  DB::table('types')->insertGetId(
							array(
							'name' => 'اضافات داخلية',
							'slug' => 'interior_equipments',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'multi',
							'search_field'=>'interior_equipments'

							)
					);//7


				  
				 //===================================================
 					  //===================================================
				 $id =  DB::table('types')->insertGetId(
							array(
							'name' => 'اضافات خارجية',
							'slug' => 'exterior_equipments',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'multi',
							'search_field'=>'exterior_equipments'

							)
					);//7

		
				  
				 //===================================================
			
			

			
	 		


	

    }


function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
}



}
