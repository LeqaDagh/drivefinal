@extends('welcome')

@section('content')
  
    <div class="{{trans('css.text-align')}} {{trans('css.direction')}}">



<ol class="breadcrumb {{trans('css.text-align')}} {{trans('css.direction')}}">
  <li><a href="{{url('')}}">{{trans('main.home')}}</a></li>
  <li><a href="#">{{trans('main.view')}}</a></li>
  <li class="active">{{trans('main.activity')}}</li>
</ol>



  <div >
  <h1><span class="glyphicon glyphicon-th"></span> 
      {{trans('main.do')}}
      {{$activity->name}}
      {{trans('act.function.'.$activity->function)}}
      {{trans('act.controller.'.$activity->controller)}}
    </h1>
  </div>
 
    <?php
      $url  = $activity->controller;
      if($activity->function){

        if($activity->function=='edit'){


          if($activity->url){

            $url.='/'.$activity->url.'/edit';

          }
        }
        else{

          if($activity->url){

            $url.='/'.$activity->url;
          }
        }

      }

    ?>
  


<hr>

    <div class="text-center"><a href="{{url($url)}}" class="btn btn-success">{{trans('main.view')}}</a></div>
    <br>

<div>
@if($activity->text)
{{printArray(unserialize($activity->text))}}
@endif
</div>

<br>

</div>
  @endsection


  <?php 

  function printArray($value){

    echo "<table class='table table-striped'>";
      foreach ($value as $key => $v) {

          if(is_array($v)){

            printArray($value);

          }
          else{
            echo "<tr>";
            echo "<td>".trans('main.'.$key).'</td>';
            echo "<td>".$v.'</td>';
            echo "</tr>";
          }
      }
echo "</table>";

  }

  ?>
