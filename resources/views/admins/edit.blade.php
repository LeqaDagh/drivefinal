@extends('layouts.app')

@section('content')
  @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.admins'),'url'=>url('admins')], 'action' =>trans('main.edit')])

  @include('partials.errors')

        {!! Form::model($admin,['method'=>'PATCH','class'=>'form-horizontal','files' => true,'action'=>['AdminsController@update',$admin->id]]) !!}
	   @include('admins.partials.form',['btn' =>trans('main.edit'), 'form' =>'editing'])
     {!! Form::close() !!}

@endsection





