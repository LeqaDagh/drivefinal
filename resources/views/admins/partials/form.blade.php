


 {!! Form::hidden('form', $form) !!}

 <section class="basic-elements">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('main.admins')</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">

                     

                            <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('region',trans('main.region'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!! Form::select('region_id',$regions,@$user->region_id,['class'=>'form-control','id'=>'region_id']) !!}
                                </fieldset>
                            </div>


                  

                         

                            <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('name',trans('main.name'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('name',old('name'), ['id'=>"name",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>


                                  <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('username',trans('main.username'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('username',old('username'), ['id'=>"username",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>

                              <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('email',trans('main.email'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::email('email',old('email'), ['id'=>"email",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>




                             <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('mobile',trans('main.mobile'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('mobile',old('mobile'), ['id'=>"mobile",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>




                            <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('address',trans('main.address'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('address',old('address'), ['id'=>"address",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>

                    

                        <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('user_status',trans('main.status'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!! Form::select('user_status',trans('main.yes_no'),@$user->region_id,['class'=>'form-control','id'=>'user_status']) !!}
                                </fieldset>
                            </div>

                      @if($form=='adding')

                        <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('password',trans('main.password'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::password('password', ['id'=>"password",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                        </div>


                        <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('password_confirmation',trans('main.password_confirmation'), ['class' => ' '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::password('password_confirmation', ['id'=>"password_confirmation",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                        </div>


                        
                      @endif



                        </div>
                        <div class="form-group overflow-hidden">
                                <div class="col-12">
                                    <button data-repeater-create="" class="btn btn-primary btn-lg">
                                        <i class="icon-plus4"></i>  {{$btn}}
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



