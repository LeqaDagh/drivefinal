<div class="row">
	<div class="col-xl-3 col-lg-6 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('region_id',trans('main.region'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
            {!! Form::select('region_id',$regions,Request::get('region_id'),['class'=>'form-control','id'=>'region_id']) !!}
        </fieldset>
	</div>





  <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('name',trans('main.name'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
            {!!  Form::text('name',Request::get('name'), ['id'=>"name",'class'=>"form-control"]) !!}
        </fieldset>
  </div>

    <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('username',trans('main.username'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
            {!!  Form::text('username',Request::get('username'), ['id'=>"username",'class'=>"form-control"]) !!}
        </fieldset>
    </div>
  <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
    <fieldset class="form-group">
        {!! Form::label('email',trans('main.email'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
        {!!  Form::text('email',Request::get('email'), ['id'=>"email",'class'=>"form-control"]) !!}
    </fieldset>
</div>



 <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
    <fieldset class="form-group">
        {!! Form::label('mobile',trans('main.mobile'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
        {!!  Form::text('mobile',Request::get('mobile'), ['id'=>"mobile",'class'=>"form-control"]) !!}
    </fieldset>
</div>




<div class="col-xl-3 col-lg-6 col-md-12 mb-1">
    <fieldset class="form-group">
        {!! Form::label('user_status',trans('main.status'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
        {!! Form::select('user_status',$user_status,Request::get('user_status'),['class'=>'form-control','id'=>'user_status']) !!}
    </fieldset>
</div>
</div>