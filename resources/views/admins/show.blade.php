@extends('layouts.app')

@section('content')


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.admins'),'url'=>url('admins')], 'action' =>trans('main.show')])
<h1>{{$admin->name}}</h1>
@endsection
