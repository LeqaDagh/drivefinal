@extends('layouts.app')

@section('content')
        @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.attributes'),'url'=>url('attributes')], 'action' =>trans('main.create')])
        @include('partials.errors')

            {!! Form::open(['url' => 'attributes','class'=>'form-horizontal']) !!}
                @include('attributes.partials.form',['btn' =>trans('main.create'), 'form' =>'adding'])
            {!! Form::close() !!}

@endsection
    




