@extends('layouts.app')

@section('content')
<div >
	


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.attributes'),'url'=>url('attributes')], 'action' =>trans('main.view')])


<div class="card">
    		@include('partials.card_header', ['title' =>trans('main.attributes')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">
					 @can('create_attributes')
					 <div >
					  <a href="{{url('attributes','create')}}" class="btn btn-primary">@lang('main.create')</a>
					 </div>
					 @endcan 
					</p>
           
                </div>
                
                    @if (count($attributes))
                    <div class="table-responsive">

<table class="table table-striped " >
	<thead>
	<tr>
		<th>{{trans('main.id')}}</th>
		<th>{{trans('main.name')}}</th>
		<th>{{trans('main.slug')}}</th>
		<th>{{trans('main.logo')}}</th>
		<th>{{trans('main.parent')}}</th>
		<th>{{trans('main.type')}}</th>
		<th>{{trans('main.order')}}</th>
		<th>{{trans('main.created_at')}}</th>
		<th class="text-center">{{trans('main.options')}}</th>

		@can('show_attributes')
		<th >{{trans('main.show')}}</th>
		@endcan

	</tr>
</thead>
<tbody>
	@foreach ($attributes as $attribute)
	<tr>
		<th scope="row">{{$attribute->id}}</th>
		<td>{{$attribute->name}}</td>
		<td>{{$attribute->slug }}</td>
		<td>{{$attribute->logo }}</td>
		<td>{{@$attribute->father->name }}</td>
		<td>{{@$attribute->type->name }}</td>
		<td>{{$attribute->order }}</td>
		<td>{{$attribute->created_at}}</td>
		<td class="text-center">

		@can('edit_attributes')
		<a href="{{url('attributes',$attribute->id)}}/edit"><i class="fas fa-edit"></i></a>
		@endcan
		
		@can('destroy_groups')
		<i class="fas fa-trash-alt delete-item"  delete_item_id="{{$attribute->id}}"></i>
		@endcan

		</td>

		@can('show_attributes')
		<td><a href="{{url('attributes',$attribute->id)}}">{{trans('main.show')}}</a></td>
		@endcan
	</tr>
			
		@endforeach
	</tbody>
</table>

{{$attributes->appends(request()->query())}}
</div>
	@endif
                </div>
            </div>
        </div>






	
@endsection


