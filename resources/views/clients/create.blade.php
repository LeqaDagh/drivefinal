@extends('layouts.app')

@section('content')
        @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.clients'),'url'=>url('clients')], 'action' =>trans('main.create')])
        @include('partials.errors')

            {!! Form::open(['url' => 'clients','class'=>'form-horizontal']) !!}
                @include('clients.partials.form',['btn' =>trans('main.create'), 'form' =>'adding'])
            {!! Form::close() !!}

@endsection
    

@section('js')

<script type="text/javascript">
	
getMaxClientNumber();

$( document ).delegate( "#station_id", "change", function() {

	getMaxClientNumber();

});

function getMaxClientNumber()
{
	// get transaction id from div
	var station_id =$('#station_id').val();

	$.post( '{{url("clients/get_max_prefix_number")}}', {station_id:station_id})
	.done(function( data ) {

	$('#prefix_number').val(data);
	});  

}

</script>
@endsection

