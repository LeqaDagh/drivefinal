<script type="text/javascript">
	
	  $( document ).delegate( ".show_vehicle_for_seller", "click", function() {

	  	 // get transaction id from div
         var seller_id =$(this).attr('seller_id');

         //call by ajax
         $.post( '{{url("clients/show_vehicle_for_seller")}}', {seller_id:seller_id})
          .done(function( data ) {


                          //remove model form html dom if exist
                $('#show_vehicle_for_seller_modal').remove();

                //append model to html dom
                $( "body").append( data );

                //popup a model
                $('#show_vehicle_for_seller_modal').modal();


             

            }); 

	  });


	  
</script>