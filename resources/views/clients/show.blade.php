@extends('layouts.app')

@section('content')


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.clients'),'url'=>url('clients')], 'action' =>trans('main.show')])
<h1>{{$client->name}}</h1>



   <div class="row">
        <div class="col-md-8">
        

            		<div class="card">
		                <div class="card-header">
		                    <h4 class="card-title">@lang('main.client')</h4>
		                </div>
		                <div class="card-content">
		                    <div class="card-body">

		                    	<table class="table table-striped " >
									
									  	 <tr><td>{{trans('main.id')}}</td><td>{{$client->id}}</td></tr>  	 
									     <tr><td>{{trans('main.name')}}</td><td>{{$client->name}}</td></tr>
									     <tr><td>{{trans('main.region')}}</td><td>{{@$client->region->name}}</td></tr>
									     <tr><td>{{trans('main.mobile')}}</td><td>{{$client->mobile}}</td></tr>
									     <tr><td>{{trans('main.status')}}</td><td> {{trans('main.yes_no.'.$client->user_status)}}</td></tr>

							
								</table>
		                    </div>
		                </div>
		            </div>
        </div>

	


    </div>






@endsection


 
  
   
