<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            color: black;
        }
        .sticky {position: fixed;}
        .right {right: 5.8rem;}
        .left {left:5.8rem;}
        .bottom {bottom: 2px}
        
        .social:hover {
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -o-transform: scale(1.1);
        }
        .social {
            -webkit-transform: scale(0.9);
            -moz-transform: scale(0.9);
            -o-transform: scale(0.9);
            -webkit-transition-duration: 0.5s;
            -moz-transition-duration: 0.5s;
            -o-transition-duration: 0.5s;
        }  

        @media (max-width: 576px) {
            .social:hover {
                -webkit-transform: scale(2.1);
                -moz-transform: scale(2.1);
                -o-transform: scale(2.1);
            }
        }
        @media (max-width: 576px) {
            .social {
                bottom:-130px;
                -webkit-transform: scale(2.9);
                -moz-transform: scale(2.9);
                -o-transform: scale(2.9);
                -webkit-transition-duration: 0.5s;
                -moz-transition-duration: 0.5s;
                -o-transition-duration: 0.5s;
            }
        }

        .compareButton {
            -webkit-transform: scale(0.9);
            -moz-transform: scale(0.9);
            -o-transform: scale(0.9);
            -webkit-transition-duration: 0.5s;
            -moz-transition-duration: 0.5s;
            -o-transition-duration: 0.5s;
        }

        .compareButton:hover {
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -o-transform: scale(1.1);
        }
            

        @media (max-width: 576px) {
            .compareButton:hover {
                -webkit-transform: scale(1.11);
                -moz-transform: scale(1.11);
                -o-transform: scale(1.11);
            }
        }
        @media (max-width: 576px) {
            .compareButton {
                bottom:12.4rem;
                -webkit-transform: scale(1.1);
                -moz-transform: scale(1.1);
                -o-transform: scale(1.1);
                -webkit-transition-duration: 0.5s;
                -moz-transition-duration: 0.5s;
                -o-transition-duration: 0.5s;
            }
        }
    </style>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'AutoAndDrive') }}</title>

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">   
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <!--<link rel="stylesheet" href="{{url('/mdb/bootstrap.css')}}">
        <link rel="stylesheet" href="{{url('/mdb/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{url('/mdb/mdb.lite.css')}}">
        <link rel="stylesheet" href="{{url('/mdb/mdb.css')}}">
        <link rel="stylesheet" href="{{url('/mdb/mdb.min.css')}}">
        <link rel="stylesheet" href="{{url('/mdb/mdb.lite.min.css')}}">-->

        <link rel="stylesheet" href="{{url('/css/style.css')}}">
        <link rel="stylesheet" href="{{url('/css/bootstrap1.min.css')}}">
        <link rel="stylesheet" href="{{url('/css/select.css')}}">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css"
        rel="stylesheet"> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <link rel="stylesheet" href="{{url('/css/util.css')}}">
        <link rel="stylesheet" href="{{url('/css/main.css')}}">
        <link rel="stylesheet" href="{{url('/css/multi.css')}}">
        <link rel="stylesheet" href="{{url('/css/sweetalert2.min.css')}}">
    </head>
    <body class="{{ $class ?? ''}}">
        
        <div class="wrapper d-flex align-items-stretch">
            @auth()
                @include('layouts.page_templates.auth')
            @endauth
            
            @guest
                @include('layouts.page_templates.guest')
            @endguest
        </div>
        
        <footer class="footer">
            <div class="container-fluid ">
                <div class="container">
                    <div class="d-flex flex-row">
                        <div class="col-lg-5 col-md-5 col-sm-5">
                            <div class="row ">
                                <div class="col-sm-5 text-white">
                                    <a  class="btn  sticky bottom right social socialAndroid" style="cursor:pointer; color:white;
                                    background-image: url('/android1.png'); width:13%; height:13%; background-size: 100%;
                                    background-repeat: no-repeat">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5">
                            <div class="d-inline-block">
                                <a class="btn  sticky bottom left social socialIOS" style="cursor:pointer; color:white;
                                    background-image: url('/ios1.png'); width:13%; height:13%; background-size: 100%;
                                    background-repeat: no-repeat;">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <div class="p-t-50">

        </div>
        <script src="{{url('/js/jquery.min.js')}}"></script>
        <script src="{{url('/js/popper.js')}}"></script>
        
        <script src="{{url('/js/main.js')}}"></script>

        <!--<script rel="stylesheet" src="{{url('/mdb/bootstrap.js')}}"></script>
        <script rel="stylesheet" src="{{url('/mdb/bootstrap.min.js')}}"></script>
        <script rel="stylesheet" src="{{url('/mdb/jquery.js')}}"></script>
        <script rel="stylesheet" src="{{url('/mdb/mdb.js')}}"></script>
        <script rel="stylesheet" src="{{url('/mdb/jquery.min.js')}}"></script>
        <script rel="stylesheet" src="{{url('/mdb/mdb.min.js')}}"></script>-->

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
        <script src="{{url('/js/bootstrap.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

        <script src="http://parsleyjs.org/dist/parsley.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
        <script src="{{url('/js/multi.js')}}"></script>
        <script src="{{url('/js/sweetalert2.all.min.js')}}"></script>
        <script>
        
        // Client Side validation for personal register.
        $('#validate-personal-register-form').parsley();

        // Client Side validation for seller register.
        $('#validate-seller-register-form').parsley();

        $('.socialAndroid').on('click', function() {
            window.location.href = "https://play.google.com/store/apps/details?id=auto.n.drive";
        });

        $('.socialIOS').on('click', function() {
            window.location.href = "https://apps.apple.com/us/app/auto-and-drive/id1519804560#?platform=iphone";
        });

        $("#datepicker").datepicker( {
            minViewMode: 2,
            format: 'yyyy'
        });
        $("#datepicker1").datepicker( {
            minViewMode: 2,
            format: 'yyyy'
        });

        // show more content for vehicle
        $(function(){
            $("#target").css("display","none");
            $(".toggle").click(function(){
                $("#target").slideToggle(1000);
                $("#toggleicon").toggleClass('fa fa-angle-up').toggleClass('fa fa-angle-down');
            })  
        })

        // show more content for edit vehicle
        $(function(){
            $("#target-edit-vehicle").css("display","none");
            $(".toggle-edit").click(function(){
                $("#target-edit-vehicle").slideToggle(1000);
                $("#toggleicon-edit").toggleClass('fa fa-angle-up').toggleClass('fa fa-angle-down');
            })  
        })

        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();
        }

        $(function(){
            //$("#toggleProfileDiv").css("display","none");
            $("#toggleProfile").click(function(){
                swal({
                    html: '@auth()<div class="col-lg-12 col-md-12"><a style="font-size:14px;background:#0070B0" class="btn btn-primary btn-block"'
                        + ' href="{{ route('profile.index') }}"> الملف الشخصي </a></br>'
                        + ' <a style="font-size:14px;background:#0070B0" class="btn btn-primary btn-block"'
                        + ' href="{{route('profile.images', Auth::user()->id)}}">ادارة الصور</a></br>'
                        + ' <a style="font-size:14px;background:#0070B0"" class="btn btn-primary btn-block"'
                        + ' href="{{route('vec-seller.show', Auth::user()->id)}}"> مركباتي </a></br>'
                        + ' <a style="color:white;font-size:14px;background:#0070B0"" class="btn btn-primary btn-block"'
                        + ' data-id="{{Auth::user()->id}}" id="profileSellerCopy"> نسخ الملف الشخصي </a></br>'
                        + ' <form action="{{ route('logout') }}" id="formLogOut" method="POST" '
                        + ' style="display: none;"> @csrf </form> <a  class="btn btn-primaryc btn-block" style="color:white; '
                        + ' cursor:pointer;font-size:14px;background:#0070B0"" onclick="document.getElementById("formLogOut").submit();">خروج </a>'
                        + ' <p id="urlProfileCopied" hidden></p></div>@endauth',
                    showConfirmButton: false,
                });

                var urlV = window.location.href; 
                var element = $("#urlProfileCopied");
                var arrV = urlV.split('more');  
                $("#profileSellerCopy").click(function(){
                    id = $(this).attr('data-id');
                    newUrl = arrV[0] + "all-vehicle/"+ id;
                    element.html(newUrl);
                    copyToClipboard(element);
                    swal("",  "تم نسخ رابط الملف الشخصي بنجاح");
                });
            })  
        })

        $(document).ready(function() {

            $("#formVehicle").css('display','block');

            var fileupload = $("#FileUpload");
            var filePath = $("#spnFilePath");
            var count = 0;
           
            $(".add-multiple-images").click(function() { 
                fileupload.click();

            });
            
            fileupload.change(function () {
                readURL(this);
            });

            function readURL(input) {
                var filesAmount = input.files.length;
               
                if (input.files ) {
                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
                        
                        reader.onload = function (e) {

                            $('#myImg').append('<div class="control-group row"><div class=" col-lg-6 col-sm-6 "> '
                            + '<div id="picturebox"><img src=' + e.target.result + ' style="width:50%; height:100px;">'
                            + '</div></div><div class="col-lg-6 col-sm-6" id="imagebuttonadd" >'
                            + '<div class="input-group-btn"> '
                            + '<button class="btn remove-image" type="button"><i class="fa fa-minus-circle" style="color:#0875ba;"></i> </button>'
                            + '</div></div></div>');
                            
                        }
                        
                        reader.readAsDataURL(input.files[i]);
                    }
                }
            }
            $('#myImg').on('click', '.remove-image', function(){
                $(this).parents(".control-group").remove();
            });
        });



        var vehicle_id = "";
        $(document).ready(function(){

            // Client Side validation for personal-register
            $('#validate-register-form').parsley();
            $('#validate-register-form').on('submit', function(event){
                event.preventDefault();
            });

            // Client Side validation for seller-register
            $('#validate-seller-register-form').parsley();
            $('#validate-seller-register-form').on('submit', function(event){
                event.preventDefault();
            });

            var carcolor="", carColorCode = "", carColorCodeVal = "";
            $('.ul-car-color').on('click', 'li', function(e){
                var current = this;     
                carColorCode = $(this).attr("value");
                carColorCodeVal = carColorCode.substr(1);
                carColorCodeVal = 'c' + carColorCodeVal;
                carcolor = $(this).attr('data-id');
                if(carColorCode.length > 0) {
                    $(this).find('.checkafter').toggleClass('fa fa-check');
                }

                $("#chooseCarColor").click(function () {  
                
                    $("#car-color-modal").modal('hide');
                    $("#color-car").css('background-color',carColorCode);
                    $("#color-car").css('color','white');
                    $("#cars-color span").text(carcolor);    
                    $("#body_color").val(carColorCodeVal);  
                    $(".checkafter").removeClass('fa fa-check');
                });
                
            });

            var color="", interiorColorCode="";
            $('.ul-interior-color').on('click', 'li', function(e){  
                var current = this;     
                interiorColor = $(this).attr("value");
                interiorColorCode = interiorColor.substr(1);
                interiorColorCode = 'c' + interiorColorCode;
                color = $(this).attr('data-id');
                if(color.length > 0) {
                    $(this).find('.checkafter').toggleClass('fa fa-check');
                }
            });

            $(document).on('click', '.chooseinteriorColor', function(){ 
                $("#interior-color-modal").modal('hide');
                $("#color-interior").css('background-color',interiorColor);
                $("#color-interior").css('color','white');
                $("#interior-color span").text(color);    
                $("#interior_color").val(interiorColorCode); 
                $(".checkafter").removeClass('fa fa-check');
            });

            

            var $body = "", $make_id = "", $companyName = "", $model_id = "", companyLogo = "", modelName = "",
            logoWidth = "", logoHeight= ""; 
            // Body type value
            $('.bodyType input').on('change', function() {
                $bodyTypeVal = $('input[name=test]:checked').val();
                $("#body").val($bodyTypeVal);
                $body = $("#body").val();
            });

            // Type & Modal values for the car
            $(document).on('click', '.modelsCars', function(){ 
                id = $(this).attr('data-id');
                $(".company-model-body").html("");
                var html="";
                $make_id = id;
                $.ajax({
                    url: '/make/'+ id,
                    method:"GET",
                    dataType:"json",
                    
                    success: function(data){
                        $companyName = data[0].name;
                        companyLogo = data[0].logo;
                        logoWidth = data[0].web_width;
                        logoHeight = data[0].web_height;
                    }
                });

                $.ajax({
                    url: '/vehicle/'+ id,
                    method:"GET",
                    dataType:"json",
                    
                    success: function(data) {
                        var datalength = data.length;
                        html += '<table class="table table-bordered" >';
                        $(".company-model-title").html($companyName);
                        for(var count=0; count < datalength; count++) {
                            
                            var length = data[count].length;
                            if(length == 1) {
                                var c = count;
                                html += '<tr>';
                                html += '<td><input class="radioModelName" data-id="'+ data[c][0].name +'" value="'+ data[c][0].id +'" type="radio" name="optradio">  ' + '   ' + data[c][0].name +'</td>';  
                                html += '</tr>';
                            } else {
                                var cnt = count;
                                id = data[cnt][0].id;
                                html += '<tr rel="'+ data[cnt][0].id +'" class="rrr"><td><a data-id="'+ data[cnt][0].id +'" class="testttt"';
                                html += 'style="cursor:pointer">'+ data[cnt][0].name  +'</a></td></tr>';
                                
                                for(var i=1; i < length; i++) {
                                    
                                    html += '<tr class="moreModels11 row'+ id +'Collapse"style="display:none" > ';
                                    html += '<td><input class="radioModelName" data-id="'+ data[cnt][i].name +'" value="'+ data[cnt][i].id +'" type="radio" name="optradio">  ' + '   ' + data[cnt][i].name +'</td>';  
                                    html += '</tr>';
                                }
                            }
                        }
                        html += '</table>'; 
                        $('.company-model-body').append(html); 
                        $('#company-model-modal').modal('show'); 
                        $("#make").val($companyName);
                        $make = $("#make").val();
                        
                      

                        $('.radioModelName').on('change', function() {
                            $model_id = $(this).val();
                            modelName = $(this).attr('data-id');
                            
                        });

                        $('.company-model-modal-submit').on('click', function() {
                            $('#company-model-modal').modal('hide'); 
                            var logoPath = "/images/logos/"+companyLogo;
                            $("#bodyTypeLogos").fadeOut(200);
                            $('#bodyTypeLogoChoosen').empty().append('<div class="col-md-3 col-sm-3"> '
                                + '<img  src="'+ logoPath +' " width="'+ logoWidth +'"; height="'+ logoHeight+'";></div>'
                                + '<div class="col-md-3 col-sm-3"><label>'+ $companyName +'</label></div>'
                                + '<div class="col-md-3 col-sm-3"><label>'+ modelName +'</label></div>'
                                + '<div class="col-md-3 col-sm-3"><button class="btn remove-image-logo" type="button"><i class="fa fa-window-close" style="color:black; font-size:1.5em;"></i> </button>'
                                + '</div>'
                            );
                            $('.remove-image-logo').on('click', function() {
                                $("#bodyTypeLogoChoosen").empty();
                                $("#bodyTypeLogos").fadeIn(200);
                            });
                        });

                    }
                });
                
            });
        
            $(document).on('click', '.rrr' , function() { 
                var rel = $(this).attr('rel');
                $('.row' + rel + 'Collapse').slideToggle();
            });
            
            // Client Side validation for add vehicle
            $('#validate-vehicle-form').parsley();
       
            $('#validate-vehicle-form').on('submit', function(event){
                var data =  $(this).serialize()+ "&make_id=" + $make_id + "&mould_id=" + $model_id + 
                    "&body=" + $body + "&body_color=" + carColorCodeVal + "&interior_color=" + interiorColorCode;
                
                event.preventDefault();
                if($('#validate-vehicle-form').parsley().isValid()) {
                    $.ajax({
                        url : '/vehicle',
                        method:"POST",
                        data: data,
                        dataType:"json",
                        success:function(data)
                        {
                            if (data == "غير مسموح الاضافة") {
                                errorsHtml = '<div id="errorAdding" class="alert alert-danger">'+ data +'</div>';
                                $('#vehicleError').html(errorsHtml);
                                $('html, body').animate({ scrollTop: 0 }, 'fast');
                            } else {
                                vehicle_id = data.id;
                                swal("", "تم اضافة معلومات المركبة بنجاح", "success");
                                $('#validate-vehicle-form')[0].reset();
                                $('#validate-vehicle-form').parsley().reset();
                                $("#formVehicle").css('dispaly','none');
                                $("#formVehicle").fadeOut(200);
                                $("#imagesForVehicle").css('display','block');
                            }
                        },
                    });
                }
            });
        });


        $(document).ready(function() {

            $("#upload-multiple-images").fadeOut(200);
            var fileupload = $("#ImagesVehicleUpload");
            var count = 0;

            $(".add-multiple-images").click(function() { 
                fileupload.click();

            });

            fileupload.change(function () {
                readURL(this);
            });

            function readURL(input) {
                var filesAmount = input.files.length;
            
                if (input.files ) {
                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
                        
                        reader.onload = function (e) {

                            $('#myImg').append('<input id="" value="' + vehicle_id + '" style="display:none" disabled>'
                            + '<div class="control-group text-center d-flex flex-row"><div class=" col-lg-8 col-sm-8 "> '
                            + '<div class="card-img-top "><img src=' + e.target.result + ' style="width:100%; height:70px; object-fit: contain">'
                            + '</div></div><div class="col-lg-4 col-sm-4" id="imagebuttonadd" >'
                            + '<div class="input-group-btn"></br>'
                            + '<button class="btn remove-image" type="button"><i class="fa fa-minus-circle" style="color:#0875ba; font-size:1.1em;"></i> </button>'
                            + '</div></div></div>');
                            $("#upload-multiple-images").fadeIn(200);
                        }
                        $("#vehicleId").val(vehicle_id );
                        reader.readAsDataURL(input.files[i]);
                    }
                }
            }
            
            $('#myImg').on('click', '.remove-image', function(){
                $(this).parents(".control-group").remove();
            });

        });

        // select all price type
        $('#price_type_check').on('change', function() {
            if($(this).is(":checked")) {
                $('.price_type_checkbox').prop("checked", true); 
            } else {
                $('.price_type_checkbox').prop("checked", false); 
            }
            
        });

        // select all external extra
        $('#ext_ext_other_check').on('change', function() {
            if($(this).is(":checked")) {
                $('.ext_ext_other_checkbox').prop("checked", true); 
            } else {
                $('.ext_ext_other_checkbox').prop("checked", false); 
            }
        });

        // select all sensors 
        $('#ext_ext_sensors_check').on('change', function() {
            if($(this).is(":checked")) {
                $('.ext_ext_sensors_checkbox').prop("checked", true); 
            } else {
                $('.ext_ext_sensors_checkbox').prop("checked", false); 
            }
        });

        // select all other extra
        $('#ext_gen_other_check').on('change', function() {
            if($(this).is(":checked")) {
                $('.ext_gen_other_checkbox').prop("checked", true); 
            } else {
                $('.ext_gen_other_checkbox').prop("checked", false); 
            }
        });

        // select all cameras 
        $('#ext_ext_cameras_check').on('change', function() {
            if($(this).is(":checked")) {
                $('.ext_ext_cameras_checkbox').prop("checked", true); 
            } else {
                $('.ext_ext_cameras_checkbox').prop("checked", false); 
            }
        });

        // select all mirrors
        $('#ext_ext_mirrors_check').on('change', function() {
            if($(this).is(":checked")) {
                $('.ext_ext_mirrorsbox').prop("checked", true); 
            } else {
                $('.ext_ext_mirrorsbox').prop("checked", false); 
            }
        });

        // select all external lights
        $('#ext_ext_light_check').on('change', function() {
            if($(this).is(":checked")) {
                $('.ext_ext_light_checkbox').prop("checked", true); 
            } else {
                $('.ext_ext_light_checkbox').prop("checked", false); 
            }
        });

        // select all internal extra
        $('#ext_int_other_check').on('change', function() {
            if($(this).is(":checked")) {
                $('.ext_int_other_checkbox').prop("checked", true); 
            } else {
                $('.ext_int_other_checkbox').prop("checked", false); 
            }
        });

        // select all internal glass 
        $('#ext_int_glass_check').on('change', function() {
            if($(this).is(":checked")) {
                $('.ext_int_glass_checkbox').prop("checked", true); 
            } else {
                $('.ext_int_glass_checkbox').prop("checked", false); 
            }
        });

        // select all screens
        $('#ext_int_screens_check').on('change', function() {
            if($(this).is(":checked")) {
                $('.ext_int_screens_checkbox').prop("checked", true); 
            } else {
                $('.ext_int_screens_checkbox').prop("checked", false); 
            }
        });

        // select all steering
        $('#ext_int_steering_check').on('change', function() {
            if($(this).is(":checked")) {
                $('.ext_int_steering_checkbox').prop("checked", true); 
            } else {
                $('.ext_int_steering_checkbox').prop("checked", false); 
            }
        });

        // select all seats
        $('#ext_int_seats_check').on('change', function() {
            if($(this).is(":checked")) {
                $('.ext_int_seats_checkbox').prop("checked", true); 
            } else {
                $('.ext_int_seats_checkbox').prop("checked", false); 
            }
        });

        $('#whatsAppLink').on('click', function() {
            whatsAppNumber = $(this).attr("data-id");
            swal("تواصل معنا على رقم الواتس",  whatsAppNumber, "info");
        });
    </script>
    </body>
</html>