<script src="{{url('/js/jquery.min.js')}}"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<style>

.navbar {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex-pack: justify;
    justify-content: space-between;
    padding: 5px;
}

/*
headeer top
*/
.topbar{
  background-color: #212529;
  padding: 0;
}

.topbar .container .row {
  margin:-7px;
  padding:0;
}

.topbar .container .row .col-md-12 { 
  padding:0;
}

.topbar p{
  margin:0;
  display:inline-block;
  font-size: 13px;
  color: #f1f6ff;
}

.topbar p > i{
  margin-right:5px;
}
.topbar p:last-child{
  text-align:right;
} 

header .navbar {
    margin-bottom: 0;
}

.topbar li.topbar {
    display: inline;
    padding-right: 18px;
    line-height: 52px;
    transition: all .3s linear;
}

.topbar li.topbar:hover {
    color: #1bbde8;
}

.topbar ul.info i {
    color: #131313;
    font-style: normal;
    margin-right: 8px;
    display: inline-block;
    position: relative;
    top: 4px;
}

.topbar ul.info li {
    float: right;
    padding-left: 30px;
    color: #ffffff;
    font-size: 13px;
    line-height: 44px;
}

.topbar ul.info i span {
    color: #aaa;
    font-size: 13px;
    font-weight: 400;
    line-height: 50px;
    padding-left: 18px;
}

ul.social-network {
  border:none;
  margin:0;
  padding:0;
}

ul.social-network li {
  border:none;  
  margin:0;
}

ul.social-network li i {
  margin:0;
}

ul.social-network li {
    display:inline;
    margin: 0 5px;
    border: 0px solid #2D2D2D;
    padding: 5px 0 0;
    width: 32px;
    display: inline-block;
    text-align: center;
    height: 32px;
    vertical-align: baseline;
    color: #000;
}

ul.social-network {
  list-style: none;
  margin: 5px 0 10px -25px;
  float: right;
}



.waves-effect {
    position: relative;
    cursor: pointer;
    display: inline-block;
    overflow: hidden;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: transparent;
    vertical-align: middle;
    z-index: 1;
    will-change: opacity, transform;
    transition: .3s ease-out;
    color: #fff;
}
a {
  color: #0a0a0a;
  text-decoration: none;
}

li {
    list-style-type: none;
}
.bg-image-full {
    background-position: center center;
    background-repeat: no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
}
.bg-dark {
    background-color: #222!important;
}

.mx-background-top-linear {
    background: -webkit-linear-gradient(45deg, #ffffff 48%, #1b1e21 48%);
    background: -webkit-linear-gradient(left, #ffffff 48%, #1b1e21 48%);
    background: linear-gradient(45deg, #ffffff 48%, #1b1e21 48%);
}

@media(min-width: 768px) {
    .navbar .navbar-nav {
        float: right;
    }
}
</style>

<div class="fixed-top">
    <header class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="social-network">
                        <li>
                            <a class="waves-effect waves-dark" href="https://www.facebook.com/Auto-and-drive-102747047769028/">
                                <i style="color:#0062ff" class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a class="waves-effect waves-dark" href="https://www.instagram.com/autoand.drive/">
                                <i style="color:#ff0080" class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a class="waves-effect waves-dark" href="#">
                                <i style="color:#33cc33" class="fa fa-whatsapp"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <nav class="navbar navbar-expand-lg navbar-dark mx-background-top-linear">
        <div class="container">
            <img src="{{url('/logo.png')}}" width=45>  
            <a class="navbar-brand" rel="nofollow" target="_blank" href="{{ route('homepage') }}" 
                style="color:black"> AutoAndDrive
            </a>
            <button class="navbarResponsiveButton navbar-toggler" id="navbarResponsiveButton" type="button" 
            data-toggle="collapse" data-target="#navbarResponsive"
             aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbarResponsiveMobile collapse navbar-collapse" id="navbarResponsive" style="visibility: hidden;">
                <ul class="navbar-nav ml-auto">  
                    <li class="nav-item active">
                        @auth()
                        <a class="nav-link" href="{{ route('homepage') }}">
                            <span></span>  {{ Auth::user()->name }}
                            <i class="material-icons">perm_identity</i> 
                        </a>
                        @endauth
                    </li>
                    <li class="nav-item"  id="homeLink">
                        <a class="nav-link" href="{{ route('homepage') }}">
                            <span></span>  الرئيسية
                            <i class="material-icons">home</i> 
                        </a>
                    </li>
                    <li class="nav-item" id="searchLink">
                        <a class="nav-link" href="{{ route('search.index') }}">
                            <span ></span>  البحث 
                            <i class="material-icons">search</i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('vehicle.index') }}">
                            <span> </span>  الاِضافة
                            <i class="material-icons">post_add</i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('compare.index') }}"> 
                            <span></span> المقارنة 
                            <i class="material-icons">compare</i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('stared.index') }}"> 
                            <span></span> المميزة 
                            <i class="material-icons">star_half</i> 
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('more.index') }}"> 
                            <span ></span> المزيد 
                            <i class="material-icons">more_vert</i>
                        </a>
                    </li>
                </ul>
            </div>
            
            <div class="navbarResponsiveWeb collapse navbar-collapse " id="navbarResponsive">
                <ul class="navbar-nav ml-auto">   
                
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('more.index') }}"> 
                            <span ></span> المزيد 
                            <i class="material-icons">more_vert</i>
                        </a>
                    </li>

                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('stared.index') }}"> 
                            <span></span> المميزة 
                            <i class="material-icons">star_half</i> 
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('compare.index') }}"> 
                            <span></span> المقارنة 
                            <i class="material-icons">compare</i>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('vehicle.index') }}">
                            <span> </span>  الاِضافة
                            <i class="material-icons">post_add</i>
                        </a>
                    </li>

                    <li class="nav-item" id="searchLink">
                        <a class="nav-link" href="{{ route('search.index') }}">
                            <span ></span>  البحث 
                            <i class="material-icons">search</i>
                        </a>
                    </li>

                    <li class="nav-item"  id="homeLink">
                        <a class="nav-link" href="{{ route('homepage') }}">
                            <span></span>  الرئيسية
                            <i class="material-icons">home</i> 
                        </a>
                    </li>
                    <li class="nav-item active">
                        @auth()
                        <a class="nav-link" href="{{ route('homepage') }}">
                            <span></span>  {{ Auth::user()->name }}
                            <i class="material-icons">perm_identity</i> 
                        </a>
                        @endauth
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>

<script>

  
    var url = window.location.href;
    var arr = url.split('http://')[0];

    if (url.indexOf("search") >= 0) {

        $("#homeLink").css('display','block');
        $("#searchLink").css('display','none');
    } else if (url.indexOf("home") >= 0 || arr.indexOf("") == 0) {

        $("#searchLink").css('display','block');
        $("#homeLink").css('display','none');
    }
    
    $(window).resize(function() {
        if ($(this).width() < 500) {
            $('.navbarResponsiveWeb').css("display", "none");
            $('.navbarResponsiveMobile').css("visibility", "visible");
        } else if ($(this).width() > 500) {
            $('.navbarResponsiveWeb').css("display", "block");
            $('.navbarResponsiveMobile').css("visibility", "hidden");
        }
    });

</script>