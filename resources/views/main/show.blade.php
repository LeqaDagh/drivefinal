
@extends('layouts.main')

@section('content')
<div class="row">
 
 <div class="col-lg-8" >
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative" style="background: white;">

        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">{{$vehicle->make->name}},{{$vehicle->model->name}}</h3>
          <div class="mb-1 text-muted">{{@$vehicle->seller->name}},{{@$vehicle->region->name}}</div>
          <p class="card-text mb-auto">
          	 <div class="form-row">
          		<div class="col">{{@$vehicle->price}}</div>
          		<div class="col">{{@$vehicle->year}}</div>
          		<div class="col">{{@$vehicle->power}}</div>
          		<div class="col">{{@$vehicle->num_of_seats}}</div>
          		<div class="col">{{@$vehicle->body_color}}</div>
          	</div>
          	<div class="form-row">
          		<div class="col">{{@$vehicle->sell_type->name}}</div>
          		<div class="col">{{@$vehicle->vehicle_type->name}}</div>
          		<div class="col">{{@$vehicle->gear->name}}</div>
          		<div class="col">{{@$vehicle->condtion->name}}</div>
          		<div class="col">{{@$vehicle->fuel_type->name}}</div>
          	</div>

          </p>
    
        </div>
      
      </div>
    </div>

         <div class="col-lg-4" >
          	@foreach($vehicle->photos as $photo)
          	<img style="width: 100%;margin: 2px;" src="{{url('public/storage/'.@$photo->disk_name,@$photo->file_name)}}">
          	@endforeach
          </div>
</div>

    @endsection