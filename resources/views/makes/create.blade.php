@extends('layouts.app')

@section('content')
        @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.makes'),'url'=>url('makes')], 'action' =>trans('main.create')])
        @include('partials.errors')

            {!! Form::open(['url' => 'makes','class'=>'form-horizontal']) !!}
                @include('makes.partials.form',['btn' =>trans('main.create'), 'form' =>'adding'])
            {!! Form::close() !!}

@endsection
    





