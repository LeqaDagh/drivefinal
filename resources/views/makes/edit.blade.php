@extends('layouts.app')

@section('content')
  @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.makes'),'url'=>url('makes')], 'action' =>$make->name])


  @include('partials.errors')

        {!! Form::model($make,['method'=>'PATCH','class'=>'form-horizontal','files' => true,'action'=>['MakesController@update',$make->id]]) !!}
       @include('makes.partials.form',['btn' =>trans('main.edit'), 'form' =>'editing'])
     {!! Form::close() !!}

@endsection


@section('js')
  @include('js.csrf')
@endsection



