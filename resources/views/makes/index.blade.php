@extends('layouts.app')

@section('content')
<div >
	


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.makes'),'url'=>url('makes')], 'action' =>trans('main.view')])


<div class="card">
    		@include('partials.card_header', ['title' =>trans('main.makes')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">
					 @can('create_makes')
					 <div >
					  <a href="{{url('makes','create')}}" class="btn btn-primary">@lang('main.create')</a>
					 </div>
					 @endcan 
					</p>
           
                </div>
                <div class="table-responsive">
                    @if (count($makes))

<table class="table table-striped " >
	<thead>
	<tr>
		<th>{{trans('main.id')}}</th>
		<th>{{trans('main.name')}}</th>
		<th>{{trans('main.style_width')}}</th>
		<th>{{trans('main.style_height')}}</th>
		<th>{{trans('main.order')}}</th>
		<th>{{trans('main.visibility')}}</th>
		<th>{{trans('main.moulds')}}</th>
		<th>{{trans('main.vehicles')}}</th>
		<th>{{trans('main.created_at')}}</th>
		<th class="text-center">{{trans('main.options')}}</th>

		@can('show_makes')
		<th >{{trans('main.show')}}</th>
		@endcan

	</tr>
</thead>
<tbody>
	@foreach ($makes as $make)
	<tr>
		<th scope="row">{{$make->id}}</th>
		<td>{{$make->name}}</td>
		<td>{{$make->style_width}}</td>
		<td>{{$make->style_height}}</td>
		<td>{{$make->order}}</td>
		<td>{{trans('main.yes_no.'.$make->visible)}}</td>
		<td>{{$make->moulds->count()}}</td>
		<td>{{$make->vehicles()->count()}}</td>
		<td>{{$make->created_at}}</td>
		<td class="text-center">

		@can('edit_makes')
		<a href="{{url('makes',$make->id)}}/edit"><i class="fas fa-edit"></i></a>
		@endcan
		
		@can('destroy_makes')
		<i class="fas fa-trash-alt delete-item"  delete_item_id="{{$make->id}}"></i>
		@endcan

		</td>

		@can('show_makes')
		<td><a href="{{url('makes',$make->id)}}">{{trans('main.show')}}</a></td>
		@endcan
	</tr>
			
		@endforeach
	</tbody>
</table>

{{$makes->appends(request()->query())}}
</div>
	@endif
                </div>
            </div>
        </div>






	
@endsection


