@extends('layouts.app')

@section('content')
<div >
	

@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.medias'),'url'=>url('medias')], 'action' =>trans('main.view')])


<div class="card">
    		@include('partials.card_header', ['title' =>trans('main.medias')])
            <div class="card-content collapse show">
              
                
@if (count($medias))
<div class="form-row">

	@foreach ($medias as $media)
	<div class="col-md-3" > 
		<div style="border: 1px solid #aaa;margin: 5px;border-radius: 5px;">
	<img style="width: 100%;height: 200px;border-radius: 5px;" src="{{url('public/storage/'.@$media->disk_name,@$media->file_name)}}">
		<div class="text-center">
				<a href="{{url('medias',$media->id)}}"><h3>{{$media->title}}</h3></a>
		</div>
		</div>
	</div>

	@endforeach
</div>

@endif
{{$medias->appends(request()->query())}}


                </div>
            </div>
        </div>






	
@endsection


