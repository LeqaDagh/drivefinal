@extends('layouts.app')

@section('content')
<div >
	


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.moulds'),'url'=>url('moulds')], 'action' =>trans('main.view')])


<div class="card">
    		@include('partials.card_header', ['title' =>trans('main.moulds')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">
					 @can('create_moulds')
					 <div >
					  <a href="{{url('moulds','create')}}" class="btn btn-primary">@lang('main.create')</a>
					 </div>
					 @endcan 
					</p>
           
                </div>
                <div class="table-responsive">
                    @if (count($moulds))

<table class="table table-striped " >
	<thead>
	<tr>
		<th>{{trans('main.id')}}</th>
		<th>{{trans('main.name')}}</th>
		<th>{{trans('main.parent')}}</th>
		<th>{{trans('main.make')}}</th>
		<th>{{trans('main.logo')}}</th>
		<th>{{trans('main.order')}}</th>
		<th>{{trans('main.visibility')}}</th>
		<th>{{trans('main.vehicles')}}</th>
		<th>{{trans('main.created_at')}}</th>
		<th class="text-center">{{trans('main.options')}}</th>

		@can('show_moulds')
		<th >{{trans('main.show')}}</th>
		@endcan

	</tr>
</thead>
<tbody>
	@foreach ($moulds as $mould)
	<tr>
		<th scope="row">{{$mould->id}}</th>
		<td>{{$mould->name}}</td>
		<td>{{@$mould->father->name}}</td>
		<td>{{@$mould->make->name}}</td>
		<td>{{$mould->logo}}</td>
		<td>{{$mould->order}}</td>
		<td>{{trans('main.vis_inv_status.'.$mould->visible)}}</td>
		<td>{{$mould->vehicles()->count()}}</td>
		<td>{{$mould->created_at}}</td>
		<td class="text-center">

		@can('edit_moulds')
		<a href="{{url('moulds',$mould->id)}}/edit"><i class="fas fa-edit"></i></a>
		@endcan
		
		@can('destroy_moulds')
		<i class="fas fa-trash-alt delete-item"  delete_item_id="{{$mould->id}}"></i>
		@endcan

		</td>

		@can('show_moulds')
		<td><a href="{{url('moulds',$mould->id)}}">{{trans('main.show')}}</a></td>
		@endcan
	</tr>
			
		@endforeach
	</tbody>
</table>

{{$moulds->appends(request()->query())}}
</div>
	@endif
                </div>
            </div>
        </div>






	
@endsection


