

 {!! Form::hidden('form', $form) !!}

 <section class="basic-elements">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('main.moulds')</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('name',trans('main.name'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                        {!!  Form::text('name',old('name'),['id'=>"name",'class'=>"form-control"]) !!}

                                </fieldset>
                            </div>

                                  <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('parent',trans('main.parent'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                         {!! Form::select('parent',$parents,null,['class'=>'form-control select2l']) !!}

                                </fieldset>
                            </div>

                               <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('make_id',trans('main.make'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                         {!! Form::select('make_id',$makes,null,['class'=>'form-control select2l']) !!}

                                </fieldset>
                            </div>


         <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('logo',trans('main.logo'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                        {!!  Form::text('logo',old('logo'),['id'=>"logo",'class'=>"form-control"]) !!}

                                </fieldset>
                            </div>
         <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('order',trans('main.order'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                        {!!  Form::number('order',old('order'),['id'=>"order",'class'=>"form-control"]) !!}

                                </fieldset>
                            </div>
    <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('visible',trans('main.visibility'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                         {!! Form::select('visible',trans('main.yes_no'),null,['class'=>'form-control select2l']) !!}

                                </fieldset>
                            </div>
                            

  <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('review',trans('main.review'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                        {!!  Form::text('review',old('review'),['id'=>"review",'class'=>"form-control"]) !!}

                                </fieldset>
                            </div>

                         

                        </div>
                        <div class="form-group overflow-hidden">
                                <div class="col-12">
                                    <button data-repeater-create="" class="btn btn-primary btn-lg">
                                        <i class="icon-plus4"></i>  {{$btn}}
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



