@extends('layouts.apps')

@section('content')

    <div id="formVehicle" class="row" style="display:none"> 
        <div class="col-lg-12 col-md-12 col-sm-12" >
            <div class="">
                <center>
                    <h5>
                    من نحن
                    </h5>
                </center>
            </div></br>
            <div class="row">
                <center>
                    <div class="col-lg-10 col-md-10 col-sm-10">
                        <div class="card ">
                            <div class="card-body ">
                            {!! html_entity_decode($aboutUs[0]->settings_value) !!}
                        </div>
                    </div>
                </center>
            </div>
        </div>
    </div>
@endsection