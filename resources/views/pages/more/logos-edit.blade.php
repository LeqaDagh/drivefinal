<div class="float-right row" id="addCar" class="addCar">
    <div class="col-lg-6">
        <label>الشركة </label>
        <input style="border: 0 !important;  box-shadow: none !important;background-color: white 
            !important;cursor: default !important;" disabled >
        <input id="makeEdit" name="make_id" style="display:none; border: 0 !important;
            box-shadow: none !important;background-color: white !important;cursor: default !important;"
        placeholder="{{ __('  ') }}" type="text" value="{{$vehicle[0]->make_id}}"
        data-parsley-required-message="   الشركة مطلوب"
        disabled required>
    </div>
</div></br></br></br>

<div id="bodyTypeLogosEdit" class="row">
    @foreach($firstMakes as $key => $data)
    <div class="col-lg-2 col-md-3 col-sm-3">
        <div class=" card-stats">
            <center>
                <div class="card-body ">
                    <a class="modelsCarsEdit" data-id="{{$data->id}}" >
                        <div >
                            <img src="{{url('/images/logos/'.$data->logo)}}" width=55; height=55;>
                        </div>
                    </a>
                </div>
                <div class="stats">
                    {{$data->name}}
                </div>
            </center>
        </div>
    </div>
    @endforeach

</div></br>
<div id="bodyTypeLogoChoosenNewValue" class="row">
    
</div></br>

<div id="bodyTypeLogoChoosenEdit" class="row">
    <div class="col-md-4 col-sm-3"> 
        <img  src="{{url('/images/logos/'.$vehicle[0]->logo)}}" width=55; height=55;>
    </div>
    <div class="col-md-3 col-sm-3">
        <label>{{$vehicle[0]->name}} </label>
    </div>
    <div class="col-md-3 col-sm-3">
        <label>{{$vehicle[0]->model_name}} </label>
    </div>
    <div class="col-md-2 col-sm-3">
        <button class="btn remove-image-logo-edit" type="button" >
            <i class="fa fa-window-close" style="color:black; font-size:1.5em;"></i> 
        </button>
    </div>
</div>

<div class="row" id="viewAllCompanyEdit">
    <div class="col-lg-12 ml-auto mr-auto">
        <div class="row">
            <div class="col-md-12">
                <a href="#" class="toggle btn btn-primary btn-block"  data-toggle="modal" 
                    data-target="#logos-modal">
                    <span>  اظهار كافة الشركات </span><i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
    </div>
</div></br>


<div class="modal" id="logos-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="margin: 0 auto;" class="modal-title">الشركات</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    @foreach($makes as $key => $data)
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class=" card-stats">
                            <center>
                                <div class="card-body ">
                                    <a class="modelsCars" data-id="{{$data->id}}">
                                        <div >
                                            <img src="{{url('/images/logos/'.$data->logo)}}" width=55; height=55;>
                                        </div>
                                    </a>
                                </div>
                                <div class="stats">
                                {{$data->name}}
                                </div>
                            </center>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <button style="margin: 0 auto;" type="button" class="text-center btn btn-primary">اختر</button>
            </div>
        </div>
    </div>
</div>