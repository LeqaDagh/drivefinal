<div id="target-edit-vehicle">
    <div class="row">

        <div class="col-lg-6 col-md-6 ml-auto mr-auto">
            <div class="form-group">
                <input class="form-control" 
                placeholder="الاٍعفاء الجمركي" type="text" 
                disabled >
                
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="customs_exemption1" type="radio" name="customs_exemption" 
                        value="yes" {{ $vehicle[0]->customs_exemption == 'yes' ? 'checked' : '' }} >
                        <label for="customs_exemption1" class="choice-button-text">نعم</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="customs_exemption2" type="radio" name="customs_exemption" 
                        value="no" {{ $vehicle[0]->customs_exemption == 'no' ? 'checked' : '' }}>
                        <label for="customs_exemption2" class="choice-button-text">لا</label>
                    </div> 
                </div>
            </div>


            <div class="form-group">
                <label >تاريخ انتهاء الرخصة
                </label>
                <input class="text-center form-control" name="license_date"
                placeholder="تاريخ انتهاء الرخصة" type="date" value="{{ $vehicle[0]->license_date }}" >
            </div>

            <div class="form-group">
                <input class="form-control"
                placeholder="ملّاك سابقون" type="text" 
                disabled >
                
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="previous_owners1" type="radio" name="previous_owners" 
                        value="ow1" {{ $vehicle[0]->previous_owners == 'ow1' ? 'checked' : '' }}>
                        <label for="previous_owners1" class="choice-button-text">يد أولى</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="previous_owners2" type="radio" name="previous_owners" 
                        value="ow2" {{ $vehicle[0]->previous_owners == 'ow2' ? 'checked' : '' }}>
                        <label for="previous_owners2" class="choice-button-text">يد ثانية</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="previous_owners3" type="radio" name="previous_owners" 
                        value="ow3" {{ $vehicle[0]->previous_owners == 'ow3' ? 'checked' : '' }}>
                        <label for="previous_owners3" class="choice-button-text">يد ثالثة</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="previous_owners4" type="radio" name="previous_owners" 
                        value="ow4" {{ $vehicle[0]->previous_owners == 'ow4' ? 'checked' : '' }} >
                        <label for="previous_owners4" class="choice-button-text">أخرى</label>
                    </div> 
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 ml-auto mr-auto">
            <div class="form-group">
                <input class="form-control"
                placeholder="الأصل" type="text" 
                disabled >
                
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="origin1" type="radio" name="origin" value="spc" 
                        {{ $vehicle[0]->origin == 'spc' ? 'checked' : '' }}>
                        <label for="origin1" class="choice-button-text">خصوصي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="origin2" type="radio" name="origin" value="txi"
                        {{ $vehicle[0]->origin == 'txi' ? 'checked' : '' }} >
                        <label for="origin2" class="choice-button-text">عمومي</label>
                    </div> 
                </div>
            </div>

            <div class="form-group">
                <input class="form-control" 
                placeholder="عدد المفاتيح" type="text" 
                disabled >
                
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="num_of_keys1" type="radio" name="num_of_keys" value="kes1" 
                        {{ $vehicle[0]->num_of_keys == 'kes1' ? 'checked' : '' }}>
                        <label for="num_of_keys1" class="choice-button-text">1</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="num_of_keys2" type="radio" name="num_of_keys" value="kes2"
                        {{ $vehicle[0]->num_of_keys == 'kes2' ? 'checked' : '' }} >
                        <label for="num_of_keys2" class="choice-button-text">2</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="num_of_keys3" type="radio" name="num_of_keys" value="kes3" 
                        {{ $vehicle[0]->num_of_keys == 'kes3' ? 'checked' : '' }}>
                        <label for="num_of_keys3" class="choice-button-text">3</label>
                    </div> 
                </div>
            </div>

            <div class="form-group shadow-textarea">
                <label class="float-right" for="exampleFormControlTextarea6">وصف اضافي</label>
                <textarea class="form-control z-depth-1" name="desc"
                    id="exampleFormControlTextarea6" rows="3" >
                </textarea>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-md-6 ml-auto mr-auto" >
            <div  class="float-right">
            اضافات داخلية
            </div>
        </div>
    </div></br>

    <div class="row">
        <div class="col-lg-6 col-md-6 ml-auto mr-auto">

            <div class="form-group">
                <input class="form-control"
                placeholder="نوع الفرش" type="text" 
                disabled >
                
            </div> 
            @if(isset($furniture[0]) && $furniture[0]->equipment_value !== null)
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture1" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_leat" 
                        {{ $furniture[0]->equipment_value == 'ext_int_furniture_leat' ? 'checked' : '' }}>
                        <label for="ext_int_furniture1" class="choice-button-text">جلد</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_colt" 
                        {{ $furniture[0]->equipment_value == 'ext_int_furniture_colt' ? 'checked' : '' }}>
                        <label for="ext_int_furniture2" class="choice-button-text">قماش</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture3" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_lndc" 
                        {{ $furniture[0]->equipment_value == 'ext_int_furniture_lndc' ? 'checked' : '' }}>
                        <label for="ext_int_furniture3" class="choice-button-text">جلد + قماش</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture4" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_sprt" 
                        {{ $furniture[0]->equipment_value == 'ext_int_furniture_sprt' ? 'checked' : '' }}>
                        <label for="ext_int_furniture4" class="choice-button-text">رياضي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture5" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_velv" 
                        {{ $furniture[0]->equipment_value == 'ext_int_furniture_velv' ? 'checked' : '' }}>
                        <label for="ext_int_furniture5" class="choice-button-text">مخمل</label>
                    </div> 
                </div>
            </div>
            @else 
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture1" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_leat" >
                        <label for="ext_int_furniture1" class="choice-button-text">جلد</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_colt" >
                        <label for="ext_int_furniture2" class="choice-button-text">قماش</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture3" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_lndc" >
                        <label for="ext_int_furniture3" class="choice-button-text">جلد + قماش</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture4" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_sprt" >
                        <label for="ext_int_furniture4" class="choice-button-text">رياضي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture5" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_velv" >
                        <label for="ext_int_furniture5" class="choice-button-text">مخمل</label>
                    </div> 
                </div>
            </div>
            @endif
            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control"  id="inputGroup"
                    placeholder="المقاعد" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_int_seats_check" >
                </div> 
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($seats, 'ext_int_seats_htst')))
                            <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats1" type="checkbox" 
                            name="ext_int_seats[]" value="ext_int_seats_htst" checked>
                            <label for="ext_int_seats1" class="choice-button-text">تدفئة مقاعد</label>
                        @else
                            <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats1" type="checkbox" 
                            name="ext_int_seats[]" value="ext_int_seats_htst" >
                            <label for="ext_int_seats1" class="choice-button-text">تدفئة مقاعد</label>
                        @endif
                        
                    </div> 
                </div>
                
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($seats, 'ext_int_seats_clst')))
                            <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats2" type="checkbox" 
                            name="ext_int_seats[]" value="ext_int_seats_clst" checked>
                            <label for="ext_int_seats2" class="choice-button-text">تبريد مقاعد</label>
                        @else
                            <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats2" type="checkbox" 
                            name="ext_int_seats[]" value="ext_int_seats_clst" >
                            <label for="ext_int_seats2" class="choice-button-text">تبريد مقاعد</label>
                        @endif
                    </div> 
                </div>
                
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($seats, 'ext_int_seats_mast')))
                            <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats3" type="checkbox" 
                            name="ext_int_seats[]" value="ext_int_seats_mast" checked>
                            <label for="ext_int_seats3" class="choice-button-text">مساج مقاعد</label>
                        @else
                            <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats3" type="checkbox" 
                            name="ext_int_seats[]" value="ext_int_seats_mast" >
                            <label for="ext_int_seats3" class="choice-button-text">مساج مقاعد</label>
                        @endif
                    </div> 
                </div>
                
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($seats, 'ext_int_seats_spst')))
                            <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats4" type="checkbox" 
                            name="ext_int_seats[]" value="ext_int_seats_spst" checked>
                            <label for="ext_int_seats4" class="choice-button-text">مقاعد رياضية</label>
                        @else
                            <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats4" type="checkbox" 
                            name="ext_int_seats[]" value="ext_int_seats_spst">
                            <label for="ext_int_seats4" class="choice-button-text">مقاعد رياضية</label>
                        @endif
                    </div> 
                </div>
            </div>
            
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($seats, 'ext_int_seats_elst')))
                            <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats5" type="checkbox" 
                            name="ext_int_seats[]" value="ext_int_seats_elst" checked>
                            <label for="ext_int_seats5" class="choice-button-text">تحكم كهرباء</label>
                        @else
                            <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats5" type="checkbox" 
                            name="ext_int_seats[]" value="ext_int_seats_elst" >
                            <label for="ext_int_seats5" class="choice-button-text">تحكم كهرباء</label>
                        @endif
                    </div> 
                </div>
            
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($seats, 'ext_int_seats_mest')))
                            <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats6" type="checkbox" 
                            name="ext_int_seats[]" value="ext_int_seats_mest" checked>
                            <label for="ext_int_seats6" class="choice-button-text">ذاكرة مقاعد</label>
                        @else
                            <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats6" type="checkbox" 
                            name="ext_int_seats[]" value="ext_int_seats_mest">
                            <label for="ext_int_seats6" class="choice-button-text">ذاكرة مقاعد</label>
                        @endif
                    </div> 
                </div>
            </div>
            

            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control" id="inputGroup"
                    placeholder="المقود" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_int_steering_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($steering, 'ext_int_steering_refi')))
                            <input class="ext_int_steering_checkbox radiochoosen" id="ext_int_steering1" type="checkbox" 
                            name="ext_int_steering[]" value="ext_int_steering_refi" checked>
                            <label for="ext_int_steering1" class="choice-button-text">غيارات مقود</label>
                        @else 
                            <input class="ext_int_steering_checkbox radiochoosen" id="ext_int_steering1" type="checkbox" 
                            name="ext_int_steering[]" value="ext_int_steering_refi" >
                            <label for="ext_int_steering1" class="choice-button-text">غيارات مقود</label>
                        @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($steering, 'ext_int_steering_stco')))
                            <input class="ext_int_steering_checkbox radiochoosen" id="ext_int_steering2" type="checkbox" 
                            name="ext_int_steering[]" value="ext_int_steering_stco" checked>
                            <label for="ext_int_steering2" class="choice-button-text">تحكم مقود</label>
                        @else 
                            <input class="ext_int_steering_checkbox radiochoosen" id="ext_int_steering2" type="checkbox" 
                            name="ext_int_steering[]" value="ext_int_steering_stco" >
                            <label for="ext_int_steering2" class="choice-button-text">تحكم مقود</label>
                        @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($steering, 'ext_int_steering_hste')))
                            <input class="ext_int_steering_checkbox radiochoosen" id="ext_int_steering3" type="checkbox" 
                            name="ext_int_steering[]" value="ext_int_steering_hste" checked>
                            <label for="ext_int_steering3" class="choice-button-text">تدفئة مقود</label>
                        @else
                            <input class="ext_int_steering_checkbox radiochoosen" id="ext_int_steering3" type="checkbox" 
                            name="ext_int_steering[]" value="ext_int_steering_hste">
                            <label for="ext_int_steering3" class="choice-button-text">تدفئة مقود</label>
                        @endif
                    </div> 
                </div>
            </div>

            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control" id="inputGroup"
                    placeholder="الشاشات" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_int_screens_check" >
                </div> 
            </div> 
           
            
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($screens, 'ext_int_screens_frsc')))
                        <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens1" type="checkbox" 
                        name="ext_int_screens[]" value="ext_int_screens_frsc" checked>
                        <label for="ext_int_screens1" class="choice-button-text">شاشة امامية</label>
                        @else 
                        <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens1" type="checkbox" 
                        name="ext_int_screens[]" value="ext_int_screens_frsc" >
                        <label for="ext_int_screens1" class="choice-button-text">شاشة امامية</label>
                        @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($screens, 'ext_int_screens_basc')))
                            <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens2" type="checkbox" 
                            name="ext_int_screens[]" value="ext_int_screens_basc" checked>
                            <label for="ext_int_screens2" class="choice-button-text">شاشات خلفية</label>
                        @else
                            <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens2" type="checkbox" 
                            name="ext_int_screens[]" value="ext_int_screens_basc" >
                            <label for="ext_int_screens2" class="choice-button-text">شاشات خلفية</label>
                        @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($screens, 'ext_int_screens_blue')))
                            <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens3" type="checkbox"
                            name="ext_int_screens[]" value="ext_int_screens_blue" checked>
                            <label for="ext_int_screens3" class="choice-button-text">بلوتوث</label>
                        @else
                            <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens3" type="checkbox"
                            name="ext_int_screens[]" value="ext_int_screens_blue" >
                            <label for="ext_int_screens3" class="choice-button-text">بلوتوث</label>
                        @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($screens, 'ext_int_screens_ebph')))
                            <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens4" type="checkbox" 
                            name="ext_int_screens[]" value="ext_int_screens_ebph" checked>
                            <label for="ext_int_screens4" class="choice-button-text">قاعدة بلوتوث للهاتف</label>
                        @else
                            <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens4" type="checkbox" 
                            name="ext_int_screens[]" value="ext_int_screens_ebph">
                            <label for="ext_int_screens4" class="choice-button-text">قاعدة بلوتوث للهاتف</label>
                        @endif
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">

                
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($screens, 'ext_int_screens_usb')))
                            <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens5" type="checkbox" 
                            name="ext_int_screens[]" value="ext_int_screens_usb" checked>
                            <label for="ext_int_screens5" class="choice-button-text">USB</label>
                        @else
                            <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens5" type="checkbox" 
                            name="ext_int_screens[]" value="ext_int_screens_usb" >
                            <label for="ext_int_screens5" class="choice-button-text">USB</label>
                        @endif
                    </div> 
                </div>
            </div>
        </div>
        
        <div class="col-lg-6 col-md-6 ml-auto mr-auto">
            
            <div class="form-group">
                <input class="form-control"
                placeholder="فتحة السقف" type="text" 
                disabled >
                
            </div> 
            @if(isset($sunroof[0]) && $sunroof[0]->equipment_value !== null)
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_sunroof1" type="radio" 
                        name="ext_int_sunroof" value="ext_int_sunroof_norm" 
                        {{ $sunroof[0]->equipment_value == 'ext_int_sunroof_norm' ? 'checked' : '' }}>
                        <label for="ext_int_sunroof1" class="choice-button-text">عادية</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_sunroof2" type="radio" 
                        name="ext_int_sunroof" value="ext_int_sunroof_pano" 
                        {{ $sunroof[0]->equipment_value == 'ext_int_sunroof_pano' ? 'checked' : '' }}>
                        <label for="ext_int_sunroof2" class="choice-button-text">بانوراما</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_sunroof3" type="radio" 
                        name="ext_int_sunroof" value="ext_int_sunroof_conv" 
                        {{ $sunroof[0]->equipment_value == 'ext_int_sunroof_conv' ? 'checked' : '' }}>
                        <label for="ext_int_sunroof3" class="choice-button-text">سقف متحرك</label>
                    </div> 
                </div>
            </div>
            @else
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_sunroof1" type="radio" 
                        name="ext_int_sunroof" value="ext_int_sunroof_norm" >
                        <label for="ext_int_sunroof1" class="choice-button-text">عادية</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_sunroof2" type="radio" 
                        name="ext_int_sunroof" value="ext_int_sunroof_pano" >
                        <label for="ext_int_sunroof2" class="choice-button-text">بانوراما</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_sunroof3" type="radio" 
                        name="ext_int_sunroof" value="ext_int_sunroof_conv" >
                        <label for="ext_int_sunroof3" class="choice-button-text">سقف متحرك</label>
                    </div> 
                </div>
            </div>
            @endif
            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control"  id="inputGroup"
                    placeholder="الزجاج" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_int_glass_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($glass, 'ext_int_glass_egls')))
                            <input class="ext_int_glass_checkbox radiochoosen" id="ext_int_glass1" type="checkbox" 
                            name="ext_int_glass[]" value="ext_int_glass_egls" checked>
                            <label for="ext_int_glass1" class="choice-button-text">كهربائي</label>
                        @else
                            <input class="ext_int_glass_checkbox radiochoosen" id="ext_int_glass1" type="checkbox" 
                            name="ext_int_glass[]" value="ext_int_glass_egls" >
                            <label for="ext_int_glass1" class="choice-button-text">كهربائي</label>
                        @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($glass, 'ext_int_glass_lsrd')))
                            <input class="ext_int_glass_checkbox radiochoosen" id="ext_int_glass2" type="checkbox" 
                            name="ext_int_glass[]" value="ext_int_glass_lsrd" checked>
                            <label for="ext_int_glass2" class="choice-button-text">تعتيم ليزر</label>
                        @else
                            <input class="ext_int_glass_checkbox radiochoosen" id="ext_int_glass2" type="checkbox" 
                            name="ext_int_glass[]" value="ext_int_glass_lsrd" >
                            <label for="ext_int_glass2" class="choice-button-text">تعتيم ليزر</label>
                        @endif
                    </div> 
                </div>
            </div>

            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control" id="inputGroup"
                    placeholder="اضافات داخلية أخرى" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_int_other_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        @if (strlen(strpos($intOthers, 'ext_int_other_eio1')))
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other1" type="checkbox" 
                        name="ext_int_other[]" value="ext_int_other_eio1" checked>
                        <label for="ext_int_other1" class="choice-button-text">زر تشغيل</label>
                        @else
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other1" type="checkbox" 
                        name="ext_int_other[]" value="ext_int_other_eio1" >
                        <label for="ext_int_other1" class="choice-button-text">زر تشغيل</label>
                        @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($intOthers, 'ext_int_other_eio2')))
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other2" type="checkbox"
                         name="ext_int_other[]" value="ext_int_other_eio2" checked>
                        <label for="ext_int_other2" class="choice-button-text">اٍضاءة داخلية</label>
                    @else
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other2" type="checkbox"
                         name="ext_int_other[]" value="ext_int_other_eio2" >
                        <label for="ext_int_other2" class="choice-button-text">اٍضاءة داخلية</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($intOthers, 'ext_int_other_eio3')))
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other3" type="checkbox" 
                        name="ext_int_other[]" value="ext_int_other_eio3" checked>
                        <label for="ext_int_other3" class="choice-button-text">نظام صوتي</label>
                    @else
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other3" type="checkbox" 
                        name="ext_int_other[]" value="ext_int_other_eio3" >
                        <label for="ext_int_other3" class="choice-button-text">نظام صوتي</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($intOthers, 'ext_int_other_eio4')))
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other4" type="checkbox" 
                        name="ext_int_other[]" value="ext_int_other_eio4" checked>
                        <label for="ext_int_other4" class="choice-button-text">اٍغلاق مركزي</label>
                    @else
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other4" type="checkbox" 
                        name="ext_int_other[]" value="ext_int_other_eio4" >
                        <label for="ext_int_other4" class="choice-button-text">اٍغلاق مركزي</label>
                    @endif
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($intOthers, 'ext_int_other_eio5')))
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other5" type="checkbox" 
                        name="ext_int_other[]" value="ext_int_other_eio5" checked>
                        <label for="ext_int_other5" class="choice-button-text">وسائد هوائية</label>
                    @else
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other5" type="checkbox" 
                        name="ext_int_other[]" value="ext_int_other_eio5" >
                        <label for="ext_int_other5" class="choice-button-text">وسائد هوائية</label>
                    @endif
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-6 ml-auto mr-auto" >
            <div  class="float-right">
            اضافات خارجية
            </div>
        </div>
    </div></br>

    <div class="row">
        <div class="col-lg-6 col-md-6 ml-auto mr-auto">

            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control" id="inputGroup"
                    placeholder="الاٍضاءة" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_ext_light_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($lights, 'ext_ext_light_znon')))
                        <input class="ext_ext_light_checkbox radiochoosen" id="ext_ext_light1" type="checkbox"
                         name="ext_ext_light[]" value="ext_ext_light_znon" checked>
                        <label for="ext_ext_light1" class="choice-button-text">زينون</label>
                    @else
                        <input class="ext_ext_light_checkbox radiochoosen" id="ext_ext_light1" type="checkbox"
                         name="ext_ext_light[]" value="ext_ext_light_znon" >
                        <label for="ext_ext_light1" class="choice-button-text">زينون</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($lights, 'ext_ext_light_ledl')))
                        <input class="ext_ext_light_checkbox radiochoosen" id="ext_ext_light2" type="checkbox" 
                        name="ext_ext_light[]" value="ext_ext_light_ledl" checked>
                        <label for="ext_ext_light2" class="choice-button-text">ليد</label>
                    @else
                        <input class="ext_ext_light_checkbox radiochoosen" id="ext_ext_light2" type="checkbox" 
                        name="ext_ext_light[]" value="ext_ext_light_ledl" >
                        <label for="ext_ext_light2" class="choice-button-text">ليد</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($lights, 'ext_ext_light_fogl')))
                        <input class="ext_ext_light_checkbox radiochoosen" id="ext_ext_light3" type="checkbox" 
                        name="ext_ext_light[]" value="ext_ext_light_fogl" checked>
                        <label for="ext_ext_light3" class="choice-button-text">ضباب</label>
                    @else
                        <input class="ext_ext_light_checkbox radiochoosen" id="ext_ext_light3" type="checkbox" 
                        name="ext_ext_light[]" value="ext_ext_light_fogl" >
                        <label for="ext_ext_light3" class="choice-button-text">ضباب</label>
                    @endif
                    </div> 
                </div>
            </div>


            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control"  id="inputGroup"
                    placeholder="المرايا" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_ext_mirrors_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($mirrors, 'ext_ext_mirrors_limi')))
                        <input class="ext_ext_mirrorsbox radiochoosen" id="ext_ext_mirrors1" type="checkbox"
                         name="ext_ext_mirrors[]" value="ext_ext_mirrors_limi" checked>
                        <label for="ext_ext_mirrors1" class="choice-button-text">اٍضاءة مرايا</label>
                    @else
                        <input class="ext_ext_mirrorsbox radiochoosen" id="ext_ext_mirrors1" type="checkbox"
                         name="ext_ext_mirrors[]" value="ext_ext_mirrors_limi" >
                        <label for="ext_ext_mirrors1" class="choice-button-text">اٍضاءة مرايا</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($mirrors, 'ext_ext_mirrors_clel')))
                        <input class="ext_ext_mirrorsbox radiochoosen" id="ext_ext_mirrors2" type="checkbox" 
                        name="ext_ext_mirrors[]" value="ext_ext_mirrors_clel" checked>
                        <label for="ext_ext_mirrors2" class="choice-button-text">اٍغلاق كهرباء</label>
                    @else
                        <input class="ext_ext_mirrorsbox radiochoosen" id="ext_ext_mirrors2" type="checkbox" 
                        name="ext_ext_mirrors[]" value="ext_ext_mirrors_clel" >
                        <label for="ext_ext_mirrors2" class="choice-button-text">اٍغلاق كهرباء</label>
                    @endif
                    </div> 
                </div>
            </div>

            <div class="form-group">
                <input class="form-control" 
                placeholder="الجنط" type="text" 
                disabled >
                
            </div> 
            @if(isset($rims[0]) && $rims[0]->equipment_value !== null)
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_ext_rims1" type="radio" 
                        name="ext_ext_rims" value="ext_ext_rims_norl" 
                        {{ $rims[0]->equipment_value == 'ext_ext_rims_norl' ? 'checked' : '' }}>
                        <label for="ext_ext_rims1" class="choice-button-text">عادي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_ext_rims2" type="radio"
                         name="ext_ext_rims" value="ext_ext_rims_magn" 
                         {{ $rims[0]->equipment_value == 'ext_ext_rims_magn' ? 'checked' : '' }}>
                        <label for="ext_ext_rims2" class="choice-button-text">مغنيسيوم</label>
                    </div> 
                </div>
            </div>
            @else 
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_ext_rims1" type="radio" 
                        name="ext_ext_rims" value="ext_ext_rims_norl" >
                        <label for="ext_ext_rims1" class="choice-button-text">عادي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_ext_rims2" type="radio"
                         name="ext_ext_rims" value="ext_ext_rims_magn" >
                        <label for="ext_ext_rims2" class="choice-button-text">مغنيسيوم</label>
                    </div> 
                </div>
            </div>
            @endif
            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control" id="inputGroup"
                    placeholder="الكاميرات" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_ext_cameras_check" >
                </div> 
            </div> 
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($cameras, 'ext_ext_cameras_frnt')))
                        <input class="ext_ext_cameras_checkbox radiochoosen" id="ext_ext_cameras1" type="checkbox" 
                        name="ext_ext_cameras[]" value="ext_ext_cameras_frnt" checked>
                        <label for="ext_ext_cameras1" class="choice-button-text">أمامية</label>
                    @else
                        <input class="ext_ext_cameras_checkbox radiochoosen" id="ext_ext_cameras1" type="checkbox" 
                        name="ext_ext_cameras[]" value="ext_ext_cameras_frnt" >
                        <label for="ext_ext_cameras1" class="choice-button-text">أمامية</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($cameras, 'ext_ext_cameras_back')))
                        <input class="ext_ext_cameras_checkbox radiochoosen" id="ext_ext_cameras2" type="checkbox" 
                        name="ext_ext_cameras[]" value="ext_ext_cameras_back" checked>
                        <label for="ext_ext_cameras2" class="choice-button-text">خلفية</label>
                    @else
                        <input class="ext_ext_cameras_checkbox radiochoosen" id="ext_ext_cameras2" type="checkbox" 
                        name="ext_ext_cameras[]" value="ext_ext_cameras_back" >
                        <label for="ext_ext_cameras2" class="choice-button-text">خلفية</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($cameras, 'ext_ext_cameras_tsdc')))
                        <input class="ext_ext_cameras_checkbox radiochoosen" id="ext_ext_cameras3" type="checkbox" 
                        name="ext_ext_cameras[]" value="ext_ext_cameras_tsdc" checked>
                        <label for="ext_ext_cameras3" class="choice-button-text">360 درجة</label>
                    @else
                        <input class="ext_ext_cameras_checkbox radiochoosen" id="ext_ext_cameras3" type="checkbox" 
                        name="ext_ext_cameras[]" value="ext_ext_cameras_tsdc" >
                        <label for="ext_ext_cameras3" class="choice-button-text">360 درجة</label>
                    @endif
                    </div> 
                </div>
            </div>


            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control" id="inputGroup"
                    placeholder="اضافات أخرى" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_gen_other_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($genOther, 'ext_gen_other_fing')))
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other1" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_fing" checked>
                        <label for="ext_gen_other1" class="choice-button-text">بصمة أبواب</label>
                    @else
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other1" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_fing" >
                        <label for="ext_gen_other1" class="choice-button-text">بصمة أبواب</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($genOther, 'ext_gen_other_remo')))
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other2" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_remo" checked>
                        <label for="ext_gen_other2" class="choice-button-text">صدّام أمامي</label>
                    @else
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other2" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_remo" >
                        <label for="ext_gen_other2" class="choice-button-text">صدّام أمامي</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($genOther, 'ext_gen_other_elec')))
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other3" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_elec" checked>
                        <label for="ext_gen_other3" class="choice-button-text">قيادة ذاتية</label>
                    @else
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other3" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_elec" >
                        <label for="ext_gen_other3" class="choice-button-text">قيادة ذاتية</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($genOther, 'ext_gen_other_ecos')))
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other4" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_ecos" checked>
                        <label for="ext_gen_other4" class="choice-button-text">نظام ECO</label>
                    @else
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other4" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_ecos" >
                        <label for="ext_gen_other4" class="choice-button-text">نظام ECO</label>
                    @endif
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($genOther, 'ext_gen_other_abss')))
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other5" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_abss" checked>
                        <label for="ext_gen_other5" class="choice-button-text">نظام ABS</label>
                    @else
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other5" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_abss" >
                        <label for="ext_gen_other5" class="choice-button-text">نظام ABS</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($genOther, 'ext_gen_other_navi')))
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other6" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_navi" checked>
                        <label for="ext_gen_other6" class="choice-button-text">نظام خوارط</label>
                    @else
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other6" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_navi" >
                        <label for="ext_gen_other6" class="choice-button-text">نظام خوارط</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($genOther, 'ext_gen_other_door')))
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other7" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_door" checked>
                        <label for="ext_gen_other7" class="choice-button-text">أبواب شفط</label>
                    @else
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other7" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_door" >
                        <label for="ext_gen_other7" class="choice-button-text">أبواب شفط</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($genOther, 'ext_gen_other_main')))
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other8" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_main" checked>
                        <label for="ext_gen_other8" class="choice-button-text">دفتر صيانة</label>
                    @else
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other8" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_main" >
                        <label for="ext_gen_other8" class="choice-button-text">دفتر صيانة</label>
                    @endif
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($genOther, 'ext_gen_other_safe')))
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other9" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_safe" checked>
                        <label for="ext_gen_other9" class="choice-button-text">رادار قياس مسافة الأمان</label>
                    @else
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other9" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_safe" >
                        <label for="ext_gen_other9" class="choice-button-text">رادار قياس مسافة الأمان</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($genOther, 'ext_gen_other_serv')))
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other10" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_serv" checked>
                        <label for="ext_gen_other10" class="choice-button-text">دفتر خدمات</label>
                    @else
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other10" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_serv" >
                        <label for="ext_gen_other10" class="choice-button-text">دفتر خدمات</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($genOther, 'ext_gen_other_crui')))
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other11" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_crui" checked>
                        <label for="ext_gen_other11" class="choice-button-text">محدد سرعة</label>
                    @else
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other11" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_crui" >
                        <label for="ext_gen_other11" class="choice-button-text">محدد سرعة</label>
                    @endif
                    </div> 
                </div>
            </div>
        </div>
        
        <div class="col-lg-6 col-md-6 ml-auto mr-auto">

            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control" id="inputGroup"
                    placeholder="الحساسات" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_ext_sensors_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($sensors, 'ext_ext_sensors_frot')))
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors1" type="checkbox"
                         name="ext_ext_sensors[]" value="ext_ext_sensors_frot" checked>
                        <label for="ext_ext_sensors1" class="choice-button-text">امامي</label>
                    @else
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors1" type="checkbox"
                         name="ext_ext_sensors[]" value="ext_ext_sensors_frot" >
                        <label for="ext_ext_sensors1" class="choice-button-text">امامي</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($sensors, 'ext_ext_sensors_bake')))
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors2" type="checkbox"
                         name="ext_ext_sensors[]" value="ext_ext_sensors_bake" checked>
                        <label for="ext_ext_sensors2" class="choice-button-text">خلفي</label>
                    @else
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors2" type="checkbox"
                         name="ext_ext_sensors[]" value="ext_ext_sensors_bake" >
                        <label for="ext_ext_sensors2" class="choice-button-text">خلفي</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($sensors, 'ext_ext_sensors_tsds')))
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors3" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_tsds" checked>
                        <label for="ext_ext_sensors3" class="choice-button-text">360 درجة</label>
                    @else
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors3" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_tsds" >
                        <label for="ext_ext_sensors3" class="choice-button-text">360 درجة</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($sensors, 'ext_ext_sensors_rain')))
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors4" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_rain" checked>
                        <label for="ext_ext_sensors4" class="choice-button-text">مطر</label>
                    @else
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors4" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_rain" >
                        <label for="ext_ext_sensors4" class="choice-button-text">مطر</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($sensors, 'ext_ext_sensors_ligh')))
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors5" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_ligh" checked>
                        <label for="ext_ext_sensors5" class="choice-button-text">اٍضاءة</label>
                    @else
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors5" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_ligh" >
                        <label for="ext_ext_sensors5" class="choice-button-text">اٍضاءة</label>
                    @endif
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($sensors, 'ext_ext_sensors_colb')))
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors6" type="checkbox"
                         name="ext_ext_sensors[]" value="ext_ext_sensors_colb" checked>
                        <label for="ext_ext_sensors6" class="choice-button-text">مانع اٍصطدام</label>
                    @else
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors6" type="checkbox"
                         name="ext_ext_sensors[]" value="ext_ext_sensors_colb" >
                        <label for="ext_ext_sensors6" class="choice-button-text">مانع اٍصطدام</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($sensors, 'ext_ext_sensors_blin')))
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors7" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_blin" checked>
                        <label for="ext_ext_sensors7" class="choice-button-text">النقطة العمياء</label>
                    @else
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors7" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_blin" >
                        <label for="ext_ext_sensors7" class="choice-button-text">النقطة العمياء</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($sensors, 'ext_ext_sensors_sele')))
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors8" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_sele" checked>
                        <label for="ext_ext_sensors8" class="choice-button-text">تحديد مسار</label>
                    @else
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors8" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_sele" >
                        <label for="ext_ext_sensors8" class="choice-button-text">تحديد مسار</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($sensors, 'ext_ext_sensors_sign')))
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors9" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_sign" checked>
                        <label for="ext_ext_sensors9" class="choice-button-text">قارئ اٍشارات</label>
                    @else
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors9" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_sign" >
                        <label for="ext_ext_sensors9" class="choice-button-text">قارئ اٍشارات</label>
                    @endif
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($sensors, 'ext_ext_sensors_self')))
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors10" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_self" checked>
                        <label for="ext_ext_sensors10" class="choice-button-text">اٍصطفاف ذاتي</label>
                    @else
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors10" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_self" >
                        <label for="ext_ext_sensors10" class="choice-button-text">اٍصطفاف ذاتي</label>
                    @endif
                    </div> 
                </div>
            </div>


            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control"  id="inputGroup"
                    placeholder="اٍضافات خارجية أخرى" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_ext_other_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($extOther, 'ext_ext_other_hudp')))
                        <input class="ext_ext_other_checkbox radiochoosen" id="ext_ext_other1" type="checkbox"
                         name="ext_ext_other[]" value="ext_ext_other_hudp" checked>
                        <label for="ext_ext_other1" class="choice-button-text">Head Up Display</label>
                    @else
                        <input class="ext_ext_other_checkbox radiochoosen" id="ext_ext_other1" type="checkbox"
                         name="ext_ext_other[]" value="ext_ext_other_hudp" >
                        <label for="ext_ext_other1" class="choice-button-text">Head Up Display</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($extOther, 'ext_ext_other_sela')))
                        <input class="ext_ext_other_checkbox radiochoosen" id="ext_ext_other2" type="checkbox" 
                        name="ext_ext_other[]" value="ext_ext_other_sela" checked>
                        <label for="ext_ext_other2" class="choice-button-text">اٍصطفاف ذاتي</label>
                    @else
                        <input class="ext_ext_other_checkbox radiochoosen" id="ext_ext_other2" type="checkbox" 
                        name="ext_ext_other[]" value="ext_ext_other_sela" >
                        <label for="ext_ext_other2" class="choice-button-text">اٍصطفاف ذاتي</label>
                    @endif
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($extOther, 'ext_ext_other_bump')))
                        <input class="ext_ext_other_checkbox radiochoosen" id="ext_ext_other3" type="checkbox" 
                        name="ext_ext_other[]" value="ext_ext_other_bump" checked>
                        <label for="ext_ext_other3" class="choice-button-text">صدّام أمامي</label>
                    @else
                        <input class="ext_ext_other_checkbox radiochoosen" id="ext_ext_other3" type="checkbox" 
                        name="ext_ext_other[]" value="ext_ext_other_bump" >
                        <label for="ext_ext_other3" class="choice-button-text">صدّام أمامي</label>
                    @endif
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                    @if (strlen(strpos($extOther, 'ext_ext_other_hydr')))
                        <input class="ext_ext_other_checkbox radiochoosen" id="ext_ext_other4" type="checkbox" 
                        name="ext_ext_other[]" value="ext_ext_other_hydr" checked>
                        <label for="ext_ext_other4" class="choice-button-text">رفع وخفض هيدروليكي</label>
                    @else
                        <input class="ext_ext_other_checkbox radiochoosen" id="ext_ext_other4" type="checkbox" 
                        name="ext_ext_other[]" value="ext_ext_other_hydr">
                        <label for="ext_ext_other4" class="choice-button-text">رفع وخفض هيدروليكي</label>
                    @endif
                    </div> 
                </div>
            </div>
        </div>
    </div>
  
    </div>
</div>