@extends('layouts.apps')

@section('content')
<center>
    <div class="row">
        <div class="col-lg-5 col-md-5 ml-auto mr-auto">
            @foreach($sellers as $seller)
                <div class="card p-1 mb-1 bg-white rounded">
                    <div class="card-body ">
                        <div class="d-flex flex-row" style="padding: 0.4rem">
                            <div style="width:100%; height: 150px; background-color:white" class="col-lg-6 col-md-6 col-sm-6">
                                @if(!empty(\App\Model\Media::where('fileable_id', '=', $seller->id)->select('file_name')->first()) > 0)
                                    <img class="img-responsive" style="width:100%; height: 150px;object-fit: contain"
                                        src="{{'/storage/sellers/'. (\App\Model\Media::where('fileable_id', '=', $seller->id)->select('file_name')->first())->file_name}}">
                                @endif
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="text-center row">
                                    <h6 style="color:#595959; font-weight: bold;">
                                        {{ $seller->seller_name }} 
                                    </h6>
                                </div>
                                <div class="row">
                                    <h6 style="color: gray;font-size:13px">
                                        {{ $seller->seller_mobile }} 
                                    </h6>
                                </div></br>
                                <div class="row">
                                    <h6 style="color:#595959; font-weight: bold; font-size:13px">
                                        {{ $seller->city }} , 
                                        <span style="color:#595959; font-weight: bold;font-size:12px">
                                            {{ $seller->seller_address }}
                                        </span>
                                    </h6>
                                </div>
                                <div class="row">
                                    <h6 style="color: gray; font-size:14px">
                                        {{ $seller->seller_email }} 
                                    </h6>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <center>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <form action="tel:{{ $seller->seller_mobile }} ">
                                            <button class="btn btn-primary btn-block" style="cursor:pointer;background-color:#2e5cb8;color:white;padding:0.1rem" 
                                                type="submit"><i class="fa fa-phone"></i> اٍتصل</button>
                                        </form>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
                    
            </div>
            @endforeach
    </div></br>
    
</center>

    <div class="row" > 
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class="row" > 
            @foreach($vehicles as $vehicle)
            <div class="col-lg-6 col-md-6 col-sm-6 " >
                <div class="card p-1 mb-1 bg-white rounded" style="padding: 0.1rem">
                <div class="card-body ">
                    
                    <div id="carousel-example-all{{$vehicle->id}}" class="text-center  carousel slide" data-ride="carousel">
                        <center>
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                @foreach(\App\Model\Media::where('fileable_id', '=', $vehicle->id)->get() as $key => $data)
                                    <li data-target="#carousel-example-all{{$vehicle->id}}" data-slide-to="{{ $loop->index }}" 
                                        class="{{ $loop->first ? 'active' : '' }}"></li>
                                @endforeach
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                            @foreach(\App\Model\Media::where('fileable_id', '=', $vehicle->id)->get() as $key => $data)
                                    <div style="width:100%; height: 160px; background-color:white" class="carousel-item item {{ $loop->first ? ' active' : '' }}" >
                                        <a href="{{route('vec-seller.all', $vehicle->id)}}">
                                            <img src="{{'/storage/vehicles/'.$data->file_name}}" width="100%" 
                                            height="160px" style="object-fit: contain">
                                        </a>
                                    </div>
                                @endforeach
                            </div>

                            <!-- Controls -->
                            <a class="right carousel-control" href="#carousel-example-all{{$vehicle->id}}" role="button" data-slide="prev">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="left carousel-control" href="#carousel-example-all{{$vehicle->id}}" role="button" data-slide="next">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                <span class="sr-only">Next</span>
                            </a>
                        </center>
                    </div>
                    <a href="{{route('vec-seller.information', $vehicle->id)}}" class="text-muted">
                        <div class="row">
                            <div class="text-center col-lg-6 col-md-6 col-sm-6 ml-auto mr-auto" style="padding: 0.4rem">
                                <h6 style="color:#595959; font-weight: bold; font-size:13px">
                                {{ $vehicle->name }}  {{ $vehicle->model_name }}
                                </h6>
                            </div>   
                        </div> 
                        <div class="row">  
                            <div class="text-center col-lg-6 col-md-6 col-sm-6 ml-auto mr-auto" >
                            @if ($vehicle->vehicle_status == 'new')
                                جديد  
                            @else
                                مستعمل
                            @endif
                            </div>  
                        </div>
                        <div class="d-flex flex-row">
                            <div class="p-2 ml-auto mr-auto" >
                                <i class="fa fa-circle 0" style="color: #{{substr($vehicle->body_color, 1)}}">
                                <span style="color:black">اللون الخارجي</span>
                                </i>

                            </div> 
                            <div class="p-2 ml-auto mr-auto">
                                <i class="fa fa-circle 0" style="color: #{{substr($vehicle->interior_color, 1)}}">
                                    <span style="color:black">اللون الداخلي</span>
                                </i>
                            </div> 
                        </div> 
                        <hr>

                        <div class="text-center d-flex flex-row" style="margin: 1px">
                            <div class="col-lg-3 col-md-3 col-sm-3 ml-auto mr-auto" >
                                <div class="">
                                    <img src="{{url('/images/icons/price.png')}}" width=20>
                                </div>
                                <div class="">
                                    <label> السعر</label>
                                </div>
                                <div class="">
                                    <label style="font-size:12px">{{ $vehicle->first_payment }} </label>
                                </div>
                            </div> 
                            <div class="col-lg-3 col-md-3 col-sm-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/power.png')}}" width=30>
                                </div>
                                <div class="">
                                    <label >القوة</label>
                                </div>
                                <div class="">
                                    <label  style="font-size:12px">{{ $vehicle->power }} </label>
                                </div>
                            </div> 
                            <div class="col-lg-3 col-md-3 col-sm-3 ml-auto mr-auto">
                                <div class="">
                                    @if ($vehicle->gear == 'aut')
                                        <img src="{{url('/images/icons/automaticgear.png')}}" width=20>
                                    @else @if ($vehicle->gear == 'man')
                                        <img src="{{url('/images/icons/normalgear.png')}}" width=20>   
                                    @else  <img src="{{url('/images/icons/semigear.png')}}" width=20>
                                    @endif
                                    @endif
                                
                                </div>
                                <div class="">
                                    <label> ناقل الحركة</label>
                                </div>
                                <div class="">
                                    <label  style="font-size:12px">
                                        @if ($vehicle->gear == 'aut')
                                        أوتماتيك  
                                        @else @if ($vehicle->gear == 'man')
                                        يدوي    
                                        @else نصف أوتماتيك
                                        @endif
                                        @endif
                                    </label>
                                </div>
                            </div> 
                        </div> 
                        <hr>

                        <div class="text-center d-flex flex-row">
                            <div class="col-lg-3 col-md-3 col-sm-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/fuel.jpeg')}}" width=20>
                                </div>
                                <div class="">
                                    <label> الوقود</label>
                                </div>
                                <div class="">
                                    <label  style="font-size:12px">
                                        @if ($vehicle->fuel == 'bin')
                                        بنزين  
                                        @else @if ($vehicle->fuel == 'des')
                                        ديزل  
                                        @else @if ($vehicle->fuel == 'binelec')
                                        بنزين / كهرباء  
                                        @else @if ($vehicle->fuel == 'deselec')
                                        ديزل / كهرباء 
                                        @else @if ($vehicle->fuel == 'gaz')
                                        غاز    
                                        @else كهرباء
                                        @endif
                                        @endif
                                        @endif
                                        @endif
                                        @endif
                                    </label>
                                </div>
                            </div> 
                            <div class="col-lg-3 col-md-3 col-sm-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/8757.png')}}" width=20>
                                </div>
                                <div class="">
                                    <label >سنة الانتاج</label>
                                </div>
                                <div class="">
                                    <label  style="font-size:12px">{{ $vehicle->year_of_product }} </label>
                                </div>
                            </div> 
                            <div class="col-lg-3 col-md-3 col-sm-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/seat.jpeg')}}" width=20>
                                </div>
                                <div class="">
                                    <label> عدد المقاعد</label>
                                </div>
                                <div class="">
                                    <label  style="font-size:12px">
                                        @if ($vehicle->num_of_seats == 's1')
                                        2+1  
                                        @else @if ($vehicle->num_of_seats == 's2')
                                        3+1  
                                        @else @if ($vehicle->num_of_seats == 's3')
                                        4+1  
                                        @else @if ($vehicle->num_of_seats == 's4')
                                        5+1    
                                        @else 6+1
                                        @endif
                                        @endif
                                        @endif
                                        @endif
                                    <label>
                                </div>
                            </div> 
                        </div> 
                        <!--
                        <div class="text-center row">
                            <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/hp.png')}}" width=60>
                                </div>
                                <div class="">
                                    <label> عدد الأحصنة</label>
                                </div>
                                <div class="">
                                    <label style="font-size:13px">{{ $vehicle->hp }} </label>
                                </div>
                            </div> 
                            <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/Speedometer-512.png')}}" width=33>
                                </div>
                                <div class="">
                                    <label >المسافة المقطوعة</label>
                                </div>
                                <div class="">
                                    <label style="font-size:13px">{{ $vehicle->mileage }} </label>
                                </div>
                            </div> 
                            <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/key.png')}}" width=30>
                                </div>
                                <div class="">
                                    <label > عدد المفاتيح </label>
                                </div>
                                <div class="">
                                    <label style="font-size:13px">
                                        @if ($vehicle->num_of_keys == 'kes1')
                                        1 
                                        @else @if ($vehicle->gear == 'kes2')
                                        2 
                                        @else 
                                        3   
                                        @endif
                                        @endif
                                    <label>
                                </div>
                            </div> 
                        </div> -->

                    </div>
                </div>
            </a>
        </div>
        
        @endforeach
        </div>
      
           
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>
@endsection