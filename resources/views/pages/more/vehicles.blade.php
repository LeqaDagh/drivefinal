@extends('layouts.apps')

@section('content')
<div class="row"> 
    <div class="col-lg-1 col-md-1 col-sm-1">   
    </div>
   
    <div class="col-lg-10 col-md-10 col-sm-10"> 
        <div class="row">
        @foreach($vehicles as $vehicle)
            <div class="col-lg-12 col-md-10 col-sm-10">
                <div class="card " style="padding: 0.8rem">
                <a href="{{route('vec-seller.information', $vehicle->id)}}" class="text-muted">
                    <div class="card-body " style="padding: 0.4rem">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div style="padding:0.8rem"class="row">
                                    <div class="text-center col-lg-6 col-md-6 ml-auto mr-auto">
                                        <h5 style="color:#595959; font-weight: bold;">
                                        {{ $vehicle->name }} {{ $vehicle->model_name }}</h5>
                                    </div>   
                                </div>
                                <div id="carousel-vehicles{{$vehicle->id}}" class="text-center carousel slide" data-ride="carousel">
                                    <center>
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            @foreach(\App\Model\Media::where('fileable_id', '=', $vehicle->id)->get() as $key => $data)
                                                <li data-target="#carousel-vehicles{{$vehicle->id}}" data-slide-to="{{ $loop->index }}" 
                                                    class="{{ $loop->first ? 'active' : '' }}"></li>
                                            @endforeach
                                        </ol>

                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            @foreach(\App\Model\Media::where('fileable_id', '=', $vehicle->id)->get() as $key => $data)
                                                <div style="width:120%; height: 210px; background-color:white"  class="carousel-item item {{ $loop->first ? ' active' : '' }}" >
                                                    <a href="{{route('vec-seller.all', $vehicle->id)}}">
                                                        <img src="{{'/storage/vehicles/'.$data->file_name}}" width="120%" 
                                                        height="210px" style="object-fit: contain ">
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>

                                        <!-- Controls -->
                                        <a class="right carousel-control" href="#carousel-vehicles{{$vehicle->id}}" role="button" data-slide="prev">
                                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="left carousel-control" href="#carousel-vehicles{{$vehicle->id}}" role="button" data-slide="next">
                                            <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </center> 
                                </div>
                                
                                <div class="row" > 
                                    <div class="text-center col-lg-6 col-md-6 ml-auto mr-auto">
                                    @if ($vehicle->vehicle_status == 'new')
                                        جديد  
                                    @else
                                        مستعمل
                                    @endif
                                    </div>  
                                </div>
                                <div class="text-center d-flex flex-row">
                                    <div class=" col-lg-6 col-md-6 ml-auto mr-auto">
                                        <i class="fa fa-circle " style="color: #{{substr($vehicle->body_color, 1)}}"></i>
                                        <span style="font-size:14px;">اللون الخارجي </span> 
                                    </div> 
                                    <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                        <i class="fa fa-circle " style="color: #{{substr($vehicle->interior_color, 1)}}"></i>
                                        <span style="font-size:14px;">اللون الداخلي </span> 
                                    </div> 
                                </div> 
                            </div>
                            <div style="padding:0.9rem"class="col-lg-6 col-md-6 col-sm-6">
                                <div class="text-center d-flex flex-row">
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="form-group">
                                            <img src="{{url('/images/icons/price.png')}}" width=30>
                                        </div>
                                        <div class="">
                                            <label style="font-size:15px"> السعر</label>
                                        </div>
                                        <div class="">
                                            @if($vehicle->price_type == 'price_type_types_fp1')
                                            <label style="font-size:13px">{{ $vehicle->first_payment }} </label>
                                            @else @if($vehicle->price_type == 'price_type_types_fp2')
                                            <label style="font-size:13px">{{ $vehicle->first_payment }} </label>
                                            @endif
                                            @endif
                                        </div>
                                    </div> 
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="form-group">
                                            <img src="{{url('/images/icons/power.png')}}" width=30>
                                        </div>
                                        <div class="">
                                            <label style="font-size:15px">القوة</label>
                                        </div>
                                        <div class="">
                                            <label style="font-size:13px">{{ $vehicle->power }} </label>
                                        </div>
                                    </div> 
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="form-group">
                                            @if ($vehicle->gear == 'aut')
                                                <img src="{{url('/images/icons/automaticgear.png')}}" width=30>
                                            @else @if ($vehicle->gear == 'man')
                                                <img src="{{url('/images/icons/normalgear.png')}}" width=30>   
                                            @else  <img src="{{url('/images/icons/semigear.png')}}" width=30>
                                            @endif
                                            @endif
                                        
                                        </div>
                                        <div class="">
                                            <label style="font-size:15px"> ناقل الحركة</label>
                                        </div>
                                        <div class="">
                                            <label style="font-size:13px">
                                                @if ($vehicle->gear == 'aut')
                                                أوتماتيك  
                                                @else @if ($vehicle->gear == 'man')
                                                يدوي    
                                                @else نصف أوتماتيك
                                                @endif
                                                @endif
                                            </label>
                                        </div>
                                    </div> 
                                </div><hr>
                                <div class="text-center d-flex flex-row">
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="form-group">
                                            <img src="{{url('/images/icons/fuel.jpeg')}}" width=30>
                                        </div>
                                        <div class="">
                                            <label style="font-size:15px"> الوقود</label>
                                        </div>
                                        <div class="">
                                            <label style="font-size:13px">
                                                @if ($vehicle->fuel == 'bin')
                                                بنزين  
                                                @else @if ($vehicle->fuel == 'des')
                                                ديزل  
                                                @else @if ($vehicle->fuel == 'binelec')
                                                بنزين / كهرباء  
                                                @else @if ($vehicle->fuel == 'deselec')
                                                ديزل / كهرباء 
                                                @else @if ($vehicle->fuel == 'gaz')
                                                غاز    
                                                @else كهرباء
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                            </label>
                                        </div>
                                    </div> 
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="form-group">
                                            <img src="{{url('/images/icons/8757.png')}}" width=30>
                                        </div>
                                        <div class="">
                                            <label style="font-size:15px">سنة الانتاج</label>
                                        </div>
                                        <div class="">
                                            <label style="font-size:13px">{{ $vehicle->year_of_product }} </label>
                                        </div>
                                    </div> 
                                    <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                        <div class="form-group">
                                            <img src="{{url('/images/icons/seat.jpeg')}}" width=30>
                                        </div>
                                        <div class="">
                                            <label style="font-size:15px"> عدد المقاعد</label>
                                        </div>
                                        <div class="">
                                            <label style="font-size:13px">
                                                @if ($vehicle->num_of_seats == 's1')
                                                2+1  
                                                @else @if ($vehicle->num_of_seats == 's2')
                                                3+1  
                                                @else @if ($vehicle->num_of_seats == 's3')
                                                4+1  
                                                @else @if ($vehicle->num_of_seats == 's4')
                                                5+1    
                                                @else 6+1
                                                @endif
                                                @endif
                                                @endif
                                                @endif
                                            <label>
                                        </div>
                                    </div> 
                                </div> 
                                <hr>
                                <div style="padding-top:0.7rem"class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="text-center d-flex flex-row">
                                        <div class="col-lg-2 col-md-2 col-sm-2 ml-auto mr-auto">
                                            <a  data-id="{{$vehicle->id}}" class="shareInformationLink"
                                                style="cursor:pointer;">
                                                <i  class=" material-icons" style="color:blue">share</i>
                                            </a>
                                            <p id="urlCopied" hidden></p>
                                        </div> 
                                       <!-- <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                                            <a data-id="{{$vehicle->id}}" id="compareVehicles" class="compareVehicles"
                                                style="cursor:pointer;">
                                                <i id="compareVehiclesIcon" class="compareVehiclesIcon material-icons" style="color:#004d4d">compare</i>
                                            </a>
                                        </div> -->
                                        <div class="col-lg-2 col-md-2 col-sm-2 ml-auto mr-auto">
                                            <a id="editVehicleById" 
                                            href="{{route('vec-seller.edit', $vehicle->id)}}" >
                                                <i class="material-icons" style="color:gray">create</i>
                                            </a>
                                        </div> 
                                        
                                        <div class="col-lg-2 col-md-2 col-sm-2 ml-auto mr-auto">
                                            <a id="deleteVehicleById" data-toggle="modal" 
                                                data-target="#deleteModal" style="cursor:pointer;">
                                                <i class="material-icons" style="color:#cc0000">delete</i>
                                            </a>
                                        </div> 
                                        <div class="col-lg-2 col-md-2 col-sm-2  ml-auto mr-auto">
                                            <a id="editVehicleImagesById" href="{{route('vec-seller.images', $vehicle->id)}}">
                                                <i class="material-icons" style="color:#660066">add_photo_alternate</i>
                                            </a>
                                        </div> 

                                        <div class="col-lg-2 col-md-2 col-sm-2 ml-auto mr-auto">
                                            <a id="" data-toggle="modal" 
                                                data-target="#infoModal{{$vehicle->id}}" style="cursor:pointer;">
                                                <i class="material-icons" style="color:gray">info</i>
                                            </a>
                                        </div> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </div>
                <div style="padding: 0.3rem"></div>
            </div>
            <div class="modal modal-danger fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="Delete" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="text-center modal-title" id="exampleModalLabel">حذف المركبة </h5>
                        </div>
                        <div class="modal-body">
                            <input id="id" name="id")>
                            <h6 class="text-center">تأكيد حذف المركية؟</h6>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <a type="button" class="btn btn-sm btn-info" data-dismiss="modal">الغاء</a>
                                <a href="{{ route('vehicle.delete', $vehicle->id) }}" class="btn btn-sm btn-danger">حذف</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal modal-secondary fade" id="infoModal{{$vehicle->id}}" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="text-center modal-header" 
                            style="background-color:#cc0000; padding:0.7rem">
                            <h6  style="color:white">
                             {{ $vehicle->name }} , {{ $vehicle->model_name }}
                            </h6>
                        </div>
                        <div class="modal-body" style="padding:0.9rem">
                            <div class="element{{$vehicle->id}}" id="select_txt">
                                
                                <span class="boxspan{{$vehicle->id}}" data-id="{{ $vehicle->name }}"style="font-size:13px;">
                                {{ $vehicle->name }} , {{ $vehicle->model_name }}   {{ $vehicle->year_of_product }} 
                                </span></br>
                                
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">  القوة </span>
                                <span class="boxspan"style="font-size:13px"> {{ $vehicle->power }} ,  </span>
                                <span class="boxspan" style="font-size:13px;color:#595959; font-weight: bold;">عدد الأحصنة </span>
                                <span class="boxspan"style="font-size:13px"> {{ $vehicle->hp }} </span></br>
                                
                                
                                <span class="boxspan"style="font-size:14px;color:#595959; font-weight: bold;">  المواصفات :  </span>
                                </br>
                                
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">  ناقل الحركة  :
                                </span>
                                <span class="boxspan"style="font-size:13px"> 
                                    @if ($vehicle->fuel == 'bin')
                                    بنزين  
                                    @else @if ($vehicle->fuel == 'des')
                                    ديزل  
                                    @else @if ($vehicle->fuel == 'binelec')
                                    بنزين / كهرباء  
                                    @else @if ($vehicle->fuel == 'deselec')
                                    ديزل / كهرباء 
                                    @else @if ($vehicle->fuel == 'gaz')
                                    غاز    
                                    @else كهرباء
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                </span></br>
                                
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">    الوقود :</span>
                                <span class="boxspan"style="font-size:13px"> 
                                    @if ($vehicle->gear == 'aut')
                                    أوتماتيك  
                                    @else @if ($vehicle->gear == 'man')
                                    يدوي    
                                    @else نصف أوتماتيك
                                    @endif
                                    @endif
                                </span></br>
                                
                                
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">    عدد المقاعد :</span>
                                <span class="boxspan"style="font-size:13px"> 
                                    @if ($vehicle->num_of_seats == 's1')
                                    2+1  
                                    @else @if ($vehicle->num_of_seats == 's2')
                                    3+1  
                                    @else @if ($vehicle->num_of_seats == 's3')
                                    4+1  
                                    @else @if ($vehicle->num_of_seats == 's4')
                                    5+1    
                                    @else 6+1
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                </span></br>
                                
                                
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">    اللون الخارجي :</span>
                                <span class="boxspan"style="font-size:13px"> 
                                    @if ($vehicle->body_color == 'c000000')
                                    أسود  
                                    @else @if ($vehicle->body_color == 'c0bde16')
                                    أخضر    
                                    @else @if ($vehicle->body_color == 'c120a0a')
                                    أسود مطفي
                                    @else @if ($vehicle->body_color == 'c0e103d')
                                    كحلي
                                    @else @if ($vehicle->body_color == 'c15b367')
                                    أزرق مخضر
                                    @else @if ($vehicle->body_color == 'c1e14de')
                                    نيلي
                                    @else @if ($vehicle->body_color == 'c1e9dba')
                                    أزرق فاتح
                                    @else @if ($vehicle->body_color == 'c215769')
                                    أزرق غامق
                                    @else @if ($vehicle->body_color == 'c24070d')
                                    بنفسجي
                                    @else @if ($vehicle->body_color == 'c2a4221')
                                    زيتي
                                    @else @if ($vehicle->body_color == 'c362517')
                                    بني غامق
                                    @else @if ($vehicle->body_color == 'c386352')
                                    أخضر غامق
                                    @else @if ($vehicle->body_color == 'c544c4a')
                                    رمادي
                                    @else @if ($vehicle->body_color == 'c6b6560')
                                    سكني
                                    @else @if ($vehicle->body_color == 'c700d0d')
                                    أحمر قاني
                                    @else @if ($vehicle->body_color == 'c70431e')
                                    بني
                                    @else @if ($vehicle->body_color == 'c8fe813')
                                    فسفوري
                                    @else @if ($vehicle->body_color == 'cba0909')
                                    خمري
                                    @else @if ($vehicle->body_color == 'cc48f60')
                                    بني برونزي
                                    @else @if ($vehicle->body_color == 'cc7c1bd')
                                    فضي
                                    @else @if ($vehicle->body_color == 'cde6b18')
                                    برتقالي
                                    @else @if ($vehicle->body_color == 'cf0e9e9')
                                    أبيض كريمي
                                    @else @if ($vehicle->body_color == 'cfcba03')
                                    أصفر
                                    @else @if ($vehicle->body_color == 'cff0000')
                                    أحمر
                                    @else @if ($vehicle->body_color == 'cffadbb')
                                    زهري
                                    @else أبيض
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                </span></br>
                            
                            
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">    اللون الداخلي :</span>
                                <span class="boxspan"style="font-size:13px"> 
                                    @if ($vehicle->interior_color == 'c000000')
                                    أسود
                                    @else @if ($vehicle->interior_color == 'c5e3b18')
                                    بني
                                    @else @if ($vehicle->interior_color == 'c751010')
                                    خمري
                                    @else @if ($vehicle->interior_color == 'c755196')
                                    بنفسجي
                                    @else @if ($vehicle->interior_color == 'cb5b3b3')
                                    سكني
                                    @else @if ($vehicle->interior_color == 'cc40a0a')
                                    أحمر
                                    @else @if ($vehicle->interior_color == 'cc9902e')
                                    عسلي
                                    @else @if ($vehicle->interior_color == 'cfbffd9')
                                    بيج
                                    @else أبيض
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                </span>
                                </br>
                            
                            
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">  طريقة الدفعات :  </span>
                                @foreach(\App\Model\Equipment::where('vehicle_id', '=', $vehicle->id)->where('equipment_type', 'payment_method')->get() as $key => $data)
                                    <label style="font-size:13px">    
                                    @if($data->equipment_value == 'payment_method_types_pay1' )
                                    نقدا , 
                                    @endif
                                    @if($data->equipment_value == 'payment_method_types_pay2' )
                                    عن طريق البنك , 
                                    @endif
                                    @if($data->equipment_value == 'payment_method_types_pay3' )
                                    تقسيط مباشر , 
                                    @endif
                                    @if($data->equipment_value == 'payment_method_types_pay4' )
                                    دفعة أولى + شيكات شخصية </br>
                                    @endif
                                    </label>
                                @endforeach
                                </br>
                                
                                <span class="boxspan"style="font-size:14px;color:#595959; font-weight: bold;">  للطلب أو الإستفسار  :  </span>
                                </br>
                                
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">   رقم المحمول :  </span>
                                <label class="boxspan"style="font-size:13px">    
                                    {{(\App\User::where('id', '=', $vehicle->user_id)->select('seller_mobile')->first())->seller_mobile}}
                                </label>
                                </br>
                            
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;">    العنوان :  </span>
                                <label class="boxspan"style="font-size:13px">    
                                    {{(\App\User::where('id', '=', $vehicle->user_id)->select('address')->first())->address}}
                                </label>
                                </br>
                            
                                <span class="boxspan"style="font-size:13px;color:#595959; font-weight: bold;"> #Auto_and_Drive </span>
                            
                            </div>

                            <div class="text-center">
                                <a type="button"  data-id="{{$vehicle->id}}" class="code btn btn-sm" 
                                style="background-color:#2c5cb8; color:white">نسخ</a>
                            </div>
                           
                            <p id="informationCopied" hidden></p>
                        </div>
                        <div style=" padding:0.7rem" class="justify-content-center modal-footer">
                            <center>
                                <a type="button"  class="doneCopy btn btn-sm"  style="color:black">تم</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach
        </div>
    </div>
    
    <div class="col-lg-1 col-md-1 col-sm-1">
        
    </div>
    <h1 id='display' onClick='copyText(this)'>Text Sample</h1>
    
</div>

    <script>

        var ele = $("#informationCopied");
        var element = $("#urlCopied");

        $('.code').on('click', function() { 
            id = $(this).attr('data-id');
            copyText = document.querySelector('.element'+ id).textContent;
            console.log(copyText);
            
            ele.html(copyText);
            copyToClipboard(ele);
            swal("",  "تم نسخ المعلومات بنجاح");
            $('.doneCopy').on('click', function() { 
                
                $('#infoModal'+ id).modal('hide'); 
            });
        });
        
        function copyToClipboardURL(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();
        }


        var urlV = window.location.href; 
        var arrV = urlV.split('vec-seller/')[0];    
        $(document).on('click', '.shareInformationLink', function() { 
            id = $(this).attr('data-id');
            newUrl = arrV + "vec-seller/information/"+ id;
            element.html(newUrl);
            copyToClipboardURL(element);
            swal("",  "تم نسخ الرابط بنجاح");
        });

        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            $temp.focus();
            document.execCommand("copy");
            $temp.remove();
        }
    </script>
@endsection