@extends('layouts.apps')

@section('content')
<div class="row"> 
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
    <div class="col-lg-10 col-md-10 col-sm-10 ">
        <div class="card ">
            <div class="card-body ">
                <div class="row">
                    <div class="col-lg-6 col-md-6"></br>
                        <div class="row">
                            <div class="text-center col-lg-6 col-md-6 ml-auto mr-auto">
                                <h5 style="color:#595959; font-weight: bold;">
                                {{$make[0]->name}}, {{$make[0]->model_name}}
                                </h5>
                            </div>   
                        </div> </br>
                        <center>
                            <div id="carousel-example" class="text-center  carousel slide" data-ride="carousel">

                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                @foreach($collection as $key => $data)
                                    <li data-target="#carousel-example" data-slide-to="{{ $loop->index }}" 
                                        class="{{ $loop->first ? 'active' : '' }}"></li>
                                @endforeach
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox" style="width:100%; height: 200px;">
                                @foreach($collection as $key => $data)
                                    <div style="width:120%; height: 200px; background-color:white" class="carousel-item item {{ $loop->first ? ' active' : '' }}" >
                                        <a href="{{route('vec-seller.all', $vehicle->id)}}">
                                            <img src="{{strstr($data->thumb_path, '/storage/')}}" width="120%" 
                                            height="200px"style="object-fit: contain ">
                                        </a>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                        </center>
                        <div class="row" style="padding-top:0.7rem"> 
                            <div class="text-center col-lg-6 col-md-6 ml-auto mr-auto">
                                <i class=" fa fa-star" style="color:#ffcc00"></i>
                                <i class=" fa fa-star" style="color:#ffcc00"></i>
                                <i class=" fa fa-star" style="color:#ffcc00"></i>
                                <i class=" fa fa-star" style="color:#ffcc00"></i>
                                <i class=" fa fa-star" style="color:#ffcc00"></i>
                            </div>   
                            <div class="text-center col-lg-6 col-md-6 ml-auto mr-auto">
                            @if ($vehicle->vehicle_status == 'new')
                                جديد  
                            @else
                                مستعمل
                            @endif
                            </div>  
                        </div> </br>
                        <div class="text-center d-flex flex-row">
                            <div class=" col-lg-6 col-md-6 ml-auto mr-auto">
                                <i class="fa fa-circle fa-lg" style="color: #{{substr($vehicle->body_color, 1)}}"></i>
                                <span style="font-size:14px;">اللون الخارجي </span>   
                            </div> 
                            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                <i class="fa fa-circle fa-lg" style="color: #{{substr($vehicle->interior_color, 1)}}"></i>
                                  <span style="font-size:14px;">اللون الداخلي </span>  
                            </div> 
                        </div> 
                    </div>
                    <div class="col-lg-6 col-md-6"></br>
                        <div class="text-center d-flex flex-row">
                            <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/price.png')}}" width=30>
                                </div>
                                    <label style="font-size:13px"> السعر</label>
                               
                                <div class="">
                                    @if($vehicle->price_type == 'price_type_types_fp1')
                                    <label style="font-size:12px">{{ $vehicle->first_payment }} </label>
                                    @else @if($vehicle->price_type == 'price_type_types_fp2')
                                    <label style="font-size:12px">{{ $vehicle->first_payment }} </label>
                                    @endif
                                    @endif
                                </div>
                            </div> 
                            <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/power.png')}}" width=30>
                                </div>
                                    <label style="font-size:13px">القوة</label>
                                
                                <div class="">
                                    <label style="font-size:12px">{{ $vehicle->power }} </label>
                                </div>
                            </div> 
                            <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                <div class="">
                                    @if ($vehicle->gear == 'aut')
                                        <img src="{{url('/images/icons/automaticgear.png')}}" width=30>
                                    @else @if ($vehicle->gear == 'man')
                                        <img src="{{url('/images/icons/normalgear.png')}}" width=30>   
                                    @else  <img src="{{url('/images/icons/semigear.png')}}" width=30>
                                    @endif
                                    @endif
                                
                                </div>
                                    <label style="font-size:13px"> ناقل الحركة</label>
                                
                                <div class="">
                                    <label style="font-size:12px">
                                        @if ($vehicle->gear == 'aut')
                                        أوتماتيك  
                                        @else @if ($vehicle->gear == 'man')
                                        يدوي    
                                        @else نصف أوتماتيك
                                        @endif
                                        @endif
                                    </label>
                                </div>
                            </div> 
                        </div><hr>
                        <div class="text-center d-flex flex-row">
                            <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/fuel.jpeg')}}" width=30>
                                </div>
                                    <label style="font-size:13px"> الوقود</label>
                               
                                <div class="">
                                    <label style="font-size:12px">
                                        @if ($vehicle->fuel == 'bin')
                                        بنزين  
                                        @else @if ($vehicle->fuel == 'des')
                                        ديزل  
                                        @else @if ($vehicle->fuel == 'binelec')
                                        بنزين / كهرباء  
                                        @else @if ($vehicle->fuel == 'deselec')
                                        ديزل / كهرباء 
                                        @else @if ($vehicle->fuel == 'gaz')
                                        غاز    
                                        @else كهرباء
                                        @endif
                                        @endif
                                        @endif
                                        @endif
                                        @endif
                                    </label>
                                </div>
                            </div> 
                            <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/8757.png')}}" width=30>
                                </div>
                                    <label style="font-size:13px">سنة الانتاج</label>
                                
                                <div class="">
                                    <label style="font-size:12px">{{ $vehicle->year_of_product }} </label>
                                </div>
                            </div> 
                            <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/seat.jpeg')}}" width=30>
                                </div>
                                    <label style="font-size:13px"> عدد المقاعد</label>
                                
                                <div class="">
                                    <label style="font-size:12px">
                                        @if ($vehicle->num_of_seats == 's1')
                                        2+1  
                                        @else @if ($vehicle->num_of_seats == 's2')
                                        3+1  
                                        @else @if ($vehicle->num_of_seats == 's3')
                                        4+1  
                                        @else @if ($vehicle->num_of_seats == 's4')
                                        5+1    
                                        @else 6+1
                                        @endif
                                        @endif
                                        @endif
                                        @endif
                                    <label>
                                </div>
                            </div> 
                        </div> 
                        <hr>
                        <div class="text-center d-flex flex-row">
                            <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/hp.png')}}" width=65>
                                </div>
                                    <label style="font-size:13px"> عدد الأحصنة</label>
                               
                                <div class="">
                                    <label style="font-size:12px">{{ $vehicle->hp }} </label>
                                </div>
                            </div> 
                            <div class="col-lg-4 col-md-4  col-sm-4 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/Speedometer-512.png')}}" width=33>
                                </div>
                                    <label style="font-size:13px">المسافة المقطوعة</label>
                                
                                <div class="">
                                    <label style="font-size:12px">{{ $vehicle->mileage }} </label>
                                </div>
                            </div> 
                            <div class="col-lg-3 col-md-3 ml-auto mr-auto">
                                <div class="">
                                    <img src="{{url('/images/icons/key.png')}}" width=30>
                                </div>
                                
                                    <label style="font-size:13px"> عدد المفاتيح </label>
                                
                                <div class="">
                                    <label style="font-size:12px">
                                        @if ($vehicle->num_of_keys == 'kes1')
                                        1 
                                        @else @if ($vehicle->num_of_keys == 'kes2')
                                        2 
                                        @else 
                                        3   
                                        @endif
                                        @endif
                                    <label>
                                </div>
                            </div> 
                        </div> 
                    </div>
                </div>
            </div>
        </div></br>

        <div class="row">
            <table class="table table-striped">
                <tbody >
                    <tr  class="p-1" style="font-size:13px">
                        <td class="p-1">الهيكل</td>
                        <td class="p-1">الأصل</td>
                        <td class="p-1">سنة الترخيص</td>
                        <td class="p-1"> الاعفاء الجمركي</td>
                        <td class="p-1">نظام الدفع</td>
                        <td class="p-1"> تاريخ انتهاء الرخصة</td>
                    </tr>
                    <tr style="font-size:13px">
                        @if($vehicle->model_name == 'hat')
                        <td class="p-1">hatchback</td>
                        @else @if($vehicle->model_name == 'cou')
                        <td class="p-1">coupe</td>
                        @else @if($vehicle->model_name == 'sed')
                        <td class="p-1">sedan</td>
                        @else @if($vehicle->model_name == 'sta')
                        <td class="p-1">station</td>
                        @else @if($vehicle->model_name == 'spo')
                        <td class="p-1">sport-car</td>
                        @else @if($vehicle->model_name == 'cab')
                        <td class="">cabriolet</td>
                        @else @if($vehicle->model_name == 'pic')
                        <td class="p-1">pickup</td>
                        @else @if($vehicle->model_name == 'off')
                        <td class="p-1">off-road</td>
                        @else <td>station-wagon</td>
                        @endif
                        @endif
                        @endif
                        @endif
                        @endif
                        @endif
                        @endif
                        @endif
                        @if($vehicle->origin == 'spc')
                        <td class="p-1">خصوصي</td>
                        @else 
                        <td class="p-1">عمومي</td>
                        @endif
                        <td class="p-1">{{ $vehicle->year_of_work }}</td>
                        <td class="p-1">{{ $vehicle->customs_exemption }}</td>
                        @if($vehicle->drivetrain_system == 'fbf')
                        <td class="p-1">4X4</td>
                        @else 
                        <td class="p-1">2X4</td>
                        @endif
                        <td class="p-1">{{ $vehicle->license_date }}</td>
                    </tr>
                </tbody>
            </table><hr>
        </div>
        

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 ml-auto mr-auto" >
                <div  class="text-center">
                    <div class="form-group">
                        الاضافات
                    </div> 
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                @if(isset($furniture[0]) && $furniture[0]->equipment_value !== null)
                <div class="form-group">
                    <h6 style="font-size:15px">نوع الفرش</h6>
                </div><hr>
                <div class="d-flex flex-row">
                    <div class="p-1">
                        <div class="form-group">
                            @if($furniture[0]->equipment_value == 'ext_int_furniture_leat')
                            <input class="radiochoosen" id="ext_int_furniture1" type="radio" checked>
                            <label for="ext_int_furniture1" class="choice-button-text">جلد</label>
                            @else @if($furniture[0]->equipment_value == 'ext_int_furniture_colt')
                            <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                            <label for="ext_int_furniture2" class="choice-button-text">قماش</label>
                            @else @if($furniture[0]->equipment_value == 'ext_int_furniture_lndc')
                            <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                            <label for="ext_int_furniture2" class="choice-button-text">جلد + قماش</label>
                            @else @if($furniture[0]->equipment_value == 'ext_int_furniture_sprt')
                            <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                            <label for="ext_int_furniture2" class="choice-button-text">رياضي</label>
                            <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                            <label for="ext_int_furniture2" class="choice-button-text">مخمل</label>
                            @endif
                            @endif
                            @endif
                            @endif
                        </div> 
                    </div>
                </div>
                @endif
            </div>
            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                @if(isset($sunroof[0]) && $sunroof[0]->equipment_value !== null)
                <div class="form-group">
                    <h6 style="font-size:15px">فتحة السقف</h6>
                </div><hr>
                <div class="d-flex flex-row">
                    <div class="p-1">
                        <div class="form-group">
                            @if($sunroof[0]->equipment_value == 'ext_int_sunroof_norm')
                            <input class="radiochoosen" id="ext_int_furniture1" type="radio" checked>
                            <label for="ext_int_furniture1" class="choice-button-text">عادية</label>
                            @else @if($sunroof[0]->equipment_value == 'ext_int_sunroof_pano')
                            <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                            <label for="ext_int_furniture2" class="choice-button-text">بانوراما</label>
                            @else @if($sunroof[0]->equipment_value == 'ext_int_sunroof_conv')
                            <input class="radiochoosen" id="ext_int_furniture2" type="radio" checked>
                            <label for="ext_int_furniture2" class="choice-button-text">سقف متحرك</label>
                            @endif
                            @endif
                            @endif
                        </div> 
                    </div>
                </div>
                @endif
            </div>
            
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                @if(isset($steering) && $steering !== '')
                <div class="form-group">
                    <h6 style="font-size:15px">المقود</h6>
                </div><hr>
                <div class="d-flex flex-row">
                    <div class="p-1">
                        <div class="form-group">
                            @if (strlen(strpos($steering, 'ext_int_steering_refi')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">غيارات مقود</label>
                            @endif
                            @if (strlen(strpos($steering, 'ext_int_steering_stco')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">تحكم مقود</label>
                            @endif
                            @if (strlen(strpos($steering, 'ext_int_steering_hste')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">تدفئة مقود</label>
                            @endif
                        </div> 
                    </div>
                </div>
                @endif
            </div>
            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                @if(isset($glass) && $glass !== '')
                <div class="form-group">
                    <h6 style="font-size:15px">الزجاج</h6>
                </div><hr>
                <div class="d-flex flex-row">
                    <div class="p-1">
                        <div class="form-group">
                            @if (strlen(strpos($glass, 'ext_int_glass_egls')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">كهربائي</label>
                            @endif
                            @if (strlen(strpos($glass, 'ext_int_glass_lsrd')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text"> تعتيم ليزر</label>
                            @endif
                        </div> 
                    </div>
                </div>
                @endif
            </div>
            
        </div>

        <div class="row"> 
            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                @if(isset($seats) && $seats !== '')
                <div class="form-group">
                    <h6 style="font-size:15px">المقاعد</h6>
                </div><hr>
                <div class="d-flex flex-row">
                    <div class="p-1">
                        <div class="form-group">
                            @if (strlen(strpos($seats, 'ext_int_seats_htst')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">تدفئة مقاعد</label>
                            @endif
                            @if (strlen(strpos($seats, 'ext_int_seats_clst')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">تبريد مقاعد</label>
                            @endif
                            @if (strlen(strpos($seats, 'ext_int_seats_mast')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">مساج مقاعد</label>
                            @endif
                            @if (strlen(strpos($seats, 'ext_int_seats_spst')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">مقاعد رياضية</label>
                            @endif
                            @if (strlen(strpos($seats, 'ext_int_seats_elst')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">تحكم كهرباء</label>
                            @endif
                            @if (strlen(strpos($seats, 'ext_int_seats_mest')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">ذاكرة مقاعد</label>
                            @endif
                        </div> 
                    </div>
                </div>
                @endif
            </div>
            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                @if(isset($screens) && $screens !== '')
                <div class="form-group">
                    <h6 style="font-size:15px">الشاشات</h6>
                </div><hr>
                <div class="d-flex flex-row">
                    <div class="p-1">
                        <div class="form-group">
                            @if (strlen(strpos($screens, 'ext_int_screens_frsc')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">شاشة امامية</label>
                            @endif
                            @if (strlen(strpos($screens, 'ext_int_screens_basc')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">شاشات خلفية</label>
                            @endif
                            @if (strlen(strpos($screens, 'ext_int_screens_blue')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">بلوتوث</label>
                            @endif
                            @if (strlen(strpos($screens, 'ext_int_screens_ebph')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">قاعدة بلوتوث للهاتف</label>
                            @endif
                            @if (strlen(strpos($screens, 'ext_int_screens_usb')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">USB</label>
                            @endif
                        </div> 
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-6 ml-auto mr-auto" >
            @if(isset($intOthers) && $intOthers !== '')
                <div  class="text-center">
                    <div class="form-group">
                    اضافات داخلية أخرى
                    </div> 
                </div>
            @endif
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                @if(isset($rims[0]) && $rims[0]->equipment_value !== null)
                <div class="form-group">
                    <h6 style="font-size:15px">الجنط</h6>
                </div><hr>
                <div class="d-flex flex-row">
                    <div class="p-1">
                        <div class="form-group">
                            @if($rims[0]->equipment_value == 'ext_ext_rims_norl')
                            <input class="radiochoosen" type="radio" checked>
                            <label  class="choice-button-text">عادي</label>
                            @else @if($rims[0]->equipment_value == 'ext_ext_rims_magn')
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">مغنيسيوم</label>
                            @endif
                            @endif
                        </div> 
                    </div>
                </div>
                @endif
            </div>

            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                @if(isset($mirrors) && $mirrors !== '')
                <div class="form-group">
                    <h6 style="font-size:15px">المرايا</h6>
                </div><hr>
                <div class="d-flex flex-row">
                    <div class="p-1">
                        <div class="form-group">
                            @if (strlen(strpos($mirrors, 'ext_ext_mirrors_limi')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">اٍضاءة مرايا</label>
                            @endif
                            @if (strlen(strpos($mirrors, 'ext_ext_mirrors_clel')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text"> اٍغلاق كهرباء</label>
                            @endif
                        </div> 
                    </div>
                </div>
                @endif
            </div>

            

        </div>

        <div class="row"> 
            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                @if(isset($lights) && $lights !== '')
                <div class="form-group">
                    <h6 style="font-size:15px">الاٍضاءة</h6>
                </div><hr>
                <div class="d-flex flex-row">
                    <div class="p-1">
                        <div class="form-group">
                            @if (strlen(strpos($lights, 'ext_ext_light_znon')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">زينون</label>
                            @endif
                            @if (strlen(strpos($lights, 'ext_ext_light_ledl')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text"> ليد</label>
                            @endif
                            @if (strlen(strpos($lights, 'ext_ext_light_fogl')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">ضباب</label>
                            @endif
                        </div> 
                    </div>
                </div>
                @endif
            </div>
            <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                @if(isset($cameras) && $cameras !== '')
                <div class="form-group">
                    <h6 style="font-size:15px">الكاميرات</h6>
                </div><hr>
                <div class="d-flex flex-row">
                    <div class="p-1">
                        <div class="form-group">
                            @if (strlen(strpos($cameras, 'ext_ext_cameras_frnt')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">أمامية</label>
                            @endif
                            @if (strlen(strpos($cameras, 'ext_ext_cameras_back')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text"> خلفية</label>
                            @endif
                            @if (strlen(strpos($cameras, 'ext_ext_cameras_tsdc')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text"> 360 درجة</label>
                            @endif
                        </div> 
                    </div>
                </div>
                @endif
            </div>

            
        </div>

        @if(isset($sensors) && $sensors !== '')
        <div class="row"> 
            <div class="col-lg-12 col-md-12 ml-auto mr-auto">
                <div class="form-group">
                    <h6 style="font-size:15px">الحساسات</h6>
                </div><hr>
                <div class="d-flex flex-row">
                    <div class="p-1">
                        <div class="form-group">
                            @if (strlen(strpos($sensors, 'ext_ext_sensors_frot')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">امامي</label>
                            @endif
                            @if (strlen(strpos($sensors, 'ext_ext_sensors_bake')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">خلفي</label>
                            @endif
                            @if (strlen(strpos($sensors, 'ext_ext_sensors_tsds')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">360 درجة</label>
                            @endif
                            @if (strlen(strpos($sensors, 'ext_ext_sensors_rain')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">مطر</label>
                            @endif
                            @if (strlen(strpos($sensors, 'ext_ext_sensors_ligh')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">اٍضاءة</label>
                            @endif
                            @if (strlen(strpos($sensors, 'ext_ext_sensors_colb')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">مانع اٍصطدام</label>
                            @endif
                            @if (strlen(strpos($sensors, 'ext_ext_sensors_blin')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">النقطة العمياء</label>
                            @endif
                            @if (strlen(strpos($sensors, 'ext_ext_sensors_sele')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">تحديد مسار</label>
                            @endif
                            @if (strlen(strpos($sensors, 'ext_ext_sensors_sign')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">قارئ اٍشارات</label>
                            @endif
                            @if (strlen(strpos($sensors, 'ext_ext_sensors_self')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">اٍصطفاف ذاتي</label>
                            @endif
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if(isset($extOther) && $extOther !== '')
        <div class="row">
            <div class="col-lg-12 col-md-6 ml-auto mr-auto" >
                <div  class="text-center">
                    <div class="form-group">
                    اٍضافات خارجية أخرى
                    </div> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 ml-auto mr-auto">
                <div class="d-flex flex-row">
                    <div class="p-1">
                        <div class="form-group">
                            @if (strlen(strpos($extOther, 'ext_ext_other_hudp')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">Head Up Display</label>
                            @endif
                            @if (strlen(strpos($extOther, 'ext_ext_other_sela')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">اٍصطفاف ذاتي</label>
                            @endif
                            @if (strlen(strpos($extOther, 'ext_ext_other_bump')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">صدّام أمامي</label>
                            @endif
                            @if (strlen(strpos($extOther, 'ext_ext_other_hydr')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">رفع وخفض هيدروليكي</label>
                            @endif
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if(isset($genOther) && $genOther !== '')
        <div class="row">
            <div class="col-lg-12 col-md-6 ml-auto mr-auto" >
                <div  class="text-center">
                    <div class="form-group">
                    اضافات أخرى
                    </div> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 ml-auto mr-auto">
                <div class="d-flex flex-row">
                    <div class="p-1">
                        <div class="form-group">
                            @if (strlen(strpos($genOther, 'ext_gen_other_fing')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">بصمة أبواب</label>
                            @endif
                            @if (strlen(strpos($genOther, 'ext_gen_other_remo')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">صدّام أمامي</label>
                            @endif
                            @if (strlen(strpos($genOther, 'ext_gen_other_elec')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">قيادة ذاتية</label>
                            @endif
                            @if (strlen(strpos($genOther, 'ext_gen_other_ecos')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">نظام ECO</label>
                            @endif
                            @if (strlen(strpos($genOther, 'ext_gen_other_abss')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">نظام ABS</label>
                            @endif
                            @if (strlen(strpos($genOther, 'ext_gen_other_navi')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">نظام خوارط</label>
                            @endif
                            @if (strlen(strpos($genOther, 'ext_gen_other_door')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">أبواب شفط</label>
                            @endif
                            @if (strlen(strpos($genOther, 'ext_gen_other_main')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">دفتر صيانة</label>
                            @endif
                            @if (strlen(strpos($genOther, 'ext_gen_other_safe')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">رادار قياس مسافة الأمان</label>
                            @endif
                            @if (strlen(strpos($genOther, 'ext_gen_other_serv')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text"> دفتر خدمات</label>
                            @endif
                            @if (strlen(strpos($genOther, 'ext_gen_other_crui')))
                            <input class="radiochoosen" type="radio" checked>
                            <label class="choice-button-text">محدد سرعة</label>
                            @endif
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        @endif
        <center>
            <div class="d-flex flex-row">
                <div class="col-lg-5 col-md-5 col-sm-5 ml-auto mr-auto" >
                    <div class="card p-1 mb-1 bg-white rounded" style="border-radius: 16px !important;">
                        <div class="card-body ">
                            <div class="d-flex flex-row" >
                                <div class="col-lg-12 col-md-12 col-sm-12" >
                                    <label style="color:#595959; font-weight: bold;">معلومات المعرض</label>
                                </div>
                            </div>
                            <div class="d-flex flex-row" style="padding-top:0.1rem">
                                <div class="col-lg-12 col-md-12 col-sm-12" >
                                    <label style="font-size:13px">{{$user[0]->seller_name}}</label>
                                </div>
                            </div>
                            <div class="text-center d-flex flex-row" style="padding-top:0.5rem">
                                <div class="container"> 
                                    <div class="text-center"> 
                                        <a href="{{route('all-vehicle.show', $user[0]->id)}}" 
                                            class="btn btn-primary" style="background-color:#2c5cb8">
                                            <span style="font-size:13px">
                                               صفحة المعرض
                                            </span> 
                                        </a>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </center>
        
    </div>
    
    <div class="col-lg-1 col-md-1 col-sm-1">
    
    </div>
</div>

@endsection