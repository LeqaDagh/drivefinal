<div id="target">
    <div class="row">

        <div class="col-lg-6 col-md-6 ml-auto mr-auto">
            <div class="form-group">
                <input class="form-control" 
                placeholder="الاٍعفاء الجمركي" type="text" 
                disabled >
                
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="customs_exemption1" type="radio" name="customs_exemption" value="yes" >
                        <label for="customs_exemption1" class="choice-button-text">نعم</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="customs_exemption2" type="radio" name="customs_exemption" value="no" >
                        <label for="customs_exemption2" class="choice-button-text">لا</label>
                    </div> 
                </div>
            </div>


            <div class="form-group">
                <label >تاريخ انتهاء الرخصة
                </label>
                <input class="text-center form-control" name="license_date"
                placeholder="تاريخ انتهاء الرخصة" type="date"  >
            </div>

            <div class="form-group">
                <input class="form-control"
                placeholder="ملّاك سابقون" type="text" 
                disabled >
                
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="previous_owners1" type="radio" name="previous_owners" value="ow1" >
                        <label for="previous_owners1" class="choice-button-text">يد أولى</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="previous_owners2" type="radio" name="previous_owners" value="ow2" >
                        <label for="previous_owners2" class="choice-button-text">يد ثانية</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="previous_owners3" type="radio" name="previous_owners" value="ow3" >
                        <label for="previous_owners3" class="choice-button-text">يد ثالثة</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="previous_owners4" type="radio" name="previous_owners" value="ow4" >
                        <label for="previous_owners4" class="choice-button-text">أخرى</label>
                    </div> 
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 ml-auto mr-auto">
            <div class="form-group">
                <input class="form-control"
                placeholder="الأصل" type="text" 
                disabled >
                
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="origin1" type="radio" name="origin" value="spc" >
                        <label for="origin1" class="choice-button-text">خصوصي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="origin2" type="radio" name="origin" value="txi" >
                        <label for="origin2" class="choice-button-text">عمومي</label>
                    </div> 
                </div>
            </div>

            <div class="form-group">
                <input class="form-control" 
                placeholder="عدد المفاتيح" type="text" 
                disabled >
                
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="num_of_keys1" type="radio" name="num_of_keys" value="kes1" >
                        <label for="num_of_keys1" class="choice-button-text">1</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="num_of_keys2" type="radio" name="num_of_keys" value="kes2" >
                        <label for="num_of_keys2" class="choice-button-text">2</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="num_of_keys3" type="radio" name="num_of_keys" value="kes3" >
                        <label for="num_of_keys3" class="choice-button-text">3</label>
                    </div> 
                </div>
            </div>

            <div class="form-group shadow-textarea">
                <label class="float-right" for="exampleFormControlTextarea6">وصف اضافي</label>
                <textarea class="form-control z-depth-1" name="desc"
                    id="exampleFormControlTextarea6" rows="3" >
                </textarea>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-6 ml-auto mr-auto" >
            <div  class="float-right">
            اضافات داخلية
            </div>
        </div>
    </div></br>

    <div class="row">
        <div class="col-lg-6 col-md-6 ml-auto mr-auto">

            <div class="form-group">
                <input class="form-control"
                placeholder="نوع الفرش" type="text" 
                disabled >
                
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture1" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_leat" >
                        <label for="ext_int_furniture1" class="choice-button-text">جلد</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture2" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_colt" >
                        <label for="ext_int_furniture2" class="choice-button-text">قماش</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture3" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_lndc" >
                        <label for="ext_int_furniture3" class="choice-button-text">جلد + قماش</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture4" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_sprt" >
                        <label for="ext_int_furniture4" class="choice-button-text">رياضي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_furniture5" type="radio" name="ext_int_furniture" 
                        value="ext_int_furniture_velv" >
                        <label for="ext_int_furniture5" class="choice-button-text">مخمل</label>
                    </div> 
                </div>
            </div>

            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control"  
                    placeholder="المقاعد" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_int_seats_check" >
                </div> 
            </div>

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats1" type="checkbox" 
                        name="ext_int_seats[]" value="ext_int_seats_htst" >
                        <label for="ext_int_seats1" class="choice-button-text">تدفئة مقاعد</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats2" type="checkbox" 
                        name="ext_int_seats[]" value="ext_int_seats_clst" >
                        <label for="ext_int_seats2" class="choice-button-text">تبريد مقاعد</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats3" type="checkbox" 
                        name="ext_int_seats[]" value="ext_int_seats_mast" >
                        <label for="ext_int_seats3" class="choice-button-text">مساج مقاعد</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats4" type="checkbox" 
                        name="ext_int_seats[]" value="ext_int_seats_spst" >
                        <label for="ext_int_seats4" class="choice-button-text">مقاعد رياضية</label>
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
               
                
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats5" type="checkbox" 
                        name="ext_int_seats[]" value="ext_int_seats_elst" >
                        <label for="ext_int_seats5" class="choice-button-text">تحكم كهرباء</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_seats_checkbox radiochoosen" id="ext_int_seats6" type="checkbox" 
                        name="ext_int_seats[]" value="ext_int_seats_mest" >
                        <label for="ext_int_seats6" class="choice-button-text">ذاكرة مقاعد</label>
                    </div> 
                </div>
            </div>

            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control" 
                    placeholder="المقود" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_int_steering_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_steering_checkbox radiochoosen" id="ext_int_steering1" type="checkbox" 
                        name="ext_int_steering[]" value="ext_int_steering_refi" >
                        <label for="ext_int_steering1" class="choice-button-text">غيارات مقود</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_steering_checkbox radiochoosen" id="ext_int_steering2" type="checkbox" 
                        name="ext_int_steering[]" value="ext_int_steering_stco" >
                        <label for="ext_int_steering2" class="choice-button-text">تحكم مقود</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_steering_checkbox radiochoosen" id="ext_int_steering3" type="checkbox" 
                        name="ext_int_steering[]" value="ext_int_steering_hste" >
                        <label for="ext_int_steering3" class="choice-button-text">تدفئة مقود</label>
                    </div> 
                </div>
            </div>

            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control" 
                    placeholder="الشاشات" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_int_screens_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens1" type="checkbox" 
                        name="ext_int_screens[]" value="ext_int_screens_frsc" >
                        <label for="ext_int_screens1" class="choice-button-text">شاشة امامية</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens2" type="checkbox" 
                        name="ext_int_screens[]" value="ext_int_screens_basc" >
                        <label for="ext_int_screens2" class="choice-button-text">شاشات خلفية</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens3" type="checkbox"
                         name="ext_int_screens[]" value="ext_int_screens_blue" >
                        <label for="ext_int_screens3" class="choice-button-text">بلوتوث</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens5" type="checkbox" 
                        name="ext_int_screens[]" value="ext_int_screens_usb" >
                        <label for="ext_int_screens5" class="choice-button-text">USB</label>
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_screens_checkbox radiochoosen" id="ext_int_screens4" type="checkbox" 
                        name="ext_int_screens[]" value="ext_int_screens_ebph" >
                        <label for="ext_int_screens4" class="choice-button-text">قاعدة بلوتوث للهاتف</label>
                    </div> 
                </div>
            </div>
        </div>
        
        <div class="col-lg-6 col-md-6 ml-auto mr-auto">
            
            <div class="form-group">
                <input class="form-control"
                placeholder="فتحة السقف" type="text" 
                disabled >
                
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_sunroof1" type="radio" 
                        name="ext_int_sunroof" value="ext_int_sunroof_norm" >
                        <label for="ext_int_sunroof1" class="choice-button-text">عادية</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_sunroof2" type="radio" 
                        name="ext_int_sunroof" value="ext_int_sunroof_pano" >
                        <label for="ext_int_sunroof2" class="choice-button-text">بانوراما</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_int_sunroof3" type="radio" 
                        name="ext_int_sunroof" value="ext_int_sunroof_conv" >
                        <label for="ext_int_sunroof3" class="choice-button-text">سقف متحرك</label>
                    </div> 
                </div>
            </div>

            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control"  
                    placeholder="الزجاج" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_int_glass_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_glass_checkbox radiochoosen" id="ext_int_glass1" type="checkbox" 
                        name="ext_int_glass[]" value="ext_int_glass_egls" >
                        <label for="ext_int_glass1" class="choice-button-text">كهربائي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_glass_checkbox radiochoosen" id="ext_int_glass2" type="checkbox" 
                        name="ext_int_glass[]" value="ext_int_glass_lsrd" >
                        <label for="ext_int_glass2" class="choice-button-text">تعتيم ليزر</label>
                    </div> 
                </div>
            </div>

            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control"
                    placeholder="اضافات داخلية أخرى" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_int_other_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other1" type="checkbox" 
                        name="ext_int_other[]" value="ext_int_other_eio1" >
                        <label for="ext_int_other1" class="choice-button-text">زر تشغيل</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other2" type="checkbox"
                         name="ext_int_other[]" value="ext_int_other_eio2" >
                        <label for="ext_int_other2" class="choice-button-text">اٍضاءة داخلية</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other3" type="checkbox" 
                        name="ext_int_other[]" value="ext_int_other_eio3" >
                        <label for="ext_int_other3" class="choice-button-text">نظام صوتي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other4" type="checkbox" 
                        name="ext_int_other[]" value="ext_int_other_eio4" >
                        <label for="ext_int_other4" class="choice-button-text">اٍغلاق مركزي</label>
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_int_other_checkbox radiochoosen" id="ext_int_other5" type="checkbox" 
                        name="ext_int_other[]" value="ext_int_other_eio5" >
                        <label for="ext_int_other5" class="choice-button-text">وسائد هوائية</label>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-6 ml-auto mr-auto" >
            <div  class="float-right">
            اضافات خارجية
            </div>
        </div>
    </div></br>

    <div class="row">
        <div class="col-lg-6 col-md-6 ml-auto mr-auto">

            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control" 
                    placeholder="الاٍضاءة" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_ext_light_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_light_checkbox radiochoosen" id="ext_ext_light1" type="checkbox"
                         name="ext_ext_light[]" value="ext_ext_light_znon" >
                        <label for="ext_ext_light1" class="choice-button-text">زينون</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_light_checkbox radiochoosen" id="ext_ext_light2" type="checkbox" 
                        name="ext_ext_light[]" value="ext_ext_light_ledl" >
                        <label for="ext_ext_light2" class="choice-button-text">ليد</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_light_checkbox radiochoosen" id="ext_ext_light3" type="checkbox" 
                        name="ext_ext_light[]" value="ext_ext_light_fogl" >
                        <label for="ext_ext_light3" class="choice-button-text">ضباب</label>
                    </div> 
                </div>
            </div>


            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control"  
                    placeholder="المرايا" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_ext_mirrors_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_mirrorsbox radiochoosen" id="ext_ext_mirrors1" type="checkbox"
                         name="ext_ext_mirrors[]" value="ext_ext_mirrors_limi" >
                        <label for="ext_ext_mirrors1" class="choice-button-text">اٍضاءة مرايا</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_mirrorsbox radiochoosen" id="ext_ext_mirrors2" type="checkbox" 
                        name="ext_ext_mirrors[]" value="ext_ext_mirrors_clel" >
                        <label for="ext_ext_mirrors2" class="choice-button-text">اٍغلاق كهرباء</label>
                    </div> 
                </div>
            </div>

            <div class="form-group">
                <input class="form-control" 
                placeholder="الجنط" type="text" 
                disabled >
                
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_ext_rims1" type="radio" 
                        name="ext_ext_rims" value="ext_ext_rims_norl" >
                        <label for="ext_ext_rims1" class="choice-button-text">عادي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="radiochoosen" id="ext_ext_rims2" type="radio"
                         name="ext_ext_rims" value="ext_ext_rims_magn" >
                        <label for="ext_ext_rims2" class="choice-button-text">مغنيسيوم</label>
                    </div> 
                </div>
            </div>

            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control" 
                    placeholder="الكاميرات" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_ext_cameras_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_cameras_checkbox radiochoosen" id="ext_ext_cameras1" type="checkbox" 
                        name="ext_ext_cameras[]" value="ext_ext_cameras_frnt" >
                        <label for="ext_ext_cameras1" class="choice-button-text">أمامية</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_cameras_checkbox radiochoosen" id="ext_ext_cameras2" type="checkbox" 
                        name="ext_ext_cameras[]" value="ext_ext_cameras_back" >
                        <label for="ext_ext_cameras2" class="choice-button-text">خلفية</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_cameras_checkbox radiochoosen" id="ext_ext_cameras3" type="checkbox" 
                        name="ext_ext_cameras[]" value="ext_ext_cameras_tsdc" >
                        <label for="ext_ext_cameras3" class="choice-button-text">360 درجة</label>
                    </div> 
                </div>
            </div>


            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control" 
                    placeholder="اضافات أخرى" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_gen_other_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other1" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_fing" >
                        <label for="ext_gen_other1" class="choice-button-text">بصمة أبواب</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other2" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_remo" >
                        <label for="ext_gen_other2" class="choice-button-text">صدّام أمامي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other3" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_elec" >
                        <label for="ext_gen_other3" class="choice-button-text">قيادة ذاتية</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other4" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_ecos" >
                        <label for="ext_gen_other4" class="choice-button-text">نظام ECO</label>
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other5" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_abss" >
                        <label for="ext_gen_other5" class="choice-button-text">نظام ABS</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other6" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_navi" >
                        <label for="ext_gen_other6" class="choice-button-text">نظام خوارط</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other7" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_door" >
                        <label for="ext_gen_other7" class="choice-button-text">أبواب شفط</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other8" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_main" >
                        <label for="ext_gen_other8" class="choice-button-text">دفتر صيانة</label>
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other9" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_safe" >
                        <label for="ext_gen_other9" class="choice-button-text">رادار قياس مسافة الأمان</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other10" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_serv" >
                        <label for="ext_gen_other10" class="choice-button-text">دفتر خدمات</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_gen_other_checkbox radiochoosen" id="ext_gen_other11" type="checkbox" 
                        name="ext_gen_other[]" value="ext_gen_other_crui" >
                        <label for="ext_gen_other11" class="choice-button-text">محدد سرعة</label>
                    </div> 
                </div>
            </div>
        </div>
        
        <div class="col-lg-6 col-md-6 ml-auto mr-auto">

            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control" 
                    placeholder="الحساسات" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_ext_sensors_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors1" type="checkbox"
                         name="ext_ext_sensors[]" value="ext_ext_sensors_frot" >
                        <label for="ext_ext_sensors1" class="choice-button-text">امامي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors2" type="checkbox"
                         name="ext_ext_sensors[]" value="ext_ext_sensors_bake" >
                        <label for="ext_ext_sensors2" class="choice-button-text">خلفي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors3" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_tsds" >
                        <label for="ext_ext_sensors3" class="choice-button-text">360 درجة</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors4" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_rain" >
                        <label for="ext_ext_sensors4" class="choice-button-text">مطر</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors5" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_ligh" >
                        <label for="ext_ext_sensors5" class="choice-button-text">اٍضاءة</label>
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors6" type="checkbox"
                         name="ext_ext_sensors[]" value="ext_ext_sensors_colb" >
                        <label for="ext_ext_sensors6" class="choice-button-text">مانع اٍصطدام</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors7" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_blin" >
                        <label for="ext_ext_sensors7" class="choice-button-text">النقطة العمياء</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors8" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_sele" >
                        <label for="ext_ext_sensors8" class="choice-button-text">تحديد مسار</label>
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors9" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_sign" >
                        <label for="ext_ext_sensors9" class="choice-button-text">قارئ اٍشارات</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_sensors_checkbox radiochoosen" id="ext_ext_sensors10" type="checkbox" 
                        name="ext_ext_sensors[]" value="ext_ext_sensors_self" >
                        <label for="ext_ext_sensors10" class="choice-button-text">اٍصطفاف ذاتي</label>
                    </div> 
                </div>
            </div>


            <div class="form-group">
                <div class="input-group ">
                    <input class="form-control"  
                    placeholder="اٍضافات خارجية أخرى" type="text" 
                    disabled >
                    <input class="input-group-addon" type="checkbox"  id="ext_ext_other_check" >
                </div> 
            </div> 

            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_other_checkbox radiochoosen" id="ext_ext_other1" type="checkbox"
                         name="ext_ext_other[]" value="ext_ext_other_hudp" >
                        <label for="ext_ext_other1" class="choice-button-text">Head Up Display</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_other_checkbox radiochoosen" id="ext_ext_other2" type="checkbox" 
                        name="ext_ext_other[]" value="ext_ext_other_sela" >
                        <label for="ext_ext_other2" class="choice-button-text">اٍصطفاف ذاتي</label>
                    </div> 
                </div>
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_other_checkbox radiochoosen" id="ext_ext_other3" type="checkbox" 
                        name="ext_ext_other[]" value="ext_ext_other_bump" >
                        <label for="ext_ext_other3" class="choice-button-text">صدّام أمامي</label>
                    </div> 
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="p-1">
                    <div class="form-group">
                        <input class="ext_ext_other_checkbox radiochoosen" id="ext_ext_other4" type="checkbox" 
                        name="ext_ext_other[]" value="ext_ext_other_hydr" >
                        <label for="ext_ext_other4" class="choice-button-text">رفع وخفض هيدروليكي</label>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    </div>
</div>