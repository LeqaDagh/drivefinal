    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="{{ config('app.name', '') }}">
    <meta name="keywords" content="{{ config('app.name', '') }}">
    <meta name="author" content="{{ config('app.name', '') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', '') }}</title>
    <link rel="apple-touch-icon" href="{{url('public/css','')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{url('public','logo.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('public/css','vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('public/css','app.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('public/css','custom-rtl.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('public/css','vertical-menu.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('public/css','style-rtl.css')}}">
     <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />

   