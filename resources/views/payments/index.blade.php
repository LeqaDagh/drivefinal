@extends('layouts.app')

@section('content')
<div >
	


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.payments'),'url'=>url('payments')], 'action' =>trans('main.view')])


<div class="card">
    		@include('partials.card_header', ['title' =>trans('main.payments')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">
					 @can('create_payments')
					 <div >
					  <a href="{{url('payments','create')}}" class="btn btn-primary">@lang('main.create')</a>
					 </div>
					 @endcan 
					</p>
           
                </div>
                
                    @if (count($payments))
                    <div class="table-responsive">

<table class="table table-striped " >
	<thead>
	<tr>
		<th>{{trans('main.id')}}</th>
		<th>{{trans('main.name')}}</th>
		<th>{{trans('main.label')}}</th>
		<th>{{trans('main.created_at')}}</th>
		<th class="text-center">{{trans('main.options')}}</th>

		@can('show_payments')
		<th >{{trans('main.show')}}</th>
		@endcan

	</tr>
</thead>
<tbody>
	@foreach ($payments as $payment)
	<tr>
		<th scope="row">{{$payment->id}}</th>
		<td>{{$payment->name}}</td>
		<td>{{$payment->label }}</td>
		<td>{{$payment->created_at}}</td>
		<td class="text-center">

		@can('edit_payments')
		<a href="{{url('payments',$payment->id)}}/edit"><i class="fas fa-edit"></i></a>
		@endcan
		
		@can('destroy_groups')
		<i class="fas fa-trash-alt delete-item"  delete_item_id="{{$payment->id}}"></i>
		@endcan

		</td>

		@can('show_payments')
		<td><a href="{{url('payments',$payment->id)}}">{{trans('main.show')}}</a></td>
		@endcan
	</tr>
			
		@endforeach
	</tbody>
</table>

{{$payments->appends(request()->query())}}
</div>
	@endif
                </div>
            </div>
        </div>






	
@endsection


