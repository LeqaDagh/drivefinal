

 {!! Form::hidden('form', $form) !!}

 <section class="basic-elements">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('main.permissions')</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('name',trans('main.name'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                        {!!  Form::text('name',old('name'),['id'=>"name",'class'=>"form-control",'required'=>'true','autofocus'=>'true']) !!}

                                </fieldset>
                            </div>

                            <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('label',trans('main.label'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                        {!!  Form::text('label',old('label'),['id'=>"label",'class'=>"form-control",'required'=>'true']) !!}

                                </fieldset>
                            </div>
                         

                        </div>
                        <div class="form-group overflow-hidden">
                                <div class="col-12">
                                    <button data-repeater-create="" class="btn btn-primary btn-lg">
                                        <i class="icon-plus4"></i>  {{$btn}}
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



         


