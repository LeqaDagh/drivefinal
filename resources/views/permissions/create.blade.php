@extends('layouts.app')

@section('content')
        @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.permissions'),'url'=>url('permissions')], 'action' =>trans('main.create')])
        @include('partials.errors')

            {!! Form::open(['url' => 'permissions','class'=>'form-horizontal']) !!}
                @include('permissions.partials.form',['btn' =>trans('main.create'), 'form' =>'adding'])
            {!! Form::close() !!}

@endsection
    




