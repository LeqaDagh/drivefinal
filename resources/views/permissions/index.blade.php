@extends('layouts.app')

@section('content')
<div >
	


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.permissions'),'url'=>url('permissions')], 'action' =>trans('main.view')])


<div class="card">
    		@include('partials.card_header', ['title' =>trans('main.permissions')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">
					 @can('create_permissions')
					 <div >
					  <a href="{{url('permissions','create')}}" class="btn btn-primary">@lang('main.create')</a>
					 </div>
					 @endcan 
					</p>
           
                </div>
                
                    @if (count($permissions))
                    <div class="table-responsive">

<table class="table table-striped " >
	<thead>
	<tr>
		<th>{{trans('main.id')}}</th>
		<th>{{trans('main.name')}}</th>
		<th>{{trans('main.label')}}</th>
		<th>{{trans('main.roles')}}</th>
		<th>{{trans('main.created_at')}}</th>
		<th class="text-center">{{trans('main.options')}}</th>

		@can('show_permissions')
		<th >{{trans('main.show')}}</th>
		@endcan

	</tr>
</thead>
<tbody>
	@foreach ($permissions as $permission)
	<tr>
		<th scope="row">{{$permission->id}}</th>
		<td>{{$permission->name}}</td>
		<td>{{$permission->label }}</td>
		<td>{{$permission->roles->count() }}</td>
		<td>{{$permission->created_at}}</td>
		<td class="text-center">

		@can('edit_permissions')
		<a href="{{url('permissions',$permission->id)}}/edit"><i class="fas fa-edit"></i></a>
		@endcan
		
		@can('destroy_groups')
		<i class="fas fa-trash-alt delete-item"  delete_item_id="{{$permission->id}}"></i>
		@endcan

		</td>

		@can('show_permissions')
		<td><a href="{{url('permissions',$permission->id)}}">{{trans('main.show')}}</a></td>
		@endcan
	</tr>
			
		@endforeach
	</tbody>
</table>

{{$permissions->appends(request()->query())}}
</div>
	@endif
                </div>
            </div>
        </div>






	
@endsection


