@extends('layouts.app')

@section('content')

  @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.permissions'),'url'=>url('permissions')], 'action' =>$permission->label])

<h1>{{$permission->name}}</h1>

	@endsection
