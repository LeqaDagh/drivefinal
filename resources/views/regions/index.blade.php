@extends('layouts.app')

@section('content')
<div >
	


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.regions'),'url'=>url('regions')], 'action' =>trans('main.view')])


<div class="card">
    		@include('partials.card_header', ['title' =>trans('main.regions')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">
					 @can('create_regions')
					 <div >
					  <a href="{{url('regions','create')}}" class="btn btn-primary">@lang('main.create')</a>
					 </div>
					 @endcan 
					</p>
           
                </div>
                <div class="table-responsive">
                    @if (count($regions))

<table class="table table-striped " >
	<thead>
	<tr>
		<th>{{trans('main.id')}}</th>
		<th>{{trans('main.name')}}</th>
		<th>{{trans('main.parent')}}</th>
		<th>{{trans('main.created_at')}}</th>
		<th class="text-center">{{trans('main.options')}}</th>

		@can('show_regions')
		<th >{{trans('main.show')}}</th>
		@endcan

	</tr>
</thead>
<tbody>
	@foreach ($regions as $region)
	<tr>
		<th scope="row">{{$region->id}}</th>
		<td>{{$region->name}}</td>
		<td>{{@$region->region_parent->name}}</td>
		<td>{{$region->created_at}}</td>
		<td class="text-center">

		@can('edit_regions')
		<a href="{{url('regions',$region->id)}}/edit"><i class="fas fa-edit"></i></a>
		@endcan
		
		@can('destroy_regions')
		<i class="fas fa-trash-alt remove_region"  region_id="{{$region->id}}"></i>
		@endcan

		</td>

		@can('show_regions')
		<td><a href="{{url('regions',$region->id)}}">{{trans('main.show')}}</a></td>
		@endcan
	</tr>
			
		@endforeach
	</tbody>
</table>

{{$regions->appends(request()->query())}}
</div>
	@endif
                </div>
            </div>
        </div>






	
@endsection


@section('js')

@include('regions.js.remove_region')
@endsection
