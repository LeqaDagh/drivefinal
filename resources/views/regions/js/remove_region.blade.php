<script type="text/javascript">

$( document ).delegate( ".remove_region", "click", function() {

// get transaction id from div
var region_id =$(this).attr('region_id');
var c = confirm("@lang('main.confirm_remove_region')");
if(c==true)
{

//call by ajax
$.post( '{{url("regions/remove_region")}}', {region_id:region_id})
.done(function( data ) {

	document.location ="";

}); 
}


});

</script>