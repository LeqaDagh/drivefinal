@extends('layouts.app')

@section('content')

@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.regions'),'url'=>url('regions')], 'action' =>$region->name])

<h1>{{$region->name}}</h1>
@endsection