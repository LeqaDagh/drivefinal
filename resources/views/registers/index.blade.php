@extends('layouts.app')

@section('content')
<div >
  


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.registers'),'url'=>url('registers')], 'action' =>trans('main.view')])



@include('partials.search_form',['name'=>trans('main.search'),'url'=>'registers','blade'=>'registers.partials.search_form'])


<div class="card">
        @include('partials.card_header', ['title' =>trans('main.registers')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">


           <p><br><i><code><span>عدد النتائج : </span><span>{{ $registers->total()}}</span></code></i></p>
          </p>
           
                </div>
               
                    @if (count($registers))
 <div class="table-responsive">
<table class="table table-striped " >
  <thead>
  <tr>
    <th>{{trans('main.id')}}</th>
    <th>{{trans('main.name')}}</th>
    @can('show_add_by_register')<th>{{trans('main.added_by')}}</th>@endcan
    <th>{{trans('main.region')}}</th>
    <th>{{trans('main.mobile')}}</th>
    <th>{{trans('main.status')}}</th>
    <th class="text-center">{{trans('main.options')}}</th>

    @can('show_registers')
    <th >{{trans('main.show')}}</th>
    @endcan

  </tr>
</thead>
<tbody>
  @foreach ($registers as $user)
  <tr>
    <th scope="row">{{$user->id}}</th>
    <td>{!!$user->connectStatus()!!} {{$user->name}} </td>
    @can('show_add_by_register')<td>{{@$user->parent->name}} </td>@endcan
    <td>{{@$user->region->name}}</td>
    <td> {{$user->mobile}}</td>
    <td> {{trans('main.user_status')[$user->user_status]}}</td>
    <td class="text-center">

    @can('edit_registers')
    <a href="{{url('registers',$user->id)}}/edit"><i class="fas fa-edit"></i></a>
    @endcan
    
    @can('destroy_registers')
    <i class="fas fa-trash-alt delete-item"  delete_item_id="{{$user->id}}"></i>
    @endcan

    </td>

    @can('show_registers')
    <td><a href="{{url('registers',$user->id)}}">{{trans('main.show')}}</a></td>
    @endcan
  </tr>
      
    @endforeach
  </tbody>
</table>


{{$registers->appends(request()->query())}}


</div>
  @endif
                </div>
            </div>
        </div>






  
@endsection

