<!-- Modal -->
<div class="modal fade  " id="add_seller_for_register_modal" tabindex="-1" role="dialog" aria-labelledby="add_seller_for_register_modal_label">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="add_seller_for_register_modal_label">@lang('main.add_seller')</h4>
      </div>
      <div class="modal-body">
         {!! Form::open(['autocomplete'=>'off','class'=>'form-horizontal add_seller_for_register_form ','url'=>'registers/add_car_for_register_form_insert' ,'files'=>true]) !!}
               
		                    <div class="text-center"><h1>{{$register->name}}</h1></div>

                        <hr>

    

                       <div class="form-row">
                        <div class="form-group col-md-4">
                            {!! Form::label('seller_name',trans('main.seller_name')) !!}
                            {!! Form::text('seller_name','',['class'=>'text-center  form-control ']) !!}
                        </div>
                               <div class="form-group col-md-4">
                            {!! Form::label('seller_mobile',trans('main.seller_mobile')) !!}
                            {!! Form::text('seller_mobile','',['class'=>'text-center  form-control ']) !!}
                        </div>
                        <div class="form-group col-md-4">
                          {!! Form::label('order',trans('main.order')) !!}
                          {!! Form::number('order','',['class'=>'text-center  form-control ']) !!}
                        </div>
                      </div>  

    		
                      <div class="form-row">
                        <div class="form-group col-md-4">
                            {!! Form::label('region_id',trans('main.region')) !!}
                            {!! Form::select('region_id',$regions,null,['class'=>'form-control ']) !!}
                        </div>
                        <div class="form-group col-md-4">
                           {!! Form::label('publish_status',trans('main.publish_status_text')) !!}
                            {!! Form::select('publish_status',$publish_status,null,['class'=>'form-control ']) !!}
                        </div>
                        <div class="form-group col-md-4">
                           {!! Form::label('can_ignor_price',trans('main.can_ignor_price')) !!}
                            {!! Form::select('can_ignor_price',$can_ignor_price,null,['class'=>'form-control ']) !!}
                        </div>

                        
                      </div>  
                        <div id="map" style="height: 200px; width: 100%;"></div>

                        <div id="current" class="text-center"></div>
                        <hr>

                        <div class="form-row" style="display: none;">
                          <div class="form-group col-md-6">
                          {!! Form::label('lat',trans('main.lat')) !!}
                          {!! Form::text('lat','',['class'=>'text-center  form-control ','id'=>'lat']) !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('lon',trans('main.lon')) !!}
                            {!! Form::text('lon','',['class'=>'text-center  form-control ','id'=>'lon']) !!}
                        </div>
                   
                      </div>  


                      <div class="form-row">
                        <div class="form-group col">
                            {!! Form::label('name',trans('main.photos')) !!}
                            {!! Form::file('photos[]',['id'=>'fileupload','class'=>'text-center  form-control ','multiple'=>'','data-url'=>'upload']) !!}
                        </div>
                      </div> 

                      <hr>

                      <div class="gallery img-thumbnail text-center m-1">
                          @lang('main.photos') 
                      </div>





                  
                    {!! Form::hidden('register_id', $register->id) !!}


                    <div class="form-row">
                      <div class="form-group col text-center">
                     {!! Form::submit('اضافة',['class'=>'add_seller_for_register_submit_button btn btn-primary ']) !!}
                      </div>
                    </div>


             

              {!! Form::close() !!}
      </div>
      <div ><div class="add_seller_for_register_errors alert alert-danger d-none m-1" ></div></div>
       <div ><div class="add_seller_for_register_success alert alert-success text-center" style="display: none;"></div></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-danger" data-dismiss="modal">الغاء</button>
      </div>
    </div>
  </div>
</div>


