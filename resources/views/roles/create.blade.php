@extends('layouts.app')

@section('content')
        @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.roles'),'url'=>url('roles')], 'action' =>trans('main.create')])
        @include('partials.errors')

            {!! Form::open(['url' => 'roles','class'=>'form-horizontal']) !!}
                @include('roles.partials.form',['btn' =>trans('main.create'), 'form' =>'adding'])
            {!! Form::close() !!}

@endsection
    



