@extends('layouts.app')

@section('content')
<div >
	


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.roles'),'url'=>url('roles')], 'action' =>trans('main.view')])


<div class="card">
    		@include('partials.card_header', ['title' =>trans('main.roles')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">
					 @can('create_roles')
					 <div >
					  <a href="{{url('roles','create')}}" class="btn btn-primary">@lang('main.create')</a>
					 </div>
					 @endcan 
					</p>
           
                </div>
                <div class="table-responsive">
                    @if (count($roles))

<table class="table table-striped " >
	<thead>
	<tr>
		<th>{{trans('main.id')}}</th>
		<th>{{trans('main.name')}}</th>
		<th>{{trans('main.label')}}</th>
		<th>{{trans('main.permissions')}}</th>
		<th>{{trans('main.created_at')}}</th>
		<th class="text-center">{{trans('main.options')}}</th>

		@can('show_roles')
		<th >{{trans('main.show')}}</th>
		@endcan

	</tr>
</thead>
<tbody>
	@foreach ($roles as $role)
	<tr>
		<th scope="row">{{$role->id}}</th>
		<td>{{ $role->name }}</td>
		<td>{{ $role->label }}</td>
		<td>{{ $role->permissions->count() }}</td>
		<td>{{$role->created_at}}</td>
		<td class="text-center">

		@can('edit_roles')
		<a href="{{url('roles',$role->id)}}/edit"><i class="fas fa-edit"></i></a>
		@endcan
		
		@can('destroy_roles')
		<i class="fas fa-trash-alt delete-item"  delete_item_id="{{$role->id}}"></i>
		@endcan

		</td>

		@can('show_roles')
		<td><a href="{{url('roles',$role->id)}}">{{trans('main.show')}}</a></td>
		@endcan
	</tr>
			
		@endforeach
	</tbody>
</table>


{{$roles->appends(request()->query())}}
</div>
	@endif
                </div>
            </div>
        </div>



	
@endsection


