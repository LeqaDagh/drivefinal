


 {!! Form::hidden('form', $form) !!}

 <section class="basic-elements">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('main.groups')</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('name',trans('main.name'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                        {!!  Form::text('name',old('name'),['id'=>"name",'class'=>"form-control",'required'=>'true','autofocus'=>'true']) !!}

                                </fieldset>
                            </div>

                            <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('label',trans('main.label'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                        {!!  Form::text('label',old('label'),['id'=>"label",'class'=>"form-control",'required'=>'true']) !!}

                                </fieldset>
                            </div>
                         


                     <div class="col-lg-12 @lang('main.style.pull') form-group{{ $errors->has('permissions') ? ' has-error' : '' }}">
                                          {!! Form::label('permissions',trans('main.permissions'), ['class' => 'col-md-12  '.trans('main.style.pull').' control-label']) !!}
                                            <div class="row @lang('main.style.pull')" >

                                             @foreach($permissions as $key=>$permission)
                                             <div class="col-lg-2" >
                                                <div style="background: #eee;padding: 3px;margin-bottom: 3px;">{!! Form::checkbox('permissions[]',$key) !!} {{$permission}}</div>
                                             </div>
                                             @endforeach
                                       
                                            </div>
                            </div>


                        </div>
                        <div class="form-group overflow-hidden">
                                <div class="col-12">
                                    <button data-repeater-create="" class="btn btn-primary btn-lg">
                                        <i class="icon-plus4"></i>  {{$btn}}
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


          


       


      
             
