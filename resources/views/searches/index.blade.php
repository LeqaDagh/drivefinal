@extends('layouts.app')

@section('content')
<div >
	


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.searches'),'url'=>url('searches')], 'action' =>trans('main.view')])


<div class="card">
    		@include('partials.card_header', ['title' =>trans('main.searches')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">
				
					</p>
           
                </div>
                
                    @if (count($searches))
                    <div class="table-responsive">

<table class="table table-striped " >
	<thead>
	<tr>
		<th>{{trans('main.id')}}</th>
		<th>{{trans('main.name')}}</th>
		<th>{{trans('main.text')}}</th>
		<th>{{trans('main.created_at')}}</th>
	</tr>
</thead>
<tbody>
	@foreach ($searches as $search)
	<tr>
		<th scope="row">{{$search->id}}</th>
		<td>{{@$search->user->name}}</td>
		<td>{{@$search->text }}</td>
		<td>{{$search->created_at}}</td>
	</tr>
			
		@endforeach
	</tbody>
</table>

{{$searches->appends(request()->query())}}
</div>
	@endif
                </div>
            </div>
        </div>






	
@endsection


