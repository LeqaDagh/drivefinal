@extends('layouts.app')

@section('content')
<div >
	


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.sellers'),'url'=>url('sellers')], 'action' =>trans('main.view')])

@include('partials.search_form',['name'=>trans('main.search'),'url'=>'sellers','blade'=>'sellers.partials.search_form'])

<div class="card">
    		@include('partials.card_header', ['title' =>trans('main.sellers')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">
					
					</p>
           
                </div>
                
                    @if (count($sellers))
                    <div class="table-responsive">

<table class="table table-striped " >
	<thead>
	<tr>
		<th>{{trans('main.id')}}</th>
		<th>{{trans('main.seller_name')}}</th>
		<th>{{trans('main.seller_mobile')}}</th>
		<th>{{trans('main.seller_address')}}</th>
		<th>{{trans('main.vehicles')}}</th>
		<th>{{trans('main.created_at')}}</th>
		<th class="text-center">{{trans('main.options')}}</th>
		@can('show_sellers')
		<th>{{trans('main.show')}}</th>
		@endcan

	

	</tr>
</thead>
<tbody>
	@foreach ($sellers as $seller)
	<tr>
		<th scope="row">{{$seller->id}}</th>
		<td>{{$seller->seller_name}}</td>
		<td>{{$seller->seller_mobile}}</td>
		<td>{{@$seller->seller_address}}</td>
		<td>{{@$seller->vehicles->count()}}</td>
		
		<td>{{$seller->created_at}}</td>
		<td class="text-center">

	
		@can('destroy_sellers')
		<i class="fas fa-trash-alt delete-item"  delete_item_id="{{$seller->id}}"></i>
		@endcan

		@can('edit_sellers')
		<a href="{{url('sellers',$seller->id)}}/edit"><i class="fas fa-edit"></i></a>
		@endcan
		</td>
		@can('show_sellers')
		<td><a href="{{url('sellers',$seller->id)}}">@lang('main.show')</a></td>
		@endcan

	
	</tr>
			
		@endforeach
	</tbody>
</table>

{{$sellers->appends(request()->query())}}
</div>
	@endif
                </div>
            </div>
        </div>






	
@endsection


