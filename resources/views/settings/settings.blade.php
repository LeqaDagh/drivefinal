@extends('layouts.app')
@section('content')

 <section class="basic-elements">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('main.settings')</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="" >
                        	{!! Form::open(['method' =>'POST','class'=>'form-horizontal','url'=>'settings/settings','autocomplete'=>'off']) !!}
<div class="row">

        <div class="col-md-8">

<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#menu_1">اعدادات عامة</a>
  </li>

</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane container active" id="menu_1">
    <br>
                       <fieldset class="form-group"  >
                            {!! Form::label('close_site',trans('main.close_site'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                              {!! Form::select('close_site',trans('main.yes_no'),$settings->where('settings_key','close_site')->pluck('settings_value'),['class'=>'form-control']) !!}

                        </fieldset>

                         <fieldset class="form-group"  >
                            {!! Form::label('about_us',trans('main.about_us'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                              {!! Form::textarea('about_us',@$settings->where('settings_key','about_us')->pluck('settings_value')[0],['class'=>'form-control']) !!}

                        </fieldset>

                         <fieldset class="form-group"  >
                            {!! Form::label('contact_us',trans('main.contact_us'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                              {!! Form::textarea('contact_us',@$settings->where('settings_key','contact_us')->pluck('settings_value')[0],['class'=>'form-control']) !!}

                        </fieldset>
                        <fieldset class="form-group"  >
                            {!! Form::label('privacy_and_policy',trans('main.privacy_and_policy'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                              {!! Form::textarea('privacy_and_policy',@$settings->where('settings_key','privacy_and_policy')->pluck('settings_value')[0],['class'=>'form-control']) !!}

                        </fieldset>

                        
                      


                       
                        


                        

                        
  </div>


</div>


                   


 </div>
 
     </div>                 

                     
            



                    
                  

							 {!! Form::submit(trans('main.edit'),['class'=>'btn btn-primary']) !!} 

							{!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>






@endsection

