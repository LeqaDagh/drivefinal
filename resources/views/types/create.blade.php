@extends('layouts.app')

@section('content')
        @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.types'),'url'=>url('types')], 'action' =>trans('main.create')])
        @include('partials.errors')

            {!! Form::open(['url' => 'types','class'=>'form-horizontal']) !!}
                @include('types.partials.form',['btn' =>trans('main.create'), 'form' =>'adding'])
            {!! Form::close() !!}

@endsection
    




