@extends('layouts.app')

@section('content')
  @include('partials.breadcrumbs', ['method'=>['name'=>trans('main.types'),'url'=>url('types')], 'action' =>trans('main.edit')])


  @include('partials.errors')
      
        {!! Form::model($type,['method'=>'PATCH','class'=>'form-horizontal','files' => true,'action'=>['TypesController@update',$type->id]]) !!}
       @include('types.partials.form',['btn' =>trans('main.edit'), 'form' =>'editing'])
     {!! Form::close() !!}

@endsection


