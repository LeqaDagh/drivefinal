@extends('layouts.app')

@section('content')

	


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.vehicles'),'url'=>url('vehicles')], 'action' =>trans('main.view')])
<div class="row">

    <div class="col-lg-3">
@include('partials.search_form',['name'=>trans('main.search'),'url'=>'vehicles','blade'=>'vehicles.partials.search_form'])
</div>

<div class="col-lg-9">
<div class="card">
    		@include('partials.card_header', ['title' =>trans('main.vehicles')])
            <div class="card-content collapse show">
                <div class="card-body">
                   
           
                </div>
                
                    @if (count($vehicles))
                    <div class="table-responsive">
<div class="table-responsive">

<table class="table table-striped " >
	<thead>
	<tr>
		<th>{{trans('main.id')}}</th>
		<th>{{trans('main.user')}}</th>
		<th>{{trans('main.cover')}}</th>
		<th>{{trans('main.make')}}</th>
		<th>@lang('main.publish_status')</th>
		<th>{{trans('main.body')}}</th>
		<th>{{trans('main.year_of_product')}}</th>
		<th>{{trans('main.price')}}</th>
		<th>{{trans('main.gear')}}</th>
		<th>{{trans('main.fuel')}}</th>
		@can('show_vehicles')
		<th >{{trans('main.show')}}</th>
		@endcan

	</tr>
</thead>
<tbody>
	@foreach ($vehicles as $vehicle)
	<tr>
		<td scope="row"><a href="{{url('vehicles',$vehicle->id)}}">{{$vehicle->id}}</a></td>
		<td >{{@$vehicle->user->name}}</td>
		<td>	<img  class="img-thumbnail rounded" style="height: 30px;" src="{{url('public/storage/'.@$vehicle->cover->disk_name,@$vehicle->cover->file_name)}}"></td>
		<td> @if($vehicle->is_deleted=="yes") <i class="far fa-trash-alt" style="color: red;"></i> @endif
			{{$vehicle->make->name}}/{{$vehicle->mould->name }}
		</td>
		<td>{{trans('types.publish_status_type.'.$vehicle->publish_status.'.title')}}</td>
		<td>{{trans('types.body_types.'.$vehicle->body.'.title') }}</td>
		<td>{{$vehicle->year_of_product }}</td>
		<td>{{number_format($vehicle->price) }}</td>
		<td>{{trans('types.gear_types.'.$vehicle->gear.'.title') }}</td>
		<td>{{trans('types.fuel_types.'.$vehicle->fuel.'.title') }}</td>
		@can('show_vehicles')
		<td><a href="{{url('vehicles',$vehicle->id)}}">{{trans('main.show')}}</a></td>
		@endcan
	</tr>
			
		@endforeach
	</tbody>
</table>
</div>

{{$vehicles->appends(request()->query())}}
</div>
	@endif
                </div>
            </div>
    </div>

     </div>





	
@endsection


