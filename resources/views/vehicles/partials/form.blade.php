

 {!! Form::hidden('form', $form) !!}

 <section class="basic-elements">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('main.permissions')</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                     




 <ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="choose-tab" data-toggle="tab" href="#choose" role="tab" aria-controls="choose" aria-selected="true">@lang('main.choose')</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="fill-tab" data-toggle="tab" href="#fill" role="tab" aria-controls="profile" aria-selected="false">@lang('main.fill')</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="extra-tab" data-toggle="tab" href="#extra" role="tab" aria-controls="extra" aria-selected="false">@lang('main.extra')</a>
  </li>
</ul>

<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="choose" role="tabpanel" aria-labelledby="choose-tab">
    
<br>
                  <div class="form-row">
                    <div class="form-group col-md-4">
                     {!! Form::label('region_id',trans('main.region')) !!}
                     {!! Form::select('region_id',$regions,null,['class'=>'form-control ']) !!}
                    </div>    

                   <div class="form-group col-md-4">
                     {!! Form::label('sell_type_id',trans('main.sell_type')) !!}
                     {!! Form::select('sell_type_id',$sell_types,null,['class'=>'form-control ']) !!}
                    </div>
                 


                    <div class="form-group col-md-4">
                     {!! Form::label('vehicle_type_id',trans('main.vehicle_type')) !!}
                     {!! Form::select('vehicle_type_id',$vehicle_types,null,['class'=>'form-control ']) !!}
                    </div>

                

                    </div>


                    <div class="form-row">

                    <div class="form-group col-md-4">
                     {!! Form::label('make_id',trans('main.make')) !!}
                     {!! Form::select('make_id',$makes,null,['class'=>'form-control  child_detector' ,'child_target'=>'model_id']) !!}
                    </div>

                    <div class="form-group col-md-4">
                     {!! Form::label('model_id',trans('main.model')) !!}
                     {!! Form::select('model_id',$models,null,['class'=>'form-control ']) !!}
                    </div>


                    <div class="form-group col-md-4">
                     {!! Form::label('gear_id',trans('main.gear')) !!}
                     {!! Form::select('gear_id',$gears,null,['class'=>'form-control ']) !!}
                    </div>

                 
                  </div>


                   <div class="form-row">

                    <div class="form-group col-md-4">
                     {!! Form::label('condtion_id',trans('main.condtion')) !!}
                     {!! Form::select('condtion_id',$condtions,null,['class'=>'form-control ']) !!}
                    </div>

                    <div class="form-group col-md-4">
                     {!! Form::label('body_type_id',trans('main.body_type')) !!}
                     {!! Form::select('body_type_id',$body_types,null,['class'=>'form-control ']) !!}
                    </div>
                     <div class="form-group col-md-4">
                     {!! Form::label('fuel_type_id',trans('main.fuel_type')) !!}
                     {!! Form::select('fuel_type_id',$fuel_types,null,['class'=>'form-control ']) !!}
                    </div>
                 
                  </div>
                  <div class="form-row">
                     <div class="form-group col-md-4">
                     {!! Form::label('vehicle_status_id',trans('main.vehicle_status')) !!}
                     {!! Form::select('vehicle_status_id',$vehicle_status,null,['class'=>'form-control ']) !!}
                    </div>


                    <div class="form-group col-md-4">
                     {!! Form::label('order_type_id',trans('main.order_type')) !!}
                     {!! Form::select('order_type_id',$order_types,null,['class'=>'form-control ']) !!}
                    </div>


                 
                    <div class="form-group col-md-4" >
                      {!! Form::label('order',trans('main.order')) !!}
                     {!! Form::number('order',null,['class'=>'text-center  form-control ']) !!}
                    </div>
                  </div>

  </div>
  <div class="tab-pane fade" id="fill" role="tabpanel" aria-labelledby="fill-tab">
    <br>
           <div class="form-row">
                     <div class="form-group col-md-4" >
                      {!! Form::label('name',trans('main.name')) !!}
                    {!! Form::text('name',null,['class'=>'text-center  form-control ']) !!}
                    </div>


                    <div class="form-group col-md-4" >
                      {!! Form::label('price',trans('main.price')) !!}
                     {!! Form::number('price',null,['class'=>'text-center  form-control ']) !!}
                    </div>

                     <div class="form-group col-md-4" >
                      {!! Form::label('year',trans('main.year')) !!}
                     {!! Form::number('year',null,['class'=>'text-center  form-control ']) !!}
                    </div>

                    <div class="form-group col-md-4" >
                      {!! Form::label('power',trans('main.power')) !!}
                     {!! Form::number('power',null,['class'=>'text-center  form-control ']) !!}
                    </div>
                  </div>

                    <div class="form-row">
                     <div class="form-group col-md-4" >
                      {!! Form::label('num_of_seats',trans('main.num_of_seats')) !!}
                     {!! Form::number('num_of_seats',null,['class'=>'text-center  form-control ']) !!}
                    </div>


                      <div class="form-group col-md-4" >
                      {!! Form::label('body_color',trans('main.body_color')) !!}
                     {!! Form::text('body_color',null,['class'=>'text-center  form-control ']) !!}
                    </div>


                      <div class="form-group col-md-4" >
                      {!! Form::label('previous_owners',trans('main.previous_owners')) !!}
                     {!! Form::number('previous_owners',null,['class'=>'text-center  form-control ']) !!}
                    </div>

         

                     <div class="form-group col-md-4">
               
                        {!! Form::label('published',trans('main.publish_status')) !!}
                     {!! Form::select('published',$published,null,['class'=>'form-control ']) !!}
                    </div>
                  </div>
  </div>
  <div class="tab-pane fade" id="extra" role="tabpanel" aria-labelledby="extra-tab">
          
<br>
          <div class="form-row">
                           
                           @foreach($equipments as $key=>$equipment)

                           <div class="form-group col-md-2">
                            {!! Form::checkbox('equipment_id[]',$key,in_array($key,$vehicle->equipments()->pluck('attribute_id')->toArray()) ? true : false) !!}
                            {!! Form::label('equipment_id',$equipment) !!}
                           </div>
                           @endforeach
                         </div>

                


  </div>
</div>

                        <div class="form-group overflow-hidden">
                                <div class="col-12">
                                    <button data-repeater-create="" class="btn btn-primary btn-lg">
                                        <i class="icon-plus4"></i>  {{$btn}}
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



         


