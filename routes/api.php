<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1' ], function() 
{ 




	// Route::get('ss',function(){


	// 	$r=\DB::statement('SHOW COLUMNS FROM vehicles');

	// 	dd($r);

	// 	foreach (trans('types') as $key => $type) 
	// 	{
	// 		//substr($key,0,3)=="ext"
	// 		if(true)
	// 		{
				
	// 				//$name = $key.'_'.$k;
	// 				//echo  ' $table->boolean("'.$name.'")->default(0);';
	// 			//	echo '$this->filters["'.$key.'"] = trans("types.'.$key.'");';

	// 			//echo "'".$key."',";
	// 				echo "<br>";
				
	// 		}
			
	// 	}

	// });
 	 
		 Route::any('do_data', Api\V1\HomeApiController::class.'@doData');


	/* form here */
	Route::post('company_registration', Api\V1\HomeApiController::class.'@companyRegistration'); 
	Route::post('personal_registration', Api\V1\HomeApiController::class.'@personalRegistration');
	Route::post('login', Api\V1\AuthController::class.'@login');

	Route::post('vehicles/do/save_search', Api\V1\VehiclesApiController::class.'@saveSearch');
	

	Route::post('sellers/get/sellers', Api\V1\SellersApiController::class.'@sellers');
	Route::any('sellers/get/get_seller/{id}', Api\V1\SellersApiController::class.'@getSeller');
	Route::any('regions', Api\V1\HomeApiController::class.'@regions');
	Route::post('vehicles/do/search_vehicle_filters', Api\V1\VehiclesApiController::class.'@searchVehicleFilters');
	Route::post('vehicles/get/get_result_vehicles', Api\V1\VehiclesApiController::class.'@getResultVehicles');
	Route::any('vehicles/{id}', Api\V1\VehiclesApiController::class.'@show');
	Route::any('home_data', Api\V1\VehiclesApiController::class.'@homeData');
	Route::post('vehicles/get/get_special_vehicles', Api\V1\VehiclesApiController::class.'@getSpecialVehicles');
	Route::any('vehicles/do/compare', Api\V1\VehiclesApiController::class.'@compare');
	Route::any('vehicles/do/my_favorites', Api\V1\VehiclesApiController::class.'@myFavorites');
	Route::any('vehicles/get/my_searches', Api\V1\VehiclesApiController::class.'@mySearches');
	Route::any('vehicles/do/delete_search', Api\V1\VehiclesApiController::class.'@deleteSearch');
	

	Route::get('contact_us', Api\V1\HomeApiController::class.'@contactUs');
	Route::get('about_us', Api\V1\HomeApiController::class.'@aboutUs');
	Route::get('privacy_and_policy', Api\V1\HomeApiController::class.'@privacyAndPolicy');

	/* to here */


	 Route::group(['middleware' =>['auth:api'] ], function()
              {

            

              	/* form here */
              	Route::post('my_profile', Api\V1\HomeApiController::class.'@myProfile');
              	Route::post('edit_my_profile', Api\V1\HomeApiController::class.'@editMyProfile');
              	Route::post('edit_password', Api\V1\HomeApiController::class.'@editPassword');
              	Route::post('logout', Api\V1\AuthController::class.'@logout');
              	Route::post('vehicles/do/add_vehicle_filters', Api\V1\VehiclesApiController::class.'@addVehicleFilters');
              	Route::post('vehicles/do/add', Api\V1\VehiclesApiController::class.'@addVehicle');
              	Route::any('vehicles/get/my_vehicles', Api\V1\VehiclesApiController::class.'@myVehicles');
              	Route::post('vehicles/do/get_photos', Api\V1\VehiclesApiController::class.'@getPhotos');
              	Route::post('vehicles/do/upload_photos', Api\V1\VehiclesApiController::class.'@uploadPhotos');
              	Route::post('vehicles/do/delete_photo', Api\V1\VehiclesApiController::class.'@deletePhoto');
              	Route::post('vehicles/do/delete', Api\V1\VehiclesApiController::class.'@delete');
              	Route::any('vehicles/do/edit_vehicle_filters', Api\V1\VehiclesApiController::class.'@editVehicleFilters');
              	Route::post('vehicles/do/edit_vehicle_store/{id}', Api\V1\VehiclesApiController::class.'@editVehicleStore');
				/* to here */
				Route::post('company/do/get_company_photos', Api\V1\VehiclesApiController::class.'@getCompanyPhotos');
              	Route::post('company/do/upload_company_photos', Api\V1\VehiclesApiController::class.'@uploadCompanyPhotos');
              	Route::post('company/do/delete_company_photo', Api\V1\VehiclesApiController::class.'@deleteCompanyPhoto');
			 	

				

              });

});


