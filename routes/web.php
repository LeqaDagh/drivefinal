<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
//Auth::loginUsingId(1);
//dd(bcrypt('12345678'));
Route::resource('/homepage', 'HomepageController');
Route::get('/homepage', 'HomepageController@index')->name('homepage');
Auth::routes();

Route::resource('profile', 'ProfileController');
Route::get('/profile/images/{id}', ['as' => 'profile.images', 'uses' => 'ProfileController@companyImages']);
Route::resource('search', 'SearchController');
Route::post('/vehicle/search', ['as' => 'vehicle.search', 'uses' => 'SearchController@search']);
Route::resource('/compare', 'CompareController');
Route::post('/vehicle/compare1', 'CompareController@secondCompare');
Route::post('/vehicle/compare', ['as' => 'vehicle.compare', 'uses' => 'CompareController@compare']);
Route::resource('/stared', 'StaredController');
Route::resource('/contact', 'ContactController');
Route::resource('/used', 'UsingController');
Route::resource('/about-us', 'AboutUsController');
Route::resource('/vehicle', 'VehicleController');
Route::get('/vehicle/delete/{id}', ['as' => 'vehicle.delete', 'uses' => 'VehicleController@destroy']);
Route::resource('/all-vehicle', 'VehicleShopController');
Route::resource('/model', 'ModelsController');
//Route::get('/make/model/{id}', 'CompareController@secondCompare');
Route::resource('/make', 'MakeController');
Route::get('/media/{id}','MediaController@destroy');
Route::post('/media/images', ['as' => 'media.images', 'uses' => 'MediaController@addMoreImages']);
Route::get('/media/image/{id}', 'MediaController@getImage');
Route::resource('media', 'MediaController');
Route::resource('more', 'MoreController');
//Route::put('/vec-seller/edit/{id}', ['as' => 'vec-seller.update', 'uses' => 'VehicleSellerController@update']);
Route::resource('vec-seller', 'VehicleSellerController');
Route::get('/vec-seller/images/{id}', ['as' => 'vec-seller.images', 'uses' => 'VehicleSellerController@vehicleImages']);
Route::get('/vec-seller/information/{id}', ['as' => 'vec-seller.information', 'uses' => 'VehicleSellerController@vehicleInformation']);
Route::get('/vec-seller/all/{id}', ['as' => 'vec-seller.all', 'uses' => 'VehicleSellerController@vehicleAllImages']);
Route::post('/vec-seller/images/{id}', ['as' => 'vec-seller.images', 'uses' => 'VehicleSellerController@store']);
Route::get('/seller', 'SellerController@index')->name('seller');
Route::post('/seller', 'SellerController@create');
Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	//Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::patch('/profile/update/{user}', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::patch('/profile/password/{user}', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

Route::get('/', HomeController::class.'@home');
Route::get('home', HomeController::class.'@home');
Route::get('privacy_and_policy', HomeController::class.'@privacyAndPolicy');
Route::get('terms_and_condtions', HomeController::class.'@termsAndCondtions');
Route::get('support', HomeController::class.'@support');



// Route::get('search',HomeController::class.'@search');

// Route::get('show/{id}', HomeController::class.'@show');

Route::get('test_data',function(){

$setting = \App\Setting::where('settings_key','test_data')->first();

dd(unserialize($setting->settings_value));
});


Route::group(['middleware' =>['auth','can_do'] ], function()
{


 Route::resource('permissions', PermissionsController::class);
 Route::resource('roles', RolesController::class);

 
 Route::post('regions/remove_region', RegionsController::class.'@removeRegion');
 Route::resource('regions', RegionsController::class);

 Route::get('makes/show_makes', MakesController::class.'@index');
 Route::resource('makes', MakesController::class);


 Route::resource('moulds', MouldsController::class);

 
 Route::resource('searches', SearchesController::class);



 Route::post('sellers/change_password', SellersController::class.'@changePassword');
 Route::post('sellers/add_photos_for_seller', SellersController::class.'@addPhotosForSeller');
 Route::post('sellers/add_photos_for_seller_post', SellersController::class.'@addPhotosForSellerPost');
 Route::any('sellers/remove_photo',SellersController::class.'@removePhoto');
 Route::resource('sellers', SellersController::class);

 Route::post('vehicles/update_order', VehiclesController::class.'@updateOrder'); 
 Route::post('vehicles/do/delete_vehicle', VehiclesController::class.'@deleteVehicle');
 Route::post('vehicles/do/restore_vehicle', VehiclesController::class.'@restoreVehicle');
 Route::post('vehicles/do/publish_vehicle', VehiclesController::class.'@publishVehicle');
 Route::post('vehicles/do/unpublish_vehicle', VehiclesController::class.'@unpublishVehicle');
 Route::post('vehicles/do/make_unstared', VehiclesController::class.'@makeUnStared');
 Route::post('vehicles/do/make_stared', VehiclesController::class.'@makeStared');
 Route::post('vehicles/add_photos_for_vehicle', VehiclesController::class.'@addPhotosForVehicle');
 Route::post('vehicles/add_photos_for_vehicle_post', VehiclesController::class.'@addPhotosForVehiclePost');
 Route::any('vehicles/remove_photo',VehiclesController::class.'@removePhoto');
 Route::any('vehicles/set_as_slider',VehiclesController::class.'@setAsSlider');
 Route::any('vehicles/delete_as_slider',VehiclesController::class.'@deleteAsSlider');

 
 Route::resource('vehicles',VehiclesController::class);


 Route::resource('favorites',FavoritesController::class);
 Route::resource('medias',MediasController::class);
 Route::resource('payments',PaymentsController::class);


 Route::get('admins/pdf', AdminsController::class.'@pdf');
 Route::resource('admins', AdminsController::class);


 Route::get('registers/pdf', RegistersController::class.'@pdf');
 Route::post('registers/add_seller_for_register', RegistersController::class.'@addSellerForRegister');
 Route::post('registers/add_seller_for_register_post', RegistersController::class.'@addSellerForRegisterPost');
 Route::resource('registers', RegistersController::class);


 Route::get('clients/pdf', ClientsController::class.'@pdf');
 Route::resource('clients', ClientsController::class);


 Route::get('supports/pdf', SupportsController::class.'@pdf');
 Route::resource('supports', SupportsController::class);



Route::get('settings/settings', 'SettingsController@settings');
Route::post('settings/settings', 'SettingsController@settingsUpdate'); 


});



Route::get('doperm', function () {



if(true){

$controllers = [];

foreach (Route::getRoutes()->getRoutes() as $route)
{
$action = $route->getAction();

if (array_key_exists('controller', $action))
{

$controllers[] = $action['controller'];

}
}

foreach ($controllers as $key => $value) {

if (strpos($value, 'Auth') == false) {

$name =  str_replace("App\Http\Controllers\\","",$value);

$label_ex = str_replace("Controller@","#",$name);

$label = explode("#", $label_ex);

$final_text =preg_replace('!\s+!', ' ', ltrim(preg_replace('/[A-Z]/', ' $0', $label[1].' '.$label[0])));


$er  = strtolower(str_replace(" ","_",$final_text));

$arr = [
'name'=> strtolower(str_replace(" ","_",$final_text)),
'label'=> $final_text
];


$is = \App\Permission::where('name',$er)->first();

if(is_null($is))
{
\App\Permission::create($arr);
}




}

}



}





});



 Route::get('doroles',function(){

  	  DB::table('roles')->insert(['name'=>'admin','label'=>'admin']);
  	  DB::table('roles')->insert(['name'=>'seller','label'=>'seller']);
	  DB::table('roles')->insert(['name'=>'client','label'=>'client']);
	  DB::table('roles')->insert(['name'=>'support','label'=>'support']);


 	$admin_permissions =     DB::table('permissions')->get();

 	foreach ($admin_permissions as $key => $admin_permission)
 	{
 		 $id = DB::table('permission_role')->insertGetId(['permission_id'=>$admin_permission->id,'role_id'=>1]);
 	}

	 \App\User::create([
		 	'name'=>'adli',
		 	'email'=>'almashniadli@gmail.com',
		 	'group'=>'ad',
		 	'mobile'=>'0597557799',
		 	'username'=>'adli',
		 	'region_id'=>1,
		 	'password'=>'@123456',

	 	]);
	 DB::table('role_user')->insert(['user_id'=>1,'role_id'=>1]);
	 DB::table('settings')->insert(['settings_key'=>'close_site','settings_name'=>'close_site','settings_value'=>'no']);
	 DB::table('settings')->insert(['settings_key'=>'about_us','settings_name'=>'about_us','settings_value'=>'about_us']);
	 DB::table('settings')->insert(['settings_key'=>'contact_us','settings_name'=>'contact_us','settings_value'=>'contact_us']);
	 DB::table('settings')->insert(['settings_key'=>'privacy_and_policy','settings_name'=>'privacy_and_policy','settings_value'=>'privacy_and_policy']);



 });
          
